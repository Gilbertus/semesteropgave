package storage;

import java.io.Serializable;
import java.util.ArrayList;

import application.model.Customer;
import application.model.Order;
import application.model.Payment;
import application.model.PriceList;
import application.model.Product;

public class Storage implements Serializable
{

    private static Storage instance;

    private final ArrayList<Product> products = new ArrayList<>();
    private final ArrayList<PriceList> priceLists = new ArrayList<>();
    private final ArrayList<Order> orders = new ArrayList<>();
    private final ArrayList<Payment> payments = new ArrayList<>();
    private final ArrayList<Customer> customers = new ArrayList<>();

    private Storage()
    {
        //
    }

    public static Storage getInstance()
    {
        if (instance == null)
        {
            instance = new Storage();
        }

        return instance;
    }

    public void clearStorage()
    {
        products.clear();
        priceLists.clear();
    }

    // ---------------------------- Customer --------------------------------------

    public ArrayList<Customer> getAllCustomers()
    {
        return new ArrayList<Customer>(customers);
    }

    public void storeCustomer(Customer customer)
    {
        assert customer != null;
        customers.add(customer);
    }

    public void removeCustomer(Customer customer)
    {
        assert customer != null;
        customers.remove(customer);
    }

    // ----------------------------- Product -------------------------------------------

    public ArrayList<Product> getAllProducts()
    {
        return new ArrayList<Product>(products);
    }

    public void storeProduct(Product product)
    {
        assert product != null;
        products.add(product);
    }

    public void removeProduct(Product product)
    {
        assert product != null;
        products.remove(product);
    }

    // ----------------------------- PriceList -------------------------------------------

    public ArrayList<PriceList> getAllPriceLists()
    {
        return new ArrayList<PriceList>(priceLists);
    }

    public void storePriceList(PriceList priceList)
    {
        assert priceList != null;
        priceLists.add(priceList);
    }

    public void removePriceList(PriceList priceList)
    {
        assert priceList != null;
        priceLists.remove(priceList);
    }

    // ----------------------------- Order -------------------------------------------

    public ArrayList<Order> getAllOrders()
    {
        return new ArrayList<Order>(orders);
    }

    public void storeOrder(Order order)
    {
        assert order != null;
        orders.add(order);
    }

    public void removeOrder(Order order)
    {
        assert order != null;
        assert orders.contains(order);
        orders.remove(order);
    }

    // ---------------------------- Payment -----------------------------------------

    public ArrayList<Payment> getAllPayments()
    {
        return new ArrayList<Payment>(payments);

    }

    public void storePayment(Payment payment)
    {
        assert payment != null;
        payments.add(payment);

    }

    public void removePayment(Payment payment)
    {
        assert payment != null;
        assert payments.contains(payment);
        payments.remove(payment);
    }

}
