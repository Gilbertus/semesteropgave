package storage;

import java.time.LocalDate;
import java.time.LocalDateTime;

import application.model.Customer;
import application.model.Order;
import application.model.Payment;
import application.model.PaymentType;
import application.model.PriceList;
import application.model.Product;
import application.model.ProductType;
import application.service.Service;

public class storageInitializer
{

    public static void main(String[] args)
    {
        initializeStorage();
    }

    private static void initializeStorage()
    {

        Service service = new Service();
        service.setStorage(Storage.getInstance());
        Storage storage = Storage.getInstance();
        storage.clearStorage();

        PriceList pl1 = service.createPriceList("Standard");
        PriceList pl2 = service.createPriceList("Fredag");
        PriceList pl3 = service.createPriceList("Reservations produkter");

//        // ------- VoucherClip ---------------------

        Product v1 = service.createVoucherClip("Klippekort", 500, ProductType.VOUCHERCLIP, 4);
        pl1.createPrice(100, v1);
        pl2.createPrice(100, v1);

//        // ------- Products ----------------------
        Product p1 = service.createProduct("Peanuts", 500, ProductType.MISC);
        pl1.createPrice(10, p1);
        pl2.createPrice(10, p1);
        Product p2 = service.createProduct("Glas", 500, ProductType.MISC);
        pl1.createPrice(15, p2);
        pl2.createPrice(15, p2);
        Product p3 = service.createProduct("Æblebrus", 500, ProductType.MISC);
        pl1.createPrice(15, p3);
        pl2.createPrice(15, p3);
        Product p4 = service.createProduct("Chips ", 500, ProductType.MISC);
        pl1.createPrice(10, p4);
        pl2.createPrice(10, p4);
        Product p5 = service.createProduct("Cola", 500, ProductType.MISC);
        pl1.createPrice(15, p5);
        pl2.createPrice(15, p5);
        Product p6 = service.createProduct("Nikoline", 500, ProductType.MISC);
        pl1.createPrice(15, p6);
        pl2.createPrice(15, p6);
        Product p7 = service.createProduct("7-Up", 500, ProductType.MISC);
        pl1.createPrice(15, p7);
        pl2.createPrice(15, p7);
        Product p8 = service.createProduct("Vand", 500, ProductType.MISC);
        pl1.createPrice(10, p8);
        pl2.createPrice(10, p8);
        Product p9 = service.createProduct("T-shirt", 500, ProductType.MISC);
        pl1.createPrice(70, p9);
        pl2.createPrice(70, p9);
        Product p10 = service.createProduct("Polo", 500, ProductType.MISC);
        pl1.createPrice(100, p10);
        pl2.createPrice(100, p10);
        Product p11 = service.createProduct("Cap", 500, ProductType.MISC);
        pl1.createPrice(30, p11);
        pl2.createPrice(30, p11);
        Product p12 = service.createProduct("Krus", 500, ProductType.MISC);
        pl1.createPrice(60, p12);
        pl2.createPrice(60, p12);

        // -------- Draft Beer -----------------------------
        Product f1 = service.createBerverage("Klosterbryg Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f1);
        pl2.createPrice(30, f1);
        Product f2 = service.createBerverage("Jazz Classic Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f2);
        pl2.createPrice(30, f2);
        Product f3 = service.createBerverage("Extra Pilsner Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f3);
        pl2.createPrice(30, f3);
        Product f4 = service.createBerverage("Celebration Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f4);
        pl2.createPrice(30, f4);
        Product f5 = service.createBerverage("Blondie Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f5);
        pl2.createPrice(30, f5);
        Product f6 = service.createBerverage("Forårsbryg Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f6);
        pl2.createPrice(30, f6);
        Product f7 = service.createBerverage("India Pale Ale Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f7);
        pl2.createPrice(30, f7);
        Product f8 = service.createBerverage("Julebryg Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f8);
        pl2.createPrice(30, f8);
        Product f9 = service.createBerverage("Imperial Stout Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f9);
        pl2.createPrice(30, f9);
        Product f10 = service.createBerverage("Special Fadøl", 500, ProductType.TAPBEER, 7);
        pl1.createPrice(30, f10);
        pl2.createPrice(30, f10);

        // -------------- Bottle beer -------------------------------

        Product b1 = service.createBerverage("Klosterbryg Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b1);
        pl2.createPrice(36, b1);
        Product b2 = service.createBerverage("Sweet Georgia Brown Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b2);
        pl2.createPrice(36, b2);
        Product b3 = service.createBerverage("Extra Pilsner Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b3);
        pl2.createPrice(36, b3);
        Product b4 = service.createBerverage("Celebration Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b4);
        pl2.createPrice(36, b4);
        Product b5 = service.createBerverage("Blondie Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b5);
        pl2.createPrice(36, b5);
        Product b6 = service.createBerverage("Forårsbryg Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b6);
        pl2.createPrice(36, b6);
        Product b7 = service.createBerverage("India Pale Ale Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b7);
        pl2.createPrice(36, b7);
        Product b8 = service.createBerverage("Julebryg Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b8);
        pl2.createPrice(36, b8);
        Product b9 = service.createBerverage("Juletønden Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b9);
        pl2.createPrice(36, b9);
        Product b10 = service.createBerverage("Old Strong Ale Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b10);
        pl2.createPrice(36, b10);
        Product b11 = service.createBerverage("Fregatten Jylland", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b11);
        pl2.createPrice(36, b11);
        Product b12 = service.createBerverage("Imperial Stout Flaske", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b12);
        pl2.createPrice(36, b12);
        Product b13 = service.createBerverage("Tribute", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b13);
        pl2.createPrice(36, b13);
        Product b14 = service.createBerverage("Black Monster", 500, ProductType.BOTTLE, 7);
        pl1.createPrice(50, b14);
        pl2.createPrice(50, b14);

        // ----------------- Spirits -----------------------------------

        Product s1 = service.createBerverage("Spirit of Aarhus", 500, ProductType.SPIRIT, 7);
        pl1.createPrice(300, s1);
        pl2.createPrice(300, s1);
        Product s2 = service.createBerverage("SOA med pind", 500, ProductType.SPIRIT, 7);
        pl1.createPrice(350, s2);
        pl2.createPrice(350, s2);
        Product s3 = service.createBerverage("Whisky", 500, ProductType.SPIRIT, 7);
        pl1.createPrice(500, s3);
        pl2.createPrice(500, s3);
        Product s4 = service.createBerverage("Liquor of Aarhus", 500, ProductType.SPIRIT, 7);
        pl1.createPrice(175, s4);
        pl2.createPrice(175, s4);

        // ------------------ GiftBox ----------------------------------

        Product g1 = service.createGiftBox("Gaveæske: 2 øl, 2 glas", 500, ProductType.GIFTBOX, 2, 2,
                "Gaveæske");
        pl1.createPrice(100, g1);
        pl2.createPrice(100, g1);

        Product g2 = service.createGiftBox("Gaveæske: 4 øl", 500, ProductType.GIFTBOX, 4, 0, "Gaveæske");
        pl1.createPrice(130, g2);
        pl2.createPrice(130, g2);
        Product g3 = service.createGiftBox("Gaveæske: 6 øl", 500, ProductType.GIFTBOX, 6, 0, "Trækasse");
        pl1.createPrice(240, g3);
        pl2.createPrice(240, g3);
        Product g4 = service.createGiftBox("Gaveæske: 6 øl, 2 glas", 500, ProductType.GIFTBOX, 6, 2,
                "Gavekurv");
        pl1.createPrice(250, g4);
        pl2.createPrice(250, g4);
        Product g5 = service.createGiftBox("Gaveæske: 6 øl, 6 glas", 500, ProductType.GIFTBOX, 6, 6,
                "Trækasse");
        pl1.createPrice(290, g5);
        pl2.createPrice(290, g5);
        Product g6 = service.createGiftBox("Gaveæske: 12 øl", 500, ProductType.GIFTBOX, 12, 0, "Trækasse");
        pl1.createPrice(390, g6);
        pl2.createPrice(390, g6);
        Product g7 = service.createGiftBox("Gaveæske: 12 øl", 500, ProductType.GIFTBOX, 12, 0, "Papkasse");
        pl1.createPrice(360, g7);
        pl2.createPrice(360, g7);

        // ------------------ Anlæg Product ----------------------------------

        Product al1 = service.createDraftBeerDispenser("1-hane", 5, ProductType.DISPENSER, false);
        pl3.createPrice(250, al1);
        Product al2 = service.createDraftBeerDispenser("2-haner", 10, ProductType.DISPENSER, false);
        pl3.createPrice(400, al2);
        Product al3 = service.createDraftBeerDispenser("Bar", 10, ProductType.DISPENSER, false);
        pl3.createPrice(500, al3);

        Product delivery = service.createProduct("Levering", 5000, ProductType.MISC);
        pl3.createPrice(500, delivery);

        // ------------------ Deposit Product ----------------------------------

        Product d1 = service.createDepositProduct("Fustage KlosterBryg", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d1);
        Product d2 = service.createDepositProduct("Fustage Jazz Classic", 20, ProductType.KEG, "Liter", 25, 200);
        pl3.createPrice(625, d2);
        Product d3 = service.createDepositProduct("Fustage Extra Pilsner", 20, ProductType.KEG, "Liter", 25, 200);
        pl3.createPrice(575, d3);
        Product d4 = service.createDepositProduct("Fustage Celebration", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d4);
        Product d5 = service.createDepositProduct("Fustage Blondie", 20, ProductType.KEG, "Liter", 25, 200);
        pl3.createPrice(700, d5);
        Product d6 = service.createDepositProduct("Fustage Forårsbryg", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d6);
        Product d7 = service.createDepositProduct("Fustage India Pale Ale", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d7);
        Product d8 = service.createDepositProduct("Fustage Julebryg", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d8);
        Product d9 = service.createDepositProduct("Fustage Imperial Stout", 20, ProductType.KEG, "Liter", 20, 200);
        pl3.createPrice(775, d9);
        Product d10 = service.createTourTeam("Rundvisning pr. person", 500, ProductType.TOUR);
        pl3.createPrice(100, d10);

        Product kulsyre = service.createDepositProduct("Kulsyre", 10, ProductType.CARBON, "Kg", 6, 1000);
        pl3.createPrice(400, kulsyre);

        // ---------------------- Order ---------------------------------------------

        Payment pay1 = service.createPayment(PaymentType.CASH, 30);
        Payment pay2 = service.createPayment(PaymentType.CASH, 50);
        Payment pay3 = service.createPayment(PaymentType.CASH, 10);
        Payment pay4 = service.createPayment(PaymentType.CASH, 103);
        Payment pay5 = service.createPayment(PaymentType.CASH, 145);
        Payment pay6 = service.createPayment(PaymentType.CASH, 775);
        Payment pay7 = service.createPayment(PaymentType.CASH, 1000);
        Payment pay8 = service.createPayment(PaymentType.CASH, 1000);

        // 2014, 7, 25, 0, 0, 10 (yyyy-mm-dd - hh-mm-ss
        Order o1 = service.createOrder(LocalDateTime.of(2018, 1, 10, 10, 30, 00));
        o1.createOrderLine(2, 36, b1);
        o1.createOrderLine(1, 36, b2);
        o1.addPayment(pay1);

        Order o2 = service.createOrder(LocalDateTime.of(2018, 4, 8, 14, 15, 00));
        o2.createOrderLine(2, 50, b3);
        o2.createOrderLine(1, 50, b4);
        o2.addPayment(pay2);

        Order o3 = service.createOrder(LocalDateTime.of(2018, 4, 3, 18, 45, 00));
        o3.createOrderLine(2, 36, b5);
        o3.createOrderLine(1, 76, b6);
        o3.addPayment(pay3);

        Order o4 = service.createOrder(LocalDateTime.of(2018, 3, 27, 9, 30, 00));
        o4.createOrderLine(2, 88, b7);
        o4.createOrderLine(1, 36, b8);
        o4.addPayment(pay4);

        Order o5 = service.createOrder(LocalDateTime.of(2018, 3, 9, 3, 05, 00));
        o5.createOrderLine(2, 133, b9);
        o5.createOrderLine(1, 36, b10);
        o5.addPayment(pay5);

        Order r1 = service.createOrderReservation(LocalDateTime.now(), LocalDate.of(2018, 4, 18),
                LocalDate.of(2018, 4, 20), false);
        r1.createOrderLine(1, 775, al1);
        r1.addPayment(pay6);

        Order r2 = service.createOrderReservation(LocalDateTime.now(), LocalDate.of(2018, 4, 25),
                LocalDate.of(2018, 4, 25), false);
        r2.createOrderLine(10, 1000, d10);
        r2.addPayment(pay8);

        Order r3 = service.createOrderReservation(LocalDateTime.now(), LocalDate.of(2018, 4, 20),
                LocalDate.of(2018, 4, 20), false);
        r3.createOrderLine(10, 1000, d10);
        r3.addPayment(pay7);

        Order r4 = service.createOrderReservation(LocalDateTime.now(), LocalDate.of(2018, 4, 19),
                LocalDate.of(2018, 4, 19), false);
        r4.createOrderLine(10, 1000, d10);
        r4.addPayment(pay7);

        Customer c1 = service.createCustomer("Torben HejMedVFas Johansen", "Birkemosevej 293", "89898989",
                "kodwa@kadw.dk");
        c1.addOrder(o1);
        c1.addOrder(o2);

        Customer c2 = service.createCustomer("Frank Tobiasen Julejusen", "Lupinvejefejevej 23", "67438923",
                "kodwa@kadw.dk");
        c2.addOrder(o3);
        c2.addOrder(o4);

        Customer c3 = service.createCustomer("Kenny Storgaard", "Rydevænget 81", "28995577",
                "storgaardkenny@gmail.com");
        c3.addOrder(r1);
        c3.addOrder(r2);
        c3.addOrder(r3);
        c3.addOrder(r4);

        service.saveStorage();
    }

}
