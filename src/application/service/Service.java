package application.service;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

import application.model.Beverage;
import application.model.Customer;
import application.model.DepositProduct;
import application.model.DraftBeerDispenser;
import application.model.GiftBox;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentType;
import application.model.Price;
import application.model.PriceList;
import application.model.Product;
import application.model.ProductType;
import application.model.Reservation;
import application.model.TourTeam;
import application.model.VoucherClip;
import gui.OrderPaneOrderDialog;
import storage.Storage;

public class Service
{

    private static Storage storage;

    public void setStorage(Storage storage)
    {
        Service.storage = storage;
    }

    // ------------------- Methods ----------------------------------

    public int clipsUsedInPeriod(LocalDate start, LocalDate end)
    {
        int result = 0;
        for (Order order : storage.getAllOrders())
        {

            LocalDate OrderValue = order.getDate().toLocalDate();
            if (OrderValue.isAfter(start) && OrderValue.isBefore(end)
                    || OrderValue.isEqual(end))
            {

                for (Payment payment : order.getPayments())
                {

                    result += payment.getClipsUsed();
                }

            }
        }

        return result;
    }

    public int createIdNo()
    {
        int idNo = 0;
        if (storage.getAllProducts().isEmpty())
        {
            idNo = 0;

        } else
        {
            idNo = storage.getAllProducts().get(storage.getAllProducts().size() - 1).getIdNo();
        }

        idNo = idNo + 1;
        return idNo;

    }

    public double calculateEarned(ArrayList<Order> order)
    {

        double result = 0;

        for (Order order2 : order)
        {
            for (OrderLine orderLines : order2.getOrderLines())
            {
                result += orderLines.getFullAmount();
            }
        }

        return result;

    }

    //------------------------------ Product // Product ----------------------------------------------------------

    public Product createProduct(String name, int productCount, ProductType productType)
    {
        if (name.length() < 1 || productCount < 0)
        {
            throw new RuntimeException("Skal skrive et navn");
        }

        int idNo = createIdNo();
        Product product = new Product(name, productCount, idNo, productType);
        storage.storeProduct(product);
        return product;
    }

    /**
     * Returns all products:
     */
    public ArrayList<Product> getAllProducts()
    {
        return storage.getAllProducts();
    }

    public void deleteProduct(Product product)
    {
        assert product != null;
        storage.removeProduct(product);
    }

    //------------------------------ Berverage // Product ----------------------------------------------------------

    /**
     *
     *
     */
    public Product createBerverage(String name, int productCount, ProductType productType, double alcoPer)
    {
        if (name.length() < 1 || productCount < 0 || alcoPer < 0)
        {
            throw new RuntimeException("Skal skrive et navn");
        }

        int idNo = createIdNo();
        Product berverage = new Beverage(name, productCount, idNo, productType, alcoPer);
        storage.storeProduct(berverage);
        return berverage;
    }

    //------------------------------ GiftBox // Product ----------------------------------------------------------
    /**
     *
     */

    public Product createGiftBox(String name, int productCount, ProductType productType,

            int beerAmount, int glassAmount,
            String boxType)
    {

        if (name.length() < 1 || productCount < 0 || beerAmount < 0 || glassAmount < 0)
        {
            throw new RuntimeException("Skal skrive et navn");
        }

        int idNo = createIdNo();
        Product giftBox = new GiftBox(name, productCount, idNo, productType, beerAmount, glassAmount,
                boxType);

        storage.storeProduct(giftBox);
        return giftBox;
    }

    // -------------------------- VoucherClip // Product ----------------------------------

    /**
     *
     *
     */
    public Product createVoucherClip(String name, int productCount, ProductType productType, int clipCount)
    {
        if (name.length() < 1 || productCount < 0 || clipCount < 1)
        {
            throw new RuntimeException("Skal skrive et navn");
        }

        int idNo = createIdNo();
        Product voucherClip = new VoucherClip(name, productCount, idNo, productType, clipCount);
        storage.storeProduct(voucherClip);
        return voucherClip;
    }

    // -------------------------- Deposit Product // Product ----------------------------------

    /**
     *
     *
     */

    public Product createDepositProduct(String name, int productCount, ProductType productType, String unit,
            double unitSize, double pant)
    {
        if (name.length() < 1 || productCount < 0)
        {
            throw new RuntimeException("Skal skrive et navn");
        }
        int idNo = createIdNo();
        Product p = new DepositProduct(name, productCount, idNo, productType, unit, unitSize, pant);
        storage.storeProduct(p);
        return p;

    }
    //------------------------------ PriceList ----------------------------------------------------------

    /**
     *
     *
     */
    public PriceList createPriceList(String name)
    {
        if (name.length() < 1)
        {

            throw new RuntimeException("Skal skrive et navn");
        }

        PriceList priceList = new PriceList(name);
        storage.storePriceList(priceList);
        return priceList;
    }

    public void deletePriceList(PriceList priceList)
    {
        assert priceList != null;
        assert storage.getAllPriceLists().contains(priceList);
        storage.removePriceList(priceList);

    }

    /**
     *
     *
     */
    public ArrayList<PriceList> getAllPriceLists()
    {
        return storage.getAllPriceLists();
    }

    public PriceList getReservationPriceList()
    {
        for (PriceList priceList : getAllPriceLists())
        {
            if (priceList.getName() == "Reservations produkter")
            {
                return priceList;
            }
        }
        return null;
    }

    // ------------------------- CUSTOMER ---------------------------

    public Customer createCustomer(String name, String address, String phoneNumber, String email)
    {
        Customer customer = new Customer(name, address, phoneNumber, email);
        storage.storeCustomer(customer);
        return customer;

    }

    public ArrayList<Customer> getAllCustomers()
    {
        return storage.getAllCustomers();
    }

    public void deleteCustomer(Customer customer)
    {

        storage.removeCustomer(customer);

    }

    //------------------------------ Reservation ----------------------------------------------------------

    /**
     *
     *
     */
    public Product createTourTeam(String name, int productCount, ProductType productType)
    {
        if (name.length() < 1)
        {
            throw new RuntimeException("Skal skrive et navn");
        }
        int idNo = createIdNo();
        Product p = new TourTeam(name, productCount, idNo, productType);
        storage.storeProduct(p);
        return p;
    }

    /**
     *
     *
     */
    public Product createDraftBeerDispenser(String name, int productCount, ProductType productType, boolean rentet)
    {
        if (name.length() < 1)
        {
            throw new RuntimeException("Skal skrive et navn");
        }
        int idNo = createIdNo();
        Product p = new DraftBeerDispenser(name, productCount, idNo, productType, rentet);
        storage.storeProduct(p);
        return p;
    }

    // ---------------------- ORDER --------------------------------------------------

    /**
     *
     *
     */

    public Order createOrder(LocalDateTime date)
    {

        Order order = new Order(date);
        storage.storeOrder(order);
        return order;
    }

    /**
    *
    *
    */
    public Order createOrderReservation(LocalDateTime date, LocalDate startDate, LocalDate endDate,
            boolean delivery)
    {
        Order order = new Reservation(date, startDate, endDate, delivery);
        storage.storeOrder(order);
        return order;
    }

    public ArrayList<Order> getAllOrders()
    {
        return storage.getAllOrders();
    }

    // --------------------- Payment ----------------------------------------------------

    /**
     *
     *
     */
    public Payment createPayment(PaymentType paymentType, double amount)
    {

        Payment payment = new Payment(paymentType, amount);
        storage.storePayment(payment);
        return payment;

    }

    /**
     *
     *
     */
    public ArrayList<Payment> getAllPayments()
    {
        return storage.getAllPayments();
    }

    // ---------------------- Product Type ------------------------------------

    public ArrayList<ProductType> getAllProductTypes()
    {

        ProductType[] productTypesArray = ProductType.class.getEnumConstants();
        ArrayList<ProductType> productTypes = new ArrayList<>(Arrays.asList(productTypesArray));

        return productTypes;
    }

    public ArrayList<PaymentType> getAllPaymentTypes()
    {

        PaymentType[] paymentTypesArray = PaymentType.class.getEnumConstants();
        ArrayList<PaymentType> paymentTypes = new ArrayList<>(Arrays.asList(paymentTypesArray));

        return paymentTypes;
    }

    // ---------------------------------------------------------------------------------------

    /**
     *
     *
     */

    public ArrayList<Order> getBookedProducts()
    {
        ArrayList<Order> bookedProducts = new ArrayList<>();

        for (Order order : getAllOrders())
        {
            if (order instanceof Reservation)
            {
                for (OrderLine orderline : order.getOrderLines())
                {
                    if (orderline.getProduct() instanceof DraftBeerDispenser)
                    {
                        bookedProducts.add(order);
                    }
                }
            }

        }
        return bookedProducts;
    }

    // ---------------------------------------------------------------------------------------

    /**
     *
     *
     */

    public ArrayList<Order> getBookedTours()
    {
        ArrayList<Order> bookedTours = new ArrayList<>();

        for (Order order : getAllOrders())
        {
            if (order instanceof Reservation)
            {
                if (((Reservation) order).getStartDate().isAfter(LocalDate.now().minusDays(1)))
                {

                    for (OrderLine orderline : order.getOrderLines())
                    {
                        if (orderline.getProduct() instanceof TourTeam)
                        {
                            bookedTours.add(order);
                        }
                    }
                }

            }

        }
        return bookedTours;
    }

    // ---------------------------------------------------------------------------------------

    /**
     *
     *
     */
    public void initStorage()
    {

    }

    public ArrayList<Price> getReservationDepositProduct()
    {
        ArrayList<Price> newProducts = new ArrayList<>();

        for (PriceList priceList : getAllPriceLists())
        {
            if (priceList.getName().equals("Reservations produkter"))

            {
                for (int i = 0; i < priceList.getPrices().size(); i++)
                {

                    if (priceList.getPrices().get(i).getProduct() instanceof DepositProduct)
                    {
                        newProducts.add(priceList.getPrices().get(i));

                    }
                }
            }
        }
        return newProducts;

    }

    public ArrayList<Price> getReservationDraftBeerDispenser()
    {
        ArrayList<Price> newProducts = new ArrayList<>();

        for (PriceList priceList : getAllPriceLists())
        {
            if (priceList.getName().equals("Reservations produkter"))

            {
                for (int i = 0; i < priceList.getPrices().size(); i++)
                {

                    if (priceList.getPrices().get(i).getProduct() instanceof DraftBeerDispenser
                            || priceList.getPrices().get(i).getProduct() instanceof TourTeam)
                    {
                        newProducts.add(priceList.getPrices().get(i));

                    }
                }
            }
        }
        return newProducts;

    }

    /**
     * Decreases the productCount of the Order when it is confirmed sold.
     * Checks to see if all payments connected to the order eguals the full price and if it does
     * will setPayed = true. If all payments is more than the full price a RuntimeException wil be thrown.
     */
    public void sellOrder(ArrayList<OrderLine> orderLines, ArrayList<Payment> payments, Order order)
    {
        double payed = 0;

        if (OrderPaneOrderDialog.isOrderDialog() != true)
        {
            for (OrderLine orderLine : orderLines)
            {

                orderLine.getProduct().decreaseProductCount(orderLine.getNumberOfProducts());
            }
        }

        for (Payment payment : payments)
        {
            payed += payment.getAmount();
        }

        if (payed > calculatePrice(orderLines) + calculateDeposit(orderLines))
        {

            throw new RuntimeException("Mere betalt end ordren kostede");

        }

        if (payed == calculatePrice(orderLines))
        {
            order.setPayed(true);
        }

        else
        {
            order.setPayed(false);
        }

    }

    /**
     * Calculates the price for all the OrderLines connected to the Order
     * @return result
     */
    public double calculatePrice(ArrayList<OrderLine> orderLines)
    {
        double result = 0;
        for (OrderLine orderLine : orderLines)
        {

            result += orderLine.getDiscountedPrice() * orderLine.getNumberOfProducts();
        }
        return result;
    }

    /**
     * Calculates the deposit for all the DepositProduct on the Orderlines connected to the Order
     * @return result
     */
    public double calculateDeposit(ArrayList<OrderLine> orderLines)
    {
        double result = 0;
        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct() instanceof DepositProduct)
            {
                result += ((DepositProduct) orderLine.getProduct()).getDeposit() * orderLine.getNumberOfProducts();
            }
        }
        return result;
    }

    // ------------- LOAD AND SAVE METHODS -------------------------

    public static void loadStorage()
    {
        try (FileInputStream fileIn = new FileInputStream("storage.ser"))
        {
            try (ObjectInputStream in = new ObjectInputStream(fileIn);)
            {
                storage = (Storage) in.readObject();
                System.out.println("Storage loaded from file storage.ser.");
            } catch (ClassNotFoundException ex)
            {
                System.out.println("Error loading storage object.");
                throw new RuntimeException(ex);
            }
        } catch (IOException ex)
        {
            System.out.println("Error loading storage object.");
            throw new RuntimeException(ex);
        }
    }

    /**
     * Saves the storage (including all objects in storage).
     */
    public static void saveStorage()
    {
        try (FileOutputStream fileOut = new FileOutputStream("storage.ser"))
        {
            try (ObjectOutputStream out = new ObjectOutputStream(fileOut))
            {
                out.writeObject(storage);
                System.out.println("Storage saved in file storage.ser.");
            }
        } catch (IOException ex)
        {
            System.out.println("Error saving storage object.");
            throw new RuntimeException(ex);
        }
    }

}
