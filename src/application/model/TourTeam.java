package application.model;

public class TourTeam extends Product
{

    public TourTeam(String name, int productCount, int idNo, ProductType productType)
    {
        super(name, productCount, idNo, productType);

    }

    @Override
    public String toString()
    {
        return super.getName();
    }

}
