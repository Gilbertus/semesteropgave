package application.model;

public class VoucherClip extends Product
{

    private int clipCount;

    public VoucherClip(String name, int productCount, int idNo, ProductType productType, int clipCount)
    {
        super(name, productCount, idNo, productType);
        assert clipCount > 0;
        this.clipCount = clipCount;
    }

    public int getClipCount()
    {
        return clipCount;
    }

}
