package application.model;

public class DepositProduct extends Product
{

    private String unit;
    private double unitSize;
    private double deposit;

    public DepositProduct(String name, int productCount, int idNo, ProductType productType, String unit,
            double unitSize, double deposit)
    {
        super(name, productCount, idNo, productType);
        assert deposit >= 0;
        this.unit = unit;
        this.unitSize = unitSize;
        this.deposit = deposit;
    }

    public String getUnit()
    {
        return unit;
    }

    public double getDeposit()
    {
        return deposit;
    }

    public double getUnitSize()
    {
        return unitSize;
    }

}
