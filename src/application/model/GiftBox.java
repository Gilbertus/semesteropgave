package application.model;

import java.util.ArrayList;

public class GiftBox extends Product
{

    private int glassAmount;
    private int beerAmount;
    private ArrayList<Product> bottles = new ArrayList<>();
    private String boxType;

    public GiftBox(String name, int productCount, int idNo, ProductType productType, int beerAmount, int glassAmount,
            String boxType)
    {
        super(name, productCount, idNo, productType);
        name = boxType + " " + name;
        assert glassAmount >= 0;
        assert beerAmount >= 0;
        assert boxType != null;
        this.glassAmount = glassAmount;
        this.beerAmount = beerAmount;
        this.boxType = boxType;
    }

    // ----------- Methods ----------------------------

    public void addBottle(Product product)
    {
        assert product.getProductType() == ProductType.BOTTLE;

        bottles.add(product);

    }

    // ------------ GETTERS AND SETTERS -------------------------

    public ArrayList<Product> getAllBottles()
    {
        return bottles;
    }

    public int getBeerAmount()
    {
        return beerAmount;
    }

    public int getGlassAmount()
    {
        return glassAmount;
    }

    public String getBoxType()
    {
        return boxType;
    }

}
