package application.model;

import java.io.Serializable;

public class Price implements Serializable
{

    // compostition: Price 0..* --> 1 PriceList
    private PriceList priceList;

    // association: Price 0..* --> 1 Product
    private Product product;

    private double price;

    public Price(double price, Product product)
    {
        assert price >= 0;
        assert product != null;
        this.price = price;
        this.setProduct(product);
    }

    // -------------- PRODUCT -------------------

    public Product getProduct()
    {
        return product;
    }

    public void setProduct(Product product)
    {
        if (product != null)
        {
            assert this.product == null;
            this.product = product;
            product.addPrice(this);
        } else
        {
            assert this.product != null;
            this.product.removePrice(this);
            this.product = null;
        }
    }

    // -------- PriceList ------------------------

    public PriceList getPriceList()
    {
        return priceList;
    }

    void setPriceList(PriceList priceList)
    {
        this.priceList = priceList;
    }

    // -------- GETTERS AND SETTERS -----------------

    public double getPrice()
    {
        return price;
    }

    public void setPrice(double price)
    {
        assert price >= 0;
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "Price:  " + price + "  " + product;
    }

}
