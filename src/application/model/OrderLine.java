package application.model;

import java.io.Serializable;

public class OrderLine implements Serializable
{
    // association: OrderLine 0..* --> Product 1
    private Product product;

    // compostion: OrderLine 0..* --> Order

    private int numberOfProducts;
    private double discountedPrice;
    private double fullAmount = discountedPrice * numberOfProducts;

    public OrderLine(int numberOfProducts, double discountedPrice, Product product)
    {
        this.numberOfProducts = numberOfProducts;
        this.discountedPrice = discountedPrice;
        this.fullAmount = numberOfProducts * discountedPrice;
        setProduct(product);
    }

    // -------- Product ---------------------------------

    public Product getProduct()
    {
        return product;
    }

    public void setProduct(Product product)
    {
        if (product != null)
        {
            assert this.product == null;
            this.product = product;
        } else
        {
            assert this.product != null;
            this.product = null;
        }

    }

    // --- GETTERS AND SETTERS ----------

    public int getNumberOfProducts()
    {
        return numberOfProducts;
    }

    public double getFullAmount()
    {
        return fullAmount;
    }

    public void setFullAmount()
    {
        fullAmount = this.getDiscountedPrice() * this.getNumberOfProducts();
    }

    public void setNumberOfProducts(int numberOfProducts)
    {
        this.numberOfProducts = numberOfProducts;
    }

    public double getDiscountedPrice()
    {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice)
    {
        this.discountedPrice = discountedPrice;
    }

    @Override
    public String toString()
    {
        return "OrderLine [product=" + product + ", numberOfProducts=" + numberOfProducts + ", discountedPrice="
                + discountedPrice + "]";
    }

}
