package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Product implements Serializable
{

    // association: Product 1 --> 0..* Price
    private final ArrayList<Price> prices = new ArrayList<>();

    private ProductType productType;
    private String name;
    private int productCount;
    private int idNo;

    /**
     *
     * @param name
     * @param productCount
     * @param idNo
     */

    public Product(String name, int productCount, int idNo, ProductType productType)
    {

        assert name.length() > 0;
        assert productCount >= 0;
        assert idNo >= 0;
        assert productType != null;

        this.name = name;
        this.productCount = productCount;
        this.idNo = idNo;
        this.productType = productType;

    }

    // ------------ Methods ------------------------------

    public void decreaseProductCount(int sold)
    {
        productCount = productCount - sold;
    }

    public String getName()
    {
        return name;
    }

    public int getProductCount()
    {
        return productCount;
    }

    public int getIdNo()
    {
        return idNo;
    }

    public void setProductCount(int productCount)
    {
        this.productCount = productCount;
    }

    @Override
    public String toString()
    {
        return name;
    }

    public ProductType getProductType()
    {
        return productType;
    }

    public void setProductType(ProductType productType)
    {
        this.productType = productType;
    }

    // ---------------- PRICE ----------------

    public ArrayList<Price> getPrices()
    {
        return new ArrayList<>(prices);
    }

    void addPrice(Price price)
    {
        prices.add(price);

    }

    void removePrice(Price price)
    {
        prices.remove(price);

    }

}
