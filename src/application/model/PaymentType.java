package application.model;

public enum PaymentType
{

    DEBIT("Kredit Kort"), CASH("Kontant"), MOBILEPAY("Mobile Pay"), VOUCHERCLIP("Klippekort");

    private String type;

    private PaymentType(String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return type;
    }

}
