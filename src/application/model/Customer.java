package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer implements Serializable
{
    private String name;
    private String address;
    private String phoneNumber;
    private String email;

    // association: Customer 0..1 <--> Order 0..*
    private ArrayList<Order> orders = new ArrayList<Order>();

    public Customer(String name, String address, String phoneNumber, String email)
    {
        assert phoneNumber.length() > 0;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    // ----------------- ORDER ----------------------------------------

    public ArrayList<Order> getOrders()
    {
        return new ArrayList<>(orders);
    }

    /** Pre: The order is not connected to a customer. */
    public void addOrder(Order order)
    {
        assert order.getCustomer() == null;
        orders.add(order);
        order.setCustomer(this);
    }

    /** Pre: The order is connected to this costumer. */
    public void removeOrder(Order order)
    {
        assert order.getCustomer() == this;
        orders.remove(order);
        order.setCustomer(null);
    }

}
