package application.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Reservation extends Order implements Serializable
{
    private LocalDate startDate;
    private LocalDate endDate;
    private boolean delivery;

    public Reservation(LocalDateTime date, LocalDate startDate, LocalDate endDate, boolean delivery)
    {
        super(date);
        assert startDate.isBefore(endDate) || startDate.isEqual(endDate);
        assert endDate.isAfter(startDate) || endDate.isEqual(startDate);
        this.startDate = startDate;
        this.endDate = endDate;
        this.delivery = delivery;
    }

    //----------- Getters ---------------------------

    public LocalDate getStartDate()
    {
        return startDate;
    }

    public LocalDate getEndDate()
    {
        return endDate;
    }

    public boolean isDelivery()
    {
        return delivery;
    }

    //----------- Setters ---------------------------

    public void setStartDate(LocalDate startDate)
    {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate)
    {
        this.endDate = endDate;
    }

    public void setDelivery(boolean delivery)
    {
        this.delivery = delivery;
    }

    @Override
    public String toString()
    {
        String res = "";
        for (int i = 0; i < getOrderLines().size(); i++)
        {
            if (getOrderLines().get(i) != null)
            {
                res = getOrderLines().get(i).getProduct() + " - Kunde:" + getCustomer().getName()
                        + " Start Dato: "
                        + startDate + " - Slut Dato "
                        + endDate + " - Skal leveres: " + delivery;
            }
        }

        return res;
    }

}
