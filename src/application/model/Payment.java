package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Payment implements Serializable
{
    // association: Payment 0..* --> 1..* Order
    private final ArrayList<Order> orders = new ArrayList<>();

    private PaymentType paymentType;
    private double amount;
    private int clipsUsed;

    public Payment(PaymentType paymentType, double amount)
    {
//        assert amount > 0;
        assert paymentType != null;
        this.paymentType = paymentType;
        this.amount = amount;

    }

    // ------------------ Methods ------------------------------

    /**
     * Calculates the prices the vouchers corresponds to
     * and sets the clipsUsed so it can be used for statistics
     * @param order != null
     */
    public void payedWithVoucher(Order order)
    {

        Double tempAmount = 0.0;
        if (paymentType == PaymentType.VOUCHERCLIP)
        {
            clipsUsed += amount;
            tempAmount = amount * order.pricePerTap();
            setAmount(tempAmount);
        }

    }

    // ---------- Payment ---------------------------

    public ArrayList<Order> getOrders()
    {
        return new ArrayList<>();
    }

    void addOrder(Order order)
    {
        orders.add(order);
    }

    void removeOrder(Order order)
    {
        orders.remove(order);
    }
    // ------ SETTERS AND GETTERS ----------------

    public PaymentType getPaymentType()
    {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType)
    {
        this.paymentType = paymentType;
    }

    public double getAmount()
    {
        return amount;
    }

    public void setAmount(double amount)
    {
        this.amount = amount;
    }

    public int getClipsUsed()
    {
        return clipsUsed;
    }

    public void setClipsUsed(int clipsUsed)
    {
        this.clipsUsed = clipsUsed;
    }

    @Override
    public String toString()
    {
        return "paymentType=" + paymentType + ", amount=" + amount;
    }

}
