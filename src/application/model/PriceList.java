package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class PriceList implements Serializable
{

    // composition: PriceList 1 --> 0..* Price
    private final ArrayList<Price> prices = new ArrayList<>();

    private String name;

    public PriceList(String name)
    {
        assert name.length() > 0;
        this.name = name;
    }

    // ------------- PRICE --------------------------

    public ArrayList<Price> getPrices()
    {
        return new ArrayList<>(prices);
    }

    /**
     *
     * @param price > 0
     * @param product != null
     */
    public Price createPrice(double price, Product product)
    {
        assert price >= 0;

        Price price1 = new Price(price, product);
        prices.add(price1);
        price1.setPriceList(this);
        return price1;
    }

    public void removePrice(Price price)
    {
        assert price.getPriceList() == this;
        prices.remove(price);
        price.setPriceList(null);
    }

    // ------------ GETTERS AND SETTERS -------------------------

    public String getName()
    {
        return name;
    }

    @Override
    public String toString()
    {
        return name;
    }

}
