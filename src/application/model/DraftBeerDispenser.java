package application.model;

public class DraftBeerDispenser extends Product
{

    private boolean rentet;

    public DraftBeerDispenser(String name, int productCount, int idNo, ProductType productType, boolean rentet)
    {
        super(name, productCount, idNo, productType);
        this.rentet = rentet;
    }

    public boolean isRentet()
    {
        return rentet;
    }

}
