package application.model;

public class Beverage extends Product
{

    private double alcoPer;

    public Beverage(String name, int productCount, int idNo, ProductType productType, double alcoPer)
    {
        super(name, productCount, idNo, productType);
        this.alcoPer = alcoPer;
    }

    // ------------ GETTERS AND SETTERS -------------------------

    public double getAlcoPer()
    {
        return alcoPer;
    }

}
