package application.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Order implements Serializable
{

    // association: Order 1..* --> Payment 0..*
    private final ArrayList<Payment> payments = new ArrayList<>();

    // association: Order 1 --> 0..* Orderlines
    private ArrayList<OrderLine> orderLines = new ArrayList<>();

    // association: Order 0..* <--> Customer 0..1
    private Customer customer;

    private LocalDateTime date;
    private boolean isPayed;

    public Order(LocalDateTime date)
    {
        assert date != null;
        this.date = date;

    }

    // ------------- Methods -----------------------

    /**
     * Calculates what the price for on tap beer on an order is
     * @return price of one tap beer
     */
    public double pricePerTap()
    {

        double result = 0;

        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct().getProductType() == ProductType.TAPBEER)
            {

                result += orderLine.getDiscountedPrice() * orderLine.getNumberOfProducts();
            }
        }

        if (result > 0)
        {
            result = result / amountOfTapBeer();
        }
        return result;
    }

    /**
     * Amount of tapbeer on the Order
     */
    public int amountOfTapBeer()
    {

        int result = 0;

        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct().getProductType() == ProductType.TAPBEER)
            {

                result += orderLine.getNumberOfProducts();
            }
        }
        return result;

    }

    /**
     *
     * @param product != null
     * @return true if the product is on the order
     */
    public boolean includeProduct(Product product)
    {
        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct().equals(product))
            {
                return true;
            }
        }

        return false;

    }

    /**
     *
     * @param product != null
     * @return price on product * numberOfProduct
     */
    public double amountEarned(Product product)
    {

        double amountEarned = 0;

        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct().equals(product))
            {

                amountEarned += orderLine.getDiscountedPrice() * orderLine.getNumberOfProducts();
            }

        }

        return amountEarned;

    }

    /**
     *
     * @param product != null
     * @return returns all numberOfProducts sold of the product
     */
    public int amountSold(Product product)
    {

        int amountSold = 0;

        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct().equals(product))
            {

                amountSold += orderLine.getNumberOfProducts();
            }

        }

        return amountSold;

    }

    /**
     * Calculates the price for all the OrderLines connected to the Order
     * @return result
     */
    public double calculatePrice()
    {
        double result = 0;
        for (OrderLine orderLine : orderLines)
        {

            result += orderLine.getDiscountedPrice() * orderLine.getNumberOfProducts();
        }
        return result;
    }

    /**
     * Calculates the deposit for all the DepositProduct on the Orderlines connected to the Order
     * @return result
     */
    public double calculateDeposit()
    {
        double result = 0;
        for (OrderLine orderLine : orderLines)
        {
            if (orderLine.getProduct() instanceof DepositProduct)
            {
                result += ((DepositProduct) orderLine.getProduct()).getDeposit() * orderLine.getNumberOfProducts();
            }
        }
        return result;
    }

    public double calculatePriceWhenDelivered()
    {

        double allPayed = 0;
        double moneyToPay = 0;

        for (OrderLine orderLine : orderLines)
        {

            if (orderLine.getProduct() instanceof DepositProduct)
            {
                allPayed += orderLine.getFullAmount();

            }

        }

        moneyToPay = allPayed - calculatePayed();

        return moneyToPay;

    }

    /**
     * Calculates how much is payed from all payments connected to the Order
     * @return
     */
    public double calculatePayed()
    {

        double result = 0;
        for (Payment payment : this.getPayments())
        {
            result += payment.getAmount();
        }

        return result;
    }

    /**
     * Calculates what remains to be payed from the full price of the order minus all payments connected to the order
     * @return calculatePrice() - calculatePayed()
     */
    public double calculateRemainingPrice()
    {
        return calculatePrice() - calculatePayed();
    }

    /**
     * Creates String with all Products, their prices and amount.
     * @return result String
     */
    public String printReciept()
    {

        String result = "";

        for (OrderLine orderLine : orderLines)
        {
            result += orderLine.getProduct().getName() + "   Pris: " + orderLine.getDiscountedPrice() + "   Antal: "
                    + orderLine.getNumberOfProducts() + "\n";
        }

        result += "Total pris:  " + calculatePrice();
        return result;
    }

    // ------------ OrderLine -------------------------

    public ArrayList<OrderLine> getOrderLines()
    {
        return new ArrayList<>(orderLines);
    }

    public OrderLine createOrderLine(int numberOfProducts, double discountedPrice, Product product)
    {
        assert discountedPrice >= 0;
        assert numberOfProducts >= 0;
        assert product != null;
        OrderLine orderLine = new OrderLine(numberOfProducts, discountedPrice, product);
        orderLine.setFullAmount();
        orderLines.add(orderLine);
        return orderLine;
    }

    public void removeOrderLine(OrderLine orderLine)
    {
        assert orderLines.contains(orderLine);
        orderLines.remove(orderLine);
    }

    // ---------- Customers --------------------------------

    public Customer getCustomer()
    {
        return customer;
    }

    /** Note: customer is nullable. */
    void setCustomer(Customer customer)
    { // package visibility
        this.customer = customer;
    }

    // ---------- Payments --------------------------------

    public ArrayList<Payment> getPayments()
    {
        return new ArrayList<>(payments);
    }

    public void addPayment(Payment payment)
    {

        assert calculatePrice() + calculateDeposit() >= payment.getAmount() + calculatePayed();

        assert !payment.getOrders().contains(this);
        payments.add(payment);
        payment.addOrder(this);
    }

    public void removePayment(Payment payment)
    {
        assert payment.getOrders().contains(this);
        payments.remove(payment);
        payment.removeOrder(this);
    }

    public boolean isPayed()
    {
        return isPayed;
    }

    public void setPayed(boolean isPayed)
    {
        this.isPayed = isPayed;
    }

    public LocalDateTime getDate()
    {
        return date;
    }

    @Override
    public String toString()
    {
        return "Order [payments=" + payments + ", orderLines=" + orderLines + ", date=" + date + ", isPayed=" + isPayed
                + "]";
    }

}
