package application.model;

public enum ProductType
{
    BOTTLE("Flaske"), TAPBEER("Fadøl"), SPIRIT("Spiritus"), GIFTBOX("Gaveæske"), KEG("Fustage"), CARBON(
            "Kulsyre"), MISC("Diverse"), DISPENSER("Anlæg"), TOUR("Rundvisning"), VOUCHERCLIP("Klippekort");

    private String value;

    ProductType(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return value;
    }

    @Override
    public String toString()
    {
        return value;
    }

}
