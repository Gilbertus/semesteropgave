package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;

import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentType;
import application.model.PriceList;
import application.model.Product;
import application.model.ProductType;
import application.service.Service;
import storage.Storage;

public class OrderTest
{

    private static Storage storage;

    Order o1;

    PriceList pl1;
    PriceList pl2;

    Payment pay1;
    Payment pay2;
    Payment pay3;
    Payment pay4;
    Payment pay5;
    Payment pay6;
    Payment pay7;

    Product p1;
    Product p2;
    Product p3;
    Product p4;
    Product p5;
    Product p6;
    Product p7;

    Product d1;
    Product d2;
    Product d3;
    Product d4;
    Product d5;
    Product d6;
    Product d7;

    Product k1;

    OrderLine oL1;
    OrderLine oL2;
    OrderLine oL3;
    OrderLine oL4;

    @Before
    public void oneTimeSetUp()
    {

        storage = Storage.getInstance();

        Service service = new Service();
        service.setStorage(storage);

        pay1 = service.createPayment(PaymentType.CASH, 200);
        pay2 = service.createPayment(PaymentType.DEBIT, 350);
        pay3 = service.createPayment(PaymentType.MOBILEPAY, 233);

        pay4 = service.createPayment(PaymentType.CASH, 500);
        pay5 = service.createPayment(PaymentType.DEBIT, 1);

        o1 = service.createOrder(LocalDateTime.now());

//        pl1 = service.createPriceList("Standard");
//        pl2 = service.createPriceList("Fredag");

        p1 = service.createBerverage("Klosterbryg Fadøl", 500, ProductType.TAPBEER, 7);

        p2 = service.createBerverage("Jazz Classic Fadøl", 500, ProductType.TAPBEER, 7);

        p3 = service.createBerverage("Extra Pilsner Fadøl", 500, ProductType.TAPBEER, 7);

        p4 = service.createBerverage("Celebration Fadøl", 500, ProductType.TAPBEER, 7);

        p5 = service.createBerverage("Klosterbryg Flaske", 500, ProductType.BOTTLE, 7);

        p6 = service.createBerverage("Sweet Georgia Brown Flaske", 500, ProductType.BOTTLE, 7);

        p7 = service.createBerverage("Extra Pilsner Flaske", 500, ProductType.BOTTLE, 7);

        d1 = service.createDepositProduct("Fustage KlosterBryg", 20, ProductType.KEG, "Liter", 20, 200);
//        pl1.createPrice(775, d1);
        d2 = service.createDepositProduct("Fustage Jazz Classic", 20, ProductType.KEG, "Liter", 25, 200);
//        pl1.createPrice(625, d2);
        d3 = service.createDepositProduct("Fustage Extra Pilsner", 20, ProductType.KEG, "Liter", 25, 200);
//        pl1.createPrice(575, d3);
        d4 = service.createDepositProduct("Fustage Celebration", 20, ProductType.KEG, "Liter", 20, 200);
//        pl1.createPrice(775, d4);
        d5 = service.createDepositProduct("Fustage Blondie", 20, ProductType.KEG, "Liter", 25, 200);
//        pl1.createPrice(700, d5);
        d6 = service.createDepositProduct("Fustage Forårsbryg", 20, ProductType.KEG, "Liter", 20, 200);
//        pl1.createPrice(775, d6);
        d7 = service.createDepositProduct("Fustage India Pale Ale", 20, ProductType.KEG, "Liter", 20, 200);
//        pl1.createPrice(775, d7);

        k1 = service.createDepositProduct("Kulsyre", 10, ProductType.CARBON, "Kg", 6, 1000);
//        pl1.createPrice(400, k1);

    }

    /**
     * TestCase 1 pricePrTab
     */

    @Test
    public void testCaseTapPrice1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(30, o1.pricePerTap(), 0.0);
    }

    @Test
    public void testCaseTapPrice2()
    {
        o1.createOrderLine(2, 30, p6);
        o1.createOrderLine(2, 30, p7);
        assertEquals(0, o1.pricePerTap(), 0.0);
    }

    @Test()
    public void testCaseTapPrice3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p6);
        assertEquals(30, o1.pricePerTap(), 0.0);
    }

    @Test
    public void testCaseTapPrice4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 23, p2);
        assertEquals(24.33, o1.pricePerTap(), 0.1);
    }

    /**
     * TestCase 2 amountOfTapBeer
     */

    @Test
    public void amountOfTapBeer1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(4, o1.amountOfTapBeer(), 0.0);
    }

    @Test
    public void amountOfTapBeer2()
    {
        o1.createOrderLine(2, 30, p6);
        o1.createOrderLine(2, 30, p7);
        assertEquals(0, o1.amountOfTapBeer(), 0.0);
    }

    @Test()
    public void amountOfTapBeer3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p6);
        assertEquals(1, o1.amountOfTapBeer(), 0.0);
    }

    @Test
    public void amountOfTapBeer4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 23, p2);
        assertEquals(3, o1.amountOfTapBeer(), 0.1);
    }

    /**
     * TestCase 3 includeProduct
     */

    @Test
    public void includeProduct1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(true, o1.includeProduct(p1));
    }

    @Test
    public void includeProduct2()
    {
        o1.createOrderLine(2, 30, p6);
        o1.createOrderLine(2, 30, p7);
        assertEquals(true, o1.includeProduct(p6));
    }

    @Test()
    public void includeProduct3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p6);
        assertEquals(false, o1.includeProduct(p3));
    }

    @Test
    public void includeProduct4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 23, p4);
        assertEquals(true, o1.includeProduct(p4));
    }

    /**
     * TestCase 4 amountEarned()
     */

    @Test
    public void amountEarned1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(60, o1.amountEarned(p1), 0.0);
    }

    @Test
    public void amountEarned2()
    {
        o1.createOrderLine(2, 30, p6);
        o1.createOrderLine(2, 30, p7);
        assertEquals(0, o1.amountEarned(p2), 0.0);
    }

    @Test()
    public void amountEarned3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p3);
        assertEquals(360, o1.amountEarned(p3), 0.0);
    }

    @Test
    public void amountEarned4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 23, p4);
        assertEquals(23, o1.amountEarned(p4), 0.0);
    }

    /**
     * TestCase 5 amountEarned()
     */

    @Test
    public void amountSold1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(2, o1.amountSold(p1), 0.0);
    }

    @Test
    public void amountSold2()
    {
        o1.createOrderLine(2, 30, p6);
        o1.createOrderLine(2, 30, p7);
        assertEquals(0, o1.amountSold(p2), 0.0);
    }

    @Test()
    public void amountSold3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p3);
        assertEquals(12, o1.amountSold(p3), 0.0);
    }

    @Test
    public void amountSold4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 0, p4);
        assertEquals(1, o1.amountSold(p4), 0.0);
    }

    /**
     * TestCase 5 calculatePrice
     */

    @Test
    public void calculatePrice1()
    {
        o1.createOrderLine(2, 30, p1);
        o1.createOrderLine(2, 30, p2);
        assertEquals(120, o1.calculatePrice(), 0.0);
    }

    @Test
    public void calculatePrice2()
    {
        o1.createOrderLine(10, 30, p6);
        o1.createOrderLine(1, 30, p7);
        assertEquals(330, o1.calculatePrice(), 0.0);
    }

    @Test()
    public void calculatePrice3()
    {
//        o1.createOrderLine(1, 30, p1);
//        o1.createOrderLine(12, 30, p6);
        assertEquals(0, o1.calculatePrice(), 0.0);
    }

    @Test
    public void calculatePrice4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 0, p4);
        assertEquals(50, o1.calculatePrice(), 0.0);
    }

    /**
     * TestCase 6 calculateDeposit
     */

    @Test
    public void calculateDeposit1()
    {
        o1.createOrderLine(3, 30, d1);
        o1.createOrderLine(2, 30, d4);
        assertEquals(1000, o1.calculateDeposit(), 0.0);
    }

    @Test
    public void calculateDeposit2()
    {
        o1.createOrderLine(2, 30, p2);
        o1.createOrderLine(2, 30, p1);
        assertEquals(0, o1.calculateDeposit(), 0.0);
    }

    @Test()
    public void calculateDeposit3()
    {
        o1.createOrderLine(2, 30, k1);
        o1.createOrderLine(1, 30, p3);
        assertEquals(2000, o1.calculateDeposit(), 0.0);
    }

    @Test
    public void calculateDeposit4()
    {
        o1.createOrderLine(1, 25, d1);
        assertEquals(200, o1.calculateDeposit(), 0.0);
    }

    /**
     * TestCase 7 calculatePayed()
     */

    @Test
    public void calculatePayed1()
    {
        o1.createOrderLine(20, 30, p1);
        o1.createOrderLine(20, 30, p2);

        o1.addPayment(pay1);
        o1.addPayment(pay2);
        assertEquals(550, o1.calculatePayed(), 0.0);
    }

    @Test
    public void calculatePayed2()
    {
        o1.createOrderLine(10, 30, p6);
        o1.createOrderLine(5, 30, p6);
        o1.addPayment(pay2);
        assertEquals(350, o1.calculatePayed(), 0.0);
    }

    @Test(expected = AssertionError.class)
    public void calculatePayed3()
    {
        o1.createOrderLine(1, 30, p1);
        o1.createOrderLine(12, 30, p6);
        o1.addPayment(pay3);
        o1.addPayment(pay2);

        assertEquals(583, o1.calculatePayed(), 0.0);
    }

    @Test
    public void calculatePayed4()
    {
        o1.createOrderLine(2, 25, p1);
        o1.createOrderLine(1, 23, p2);
        assertEquals(0, o1.calculatePayed(), 0.0);
    }

    /**
     * TestCase 7 calculateRemainingPrice()
     */

    @Test
    public void calculateRemainingPrice1()
    {
        o1.createOrderLine(40, 25, p1);

        o1.addPayment(pay4);

        assertEquals(500.0, o1.calculateRemainingPrice(), 0.0);
    }

    @Test
    public void calculateRemainingPrice2()
    {
        o1.createOrderLine(1, 1, p6);
        o1.addPayment(pay5);
        assertEquals(0, o1.calculateRemainingPrice(), 0.0);
    }

    @Test()
    public void calculateRemainingPrice3()
    {
        o1.createOrderLine(20, 25, p1);

        o1.addPayment(pay4);

        assertEquals(0, o1.calculateRemainingPrice(), 0.0);
    }

    @Test
    public void calculateRemainingPrice4()
    {

        assertEquals(0, o1.calculateRemainingPrice(), 0.0);
    }

    /**
     * TestCase 7 sellOrder()()
     */

}
