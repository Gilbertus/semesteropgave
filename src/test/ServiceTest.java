package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.model.Product;
import application.model.ProductType;
import application.service.Service;
import storage.Storage;

public class ServiceTest
{
    Storage storage = Storage.getInstance();
    Service service = new Service();
//    service service = new service();

    // ------------ CREATE PRODUCT ---------------------------

    @Test
    public void testCreateProduct0()
    {
        Product p1 = service.createProduct("Klosterbryg", 50, ProductType.BOTTLE);
        assertEquals("Klosterbryg", p1.getName());
        assertEquals(50, p1.getProductCount());
    }

    @Test
    public void testCreateProduct1()
    {
        Product p1 = service.createProduct("K", 0, ProductType.BOTTLE);
        assertEquals("K", p1.getName());
        assertEquals(0, p1.getProductCount());
    }

    @Test(expected = RuntimeException.class)
    public void testCreateProduct2()
    {
        Product p1 = service.createProduct("Klosterbryg", -1, ProductType.BOTTLE);
        assertEquals("Klosterbryg", p1.getName());
        assertEquals(-1, p1.getProductCount());
    }

    @Test(expected = RuntimeException.class)
    public void testCreateProduct3()
    {
        Product p1 = service.createProduct("", 50, ProductType.BOTTLE);
        assertEquals("Klosterbryg", p1.getName());
        assertEquals(50, p1.getProductCount());
    }

    // ---------------- CREATE PRICELIST ------------------------------

}
