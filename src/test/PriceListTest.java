package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import application.model.PriceList;
import application.model.Product;
import application.model.ProductType;
import application.service.Service;

public class PriceListTest
{

    Service service = new Service();

    Product p1 = service.createProduct("Klosterbryg", 20, ProductType.BOTTLE);
    PriceList pL1 = service.createPriceList("Fredag");

    @Test
    public void testCreatePrice0()
    {
        pL1.createPrice(50, p1);
        assertEquals(50, pL1.getPrices().get(0).getPrice(), 0.0);
    }

    @Test(expected = AssertionError.class)
    public void testCreatePrice1()
    {
        pL1.createPrice(-1, p1);
        assertEquals(-1, pL1.getPrices().get(0).getPrice(), 0.0);
    }

    @Test
    public void testCreatePrice2()
    {
        pL1.createPrice(0, p1);
        assertEquals(0, pL1.getPrices().get(0).getPrice(), 0.0);
    }

}
