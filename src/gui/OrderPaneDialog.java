package gui;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OrderPaneDialog extends Stage
{
    private double discountedPrice;
    private int numberOfProducts;
    private final Controller controller = new Controller();
    private boolean isConfirmed = false;

    private final Label lblError = new Label("Ugyldigt Input");

    // -----------------------initialize new stage and scene--------------

    public OrderPaneDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Antal");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private final TextField txfNumberOfProducts = new TextField();
    private final TextField txfDiscountedPrice = new TextField();

    Button btnCancel = new Button("Cancel");
    Button btnOK = new Button("OK");

    HBox buttonBox = new HBox(10);

    private void initContent(GridPane pane)
    {

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------
        Label lbl1 = new Label("Antal: ");
        pane.add(lbl1, 0, 0);

        Label lbl2 = new Label("Nypris Per Produkt: ");
        pane.add(lbl2, 0, 1);

        pane.add(lblError, 1, 2);
        lblError.setVisible(false);
        lblError.setTextFill(Color.RED);

        txfNumberOfProducts.setText("1");
        txfDiscountedPrice.setText(Double.toString(OrderPane.getPriceRowDataSingle()));

        // -------------------------- Text Fields ----------------------------

        pane.add(txfNumberOfProducts, 1, 0);
        pane.add(txfDiscountedPrice, 1, 1);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 0, 2);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);

        Button btnCancel = new Button("Cancel");
        buttonBox.getChildren().add(btnCancel);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelAction());

        Button btnOK = new Button("OK");
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.okAction());

        // -------------------------- Key Event --------------------------------

        /**
         * Keypress UP: Increases numberOfProducts with 1 each press
         * Keypress DOWN: Decreases numberOfProducts with 1 each press
         * KeyPress ENTER: call Controller.okAction()
         */

        txfNumberOfProducts.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.UP)
            {
                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());
                numberOfProducts++;
                txfNumberOfProducts.setText("" + numberOfProducts);
            }
            if (e.getCode() == KeyCode.DOWN)
            {

                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());
                if (numberOfProducts > 1)
                {
                    numberOfProducts--;
                    txfNumberOfProducts.setText("" + numberOfProducts);
                }
            }

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

        txfDiscountedPrice.setOnKeyPressed(e -> {

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

    }

    // ------------- GETTERS AND SETTERS -------------------------------------

    public double getDiscountedPrice()
    {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice)
    {
        this.discountedPrice = discountedPrice;
    }

    public int getNumberOfProducts()
    {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts)
    {
        this.numberOfProducts = numberOfProducts;
    }

    public boolean isConfirmed()
    {
        return isConfirmed;
    }

    // -------------------------------------------------------------------------

    private class Controller
    {

        /**
         * Closes OrderPaneDialog. Sets isConfirmed to false
         */
        public void cancelAction()
        {
            isConfirmed = false;
            OrderPaneDialog.this.hide();
        }

        /**
         * Sets static fields with information written in the text fields so they are reachable from OrderPane
         * Sets isConfirmed to true
         * Throws ParseException if text field test cant be parsed and IllegalArugmetnException if textField is letters
         * not numbers
         */
        public void okAction()
        {

            try
            {
                discountedPrice = Double.parseDouble(txfDiscountedPrice.getText());
                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());

                if (discountedPrice < 0)
                {
                    lblError.setVisible(true);
                    txfDiscountedPrice.requestFocus();
                    throw new IllegalArgumentException();
                }

                if (numberOfProducts < 0)
                {
                    lblError.setVisible(true);
                    txfNumberOfProducts.requestFocus();
                    throw new IllegalArgumentException();
                }

                isConfirmed = true;
                OrderPaneDialog.this.hide();
            }

            catch (ParseException pE)
            {

            }

            catch (IllegalArgumentException iAE)
            {

            }

        }
    }

}
