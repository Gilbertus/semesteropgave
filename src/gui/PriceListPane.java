package gui;

import java.util.ArrayList;

import application.model.Price;
import application.model.PriceList;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;

public class PriceListPane extends GridPane
{
    Service service = new Service();
    private final Controller controller = new Controller();
    private final Button addbtn = new Button("Ny Prisliste");
    private final Button rembtn = new Button("Fjern Prisliste");
    private final Label label = new Label("Priser");
    private final Label lbl1 = new Label("Prislister");
    private final Separator h1Separator = new Separator();
    private final Separator v1Separator = new Separator();
    private final Separator v2Separator = new Separator();
    private final TableView<Price> allPrices = new TableView<Price>();
    private final ListView<PriceList> lvwPriceList = new ListView<>();

    public PriceListPane()
    {
        this.initContent();
    }

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(20));
        this.setHgap(10);
        this.setVgap(10);

        // --------------------- Labels ---------------------------------------

        lbl1.setFont(Font.font(23));
        this.add(lbl1, 0, 0);

        // ------------------- BUTTON --------------------------------------

        this.add(addbtn, 0, 10);
        GridPane.setHalignment(addbtn, HPos.RIGHT);
        this.add(rembtn, 1, 10);
        rembtn.setOnAction(event -> controller.remAction());
        addbtn.setOnAction(event -> controller.createAction());

        // ------------------- SEPERATORS --------------------------------------

        h1Separator.orientationProperty().set(Orientation.HORIZONTAL);
        this.add(h1Separator, 0, 1, 9, 1);
        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v1Separator, 3, 0, 1, 78);
        v2Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v2Separator, 8, 0, 1, 78);

        // -------------------- Table Column Prices --------------------------------------

        label.setFont(Font.font(23));
        this.add(label, 4, 0);

        allPrices.setEditable(true);
        allPrices.setPlaceholder(new Label("Ingen prisliste markeret."));

        TableColumn<Price, String> product = new TableColumn<Price, String>("Produkt");
        product.setMinWidth(200);
        product.setCellValueFactory(new PropertyValueFactory<Price, String>("product"));

        TableColumn<Price, Double> price = new TableColumn<Price, Double>("Pris");
        price.setMinWidth(200);
        price.setCellValueFactory(new PropertyValueFactory<>("price"));
        price.setCellFactory(TextFieldTableCell.<Price, Double>forTableColumn(new DoubleStringConverter()));

        price.setOnEditCommit((CellEditEvent<Price, Double> event) -> {

            Price p = allPrices.getSelectionModel().getSelectedItem();
            TablePosition<Price, Double> pos = event.getTablePosition();
            int row = pos.getRow();
            try
            {
                Double newPrice = event.getNewValue();

                Price currentPrice = event.getTableView().getItems().get(row);
                currentPrice.setPrice(newPrice);
            }
            catch (AssertionError i)
            {
                System.out.println("AssertionError: PriceListPane: 100 -> 125");
                allPrices.refresh();
            }
            allPrices.requestFocus();
            allPrices.getSelectionModel().select(row);

        });

        // priceObject used in Lambda function to get value IdNo from Product in Price constructor - Parsed to String and
        // afterwards packed in a ReadOnlyStringWrapper to convert String to an ObservableValue<String>.
        TableColumn<Price, String> productID = new TableColumn<Price, String>("Produkt ID");
        productID.setMinWidth(200);
        productID.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        Integer.toString(priceObject.getValue().getProduct().getIdNo())));

        allPrices.getColumns().addAll(product, price, productID);
        allPrices.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        allPrices.setTooltip(new Tooltip("Dobbeltklik pris for at editere"));

        this.add(allPrices, 4, 2, 3, 76);

        // --------------------- Change Listener -------------------------------

        ChangeListener<PriceList> priceListener = (ov, o, n) -> controller.setLvPrices();
        lvwPriceList.getSelectionModel().selectedItemProperty().addListener(priceListener);

        // --------------------- List View PriceList ----------------------------

        this.add(lvwPriceList, 0, 2, 3, 8);
        lvwPriceList.setPrefWidth(250);
        lvwPriceList.setPrefHeight(200);
        lvwPriceList.getItems().setAll(controller.priceLists);
        controller.fillLvPriceList();
    }

    /**
     * Updates when PriceListPane is choosen
     */
    public void updateControls()
    {
        controller.fillLvPriceList();
        lvwPriceList.refresh();
        allPrices.refresh();
    }

    // ----------------------- controller ----------------------------

    private class Controller
    {
        private final ArrayList<PriceList> priceLists = new ArrayList<>();

        /**
         * Opens PriceListDialog for creation of new PriceList
         */
        public void createAction()
        {
            Stage stage = new Stage();
            PriceListDialog dialog = new PriceListDialog(stage);
            dialog.showAndWait();
            // ... wait for the dialog to close
            fillLvPriceList();
        }

        /**
         * Remove selected Pricelist with Alert confirmations window.
         * Updates the list after deletion.
         * Error if trying to delete last PriceList
         */
        public void remAction()
        {
            if (service.getAllPriceLists().size() > 1)
            {
                if (lvwPriceList.getSelectionModel().getSelectedItem() != null)
                {
                    PriceList tempPriceList = lvwPriceList.getSelectionModel().getSelectedItem();
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Bekræftigelse vindue");
                    alert.setHeaderText("Prisliste: " + tempPriceList.getName());
                    alert.setContentText(
                            "Er du sikker på du vil slette den valgte prisliste?: \n"
                                    + tempPriceList.getName());

                    alert.showAndWait();

                    if (alert.getResult() == ButtonType.OK)
                    {
                        for (Price price : tempPriceList.getPrices())
                        {
                            tempPriceList.removePrice(price);
                        }
                        service.deletePriceList(tempPriceList);
                        controller.fillLvPriceList();
                        lvwPriceList.refresh();
                    }
                }
            }

            else
            {
                PriceList tempPriceList = lvwPriceList.getSelectionModel().getSelectedItem();
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Fejl Vindue");
                alert.setHeaderText("Prisliste: " + tempPriceList.getName());
                alert.setContentText(
                        "Kunne ikke slette: "
                                + tempPriceList.getName() + "\nDer skal være mindst en prisliste i systemet");

                alert.showAndWait();
            }

        }

        /**
         * Fills allPrices table view with all prices and connected products on the choosen priceList
         */
        public void setLvPrices()
        {
            if (lvwPriceList.getSelectionModel().getSelectedItem() != null)
            {
                allPrices.setItems(FXCollections
                        .observableArrayList(lvwPriceList.getSelectionModel().getSelectedItem().getPrices()));
            }
        }

        /**
         * Fills the ListView with allPriceLists in storage
         */
        public void fillLvPriceList()
        {
            lvwPriceList.getItems().setAll(service.getAllPriceLists());

        }
    }

}
