package gui;

import application.service.Service;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class CustomerDialog extends Stage
{

    Service service = new Service();
    private final Label lblError = new Label();
    private final Controller controller = new Controller();
    private final TextField txfName = new TextField();
    private final TextField txfAddress = new TextField();
    private final TextField txfPhone = new TextField();
    private final TextField txfEmail = new TextField();

    Button btnCancel = new Button("Cancel");
    Button btnOK = new Button("OK");

    HBox buttonBox = new HBox(10);

    // -----------------------initialize new stage and scene--------------

    public CustomerDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Ny Kunde");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private void initContent(GridPane pane)
    {

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------

        Label lbl1 = new Label("Navn: ");
        Label lbl2 = new Label("Addresse: ");
        Label lbl3 = new Label("Tlf Nummer: ");
        Label lbl4 = new Label("Email: ");
        lblError.setText("Skal have et navn");
        pane.add(lbl1, 0, 0);
        pane.add(lbl2, 0, 1);
        pane.add(lbl3, 0, 2);
        pane.add(lbl4, 0, 3);
        pane.add(lblError, 1, 3);

        lblError.setTextFill(Color.RED);
        lblError.setVisible(false);

        // -------------------------- Text Fields ----------------------------

        pane.add(txfName, 1, 0);
        pane.add(txfAddress, 1, 1);
        pane.add(txfPhone, 1, 2);
        pane.add(txfEmail, 1, 3);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 0, 4);
        Button btnOK = new Button("OK");
        Button btnCancel = new Button("Cancel");

        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);
        buttonBox.getChildren().add(btnCancel);
        buttonBox.getChildren().add(btnOK);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);

        btnCancel.setOnAction(event -> controller.cancelAction());
        btnOK.setOnAction(event -> controller.createAction());

    }

    // -------------------------------------------------------------------------

    private class Controller
    {

        public void cancelAction()
        {
            CustomerDialog.this.hide();
        }

        /**
         * Creates new Customer. Only requires name
         */
        public void createAction()
        {
            String address = "N/A";
            String phone = "N/A";
            String email = "N/A";
            if (txfAddress.getText().isEmpty() != true)
            {
                address = txfAddress.getText();
            }

            if (txfPhone.getText().isEmpty() != true)
            {
                phone = txfPhone.getText();
            }
            if (txfEmail.getText().isEmpty() != true)
            {
                email = txfEmail.getText();
            }

            try
            {

                if (txfName.getText().isEmpty() != true)
                {
                    service.createCustomer(txfName.getText(), address, phone, email);
                    CustomerDialog.this.hide();
                }
                else
                {
                    throw new IllegalArgumentException();
                }

            }

            catch (IllegalArgumentException c)
            {

                lblError.setVisible(true);

            }
        }
    }
}
