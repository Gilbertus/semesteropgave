package gui;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.function.Predicate;

import application.model.Customer;
import application.model.Order;
import application.model.PaymentType;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OrderPanePaymentDialog extends Stage
{

    Service service = new Service();
    private final Controller controller = new Controller();
    private boolean isConfirmedSold = false;
    private boolean isWithCostumer = false;

    private final Label lbl2 = new Label();
    private final Label lblPaymentType = new Label("Betalings Metode:");
    private final Label lblVoucherClip = new Label("Brug Klippekort:");
    private final Label lblStartDate = new Label("Start Dato:");
    private final Label lblEndDate = new Label("Slut Dato:");

    private CheckBox chkboxDelivery = new CheckBox();

    private PaymentType typeOfPayment;
    private Customer tempCustomer;
    private Order order;

    private final TextField txfPriceToPay = new TextField();
    private final TextField txfAddedCus = new TextField();

    private final ComboBox<PaymentType> comboPaymentType = new ComboBox<>();
    private final ComboBox<Integer> comboAmountClips = new ComboBox<>();

    private int amountClipsUsed;
    private double remainingAmount = 0;
    private double moneyPayed;

    private final DatePicker dateStart = new DatePicker();
    private final DatePicker dateEnd = new DatePicker();

    private final Button btnAddCus = new Button("Tilføj Kunde");
    private final Button btnRemCus = new Button("Fjern Kunde");
    private final Button btnCancel = new Button("Cancel");
    private final Button btnOK = new Button("OK");
    HBox buttonBox = new HBox(10);

    private TableView<Customer> allCustomers = new TableView<Customer>();

    private final ObservableList<Customer> customers = FXCollections
            .observableArrayList(service.getAllCustomers());

    // -----------------------initialize new stage and scene--------------

    public OrderPanePaymentDialog(Stage owner)
    {
        moneyPayed = 0;
        amountClipsUsed = 0;
        isConfirmedSold = false;
        isWithCostumer = false;
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Betalings Dialog");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent(GridPane pane)
    {

        order = OrderPane.getTempOrder();
        pane.setGridLinesVisible(false);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        pane.add(lblPaymentType, 0, 0);
        pane.add(comboPaymentType, 1, 0);
        ArrayList<PaymentType> paymentTypes = service.getAllPaymentTypes();
        paymentTypes.remove(PaymentType.VOUCHERCLIP);

        pane.add(lbl2, 0, 2, 2, 1);

        Label lbl3 = new Label("Kunden betaler: ");
        pane.add(lbl3, 0, 4);
        pane.add(txfPriceToPay, 1, 4);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 0, 8, 2, 1);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_CENTER);
        buttonBox.getChildren().add(btnCancel);
        buttonBox.getChildren().add(btnOK);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);

        // ----------------- Button Actions ------------------------

        btnCancel.setOnAction(event -> controller.cancelAction());
        btnOK.setOnAction(event -> controller.okAction());
        btnRemCus.setOnAction(event -> controller.removeCustomer());

        // --------------------------- Line Seperator --------------------------

        Separator v1Separator = new Separator();
        v1Separator.orientationProperty().set(Orientation.VERTICAL);

        // --------------------------- PaymentType comboBox  --------------------------

        comboPaymentType.getItems().setAll(paymentTypes);
        comboPaymentType.setValue(paymentTypes.get(0));

        //Nullpointer rettet
        if (order != null)
        {

            int amountOfTapBeer = order.amountOfTapBeer();
            for (int i = 0; i <= amountOfTapBeer; i++)
            {

                comboAmountClips.getItems().add(i);
            }
        }

        comboAmountClips.setValue(0);

        // ------------------------- Change Listener ----------------------

        ChangeListener<Integer> comboBoxListener = (ov, o, n) -> controller.setPriceDetail();
        comboAmountClips.getSelectionModel().selectedItemProperty().addListener(comboBoxListener);

        // -------------------------- DatePicker -----------------------------

        dateStart.setEditable(false);
        dateStart.setDayCellFactory(picker -> new DateCell()
        {
            @Override
            public void updateItem(LocalDate date, boolean empty)
            {
                super.updateItem(date, empty);
                this.setDisable(empty || date.isBefore(LocalDate.now()));
            }
        });

        dateEnd.setEditable(false);
        dateEnd.setDayCellFactory(picker -> new DateCell()
        {
            @Override
            public void updateItem(LocalDate date, boolean empty)
            {

                super.updateItem(date, empty);
                if (BookingPane.isTourteam() == true)
                {
                    this.setDisable(empty || (dateStart.getValue() == null || date.isBefore(dateStart.getValue())));
                }

            }
        });

        // -------------------------- Text Fields ----------------------------

        if (OrderPaneOrderDialog.isOrderDialog() == true)
        {
            Order o3 = OrderPaneOrderDialog.gettempDialogOrder();

            if (o3.calculateDeposit() > 0)
            {
                txfPriceToPay.setText(""
                        + o3.calculatePriceWhenDelivered());
            } else
            {
                txfPriceToPay.setText("" + o3.calculateRemainingPrice());
            }

        } else if (BookingPane.isbookingPane() == true)
        {
            txfPriceToPay.setText("" + BookingPane.calcTotalDeposit());
            txfPriceToPay.setEditable(false);
        } else
        {
            txfPriceToPay.setText("" + OrderPane.getPriceTotal());
        }

        TableColumn<Customer, String> customerName = new TableColumn<Customer, String>("Navn");
        customerName.setMinWidth(150);
        customerName.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));

        TableColumn<Customer, String> customerPhone = new TableColumn<Customer, String>("Tlf Nummer");
        customerPhone.setMinWidth(100);
        customerPhone.setCellValueFactory(new PropertyValueFactory<Customer, String>("phoneNumber"));

        allCustomers.setItems(customers);
        allCustomers.getColumns().addAll(customerName, customerPhone);

        allCustomers.setMaxSize(260, 207);
        allCustomers.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // --------------------- Search Function --------------------------------

        TextField searchField = new TextField();

        searchField.setFont(Font.font("CALIBRI"));
        searchField.setPromptText("Søgefelt");

        FilteredList<Customer> filtProduct = new FilteredList<>(customers, e -> true);
        searchField.setOnKeyReleased(e -> {
            searchField.textProperty().addListener((observableValue, oldVal, newVal) -> {
                filtProduct.setPredicate((Predicate<? super Customer>) customer -> {

                    if (newVal == null || newVal.isEmpty())
                    {
                        return true;
                    }

                    String lcf = newVal.toLowerCase();
                    if (customer.getName().toLowerCase().contains(lcf) ||
                            customer.getPhoneNumber().contains(lcf))
                    {
                        return true;
                    }

                    return false;
                });
            });

            SortedList<Customer> sortedCustomer = new SortedList<>(filtProduct);
            sortedCustomer.comparatorProperty().bind(allCustomers.comparatorProperty());
            allCustomers.setItems(sortedCustomer);

        });

        // -------------------------- Key Event --------------------------------

        chkboxDelivery.setOnMouseClicked(e -> {
            double amount = BookingPane.calcTotalDeposit() + 500;
            double total = BookingPane.tempOrder.calculateRemainingPrice() + 500;
            if (chkboxDelivery.isSelected())
            {
                txfPriceToPay.setText("" + amount);
                txfPriceToPay.setEditable(false);
                lbl2.setText(
                        "Samlet beløb kunde skal betale:   " + total + " DKK"
                                + " "
                                + "+ pant: " + BookingPane.calcDeposit());
            } else
            {
                txfPriceToPay.setText("" + BookingPane.calcTotalDeposit());
                updateFields();
                txfPriceToPay.setEditable(false);
            }
        });

        txfPriceToPay.setOnKeyPressed(e ->

        {

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

        comboPaymentType.setOnKeyPressed(e ->

        {

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

        // --------

        updateFields();

        // --------------- If BookingPane ---------------------------
        if (BookingPane.isbookingPane() == true)
        {
            Label lbl4 = new Label("Kunde tilføjet reservation: ");
            pane.add(lbl4, 3, 7, 1, 1);
            pane.add(v1Separator, 2, 0, 2, 8);
            pane.add(txfAddedCus, 3, 8, 1, 1);
            pane.add(searchField, 3, 0);
            pane.add(allCustomers, 3, 1, 3, 6);
            txfAddedCus.setEditable(false);
            buttonBox.getChildren().addAll(btnAddCus, btnRemCus);
            buttonBox.setAlignment(Pos.CENTER_RIGHT);

            pane.add(dateStart, 1, 5);
            pane.add(dateEnd, 1, 6);
            pane.add(lblStartDate, 0, 5);
            pane.add(lblEndDate, 0, 6);
            dateStart.setValue(null);
            dateEnd.setValue(null);
            dateStart.setPromptText("Vælg startdato");
            dateEnd.setPromptText("Vælg slutdato");
            dateStart.setStyle(null);
            dateEnd.setStyle(null);
            if (BookingPane.isTourteam() == true)
            {

                pane.add(chkboxDelivery, 1, 7);
                chkboxDelivery.setText("Levering");
            }
        }

        EventHandler<MouseEvent> mouseEvent = (MouseEvent event) -> {

            if (!dateEnd.isEditable())
            {
                dateEnd.hide();
            }

        };
        EventHandler<ActionEvent> datePickerAction = (ActionEvent event) -> {

            dateEnd.setValue(dateStart.getValue());

        };

        if (BookingPane.isTourteam() != true)
        {
            dateStart.setOnAction(datePickerAction);
            dateEnd.setOnMouseClicked(mouseEvent);
        }
        if (BookingPane.isTourteam() == true)
        {
            dateStart.setOnAction(null);
            dateEnd.setOnMouseClicked(null);
        }

        // --------------- If OrderPane ---------------------------
        if (OrderPaneOrderDialog.isOrderDialog() != true && BookingPane.isbookingPane() != true)
        {
            buttonBox.getChildren().addAll(btnAddCus, btnRemCus);
            btnAddCus.setAlignment(Pos.CENTER_RIGHT);

            pane.add(btnOK, 0, 7, 3, 1);
            btnOK.setText("Sælg Nu");
            btnOK.setMinSize(267, 50);

            Label lbl4 = new Label("Kunde tilføjet: ");
            pane.add(lbl4, 0, 5);
            pane.add(v1Separator, 2, 0, 2, 8);
            pane.add(txfAddedCus, 1, 5);
            pane.add(searchField, 3, 0);
            pane.add(allCustomers, 3, 1, 3, 6);
            txfAddedCus.setEditable(false);

            pane.add(comboAmountClips, 1, 1);
            pane.add(lblVoucherClip, 0, 1);
            comboAmountClips.setPromptText("Tilføj Klip");
            comboAmountClips.maxWidth(40);
        }
        //------------------- Mouse Event All Orders ---------------------------------

        allCustomers.setRowFactory(allOrdMouse -> {
            TableRow<Customer> orderRow = new TableRow<>();
            orderRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!orderRow.isEmpty()))
                {

                    controller.addCustomer();

                }

            });
            return orderRow;
        });

        allCustomers.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && (!allCustomers.getSelectionModel().getSelectedItem().equals(null)))
            {
                controller.addCustomer();

            }

        });
    }

    // -------------------------------------------------------------------------

    public void updateFields()
    {
        if (BookingPane.isbookingPane() == true)
        {
            lbl2.setText(
                    "Samlet beløb kunde skal betale:   " + BookingPane.tempOrder.calculateRemainingPrice() + " DKK"
                            + " "
                            + "+ pant: " + BookingPane.calcDeposit());

        }

        else if (OrderPaneOrderDialog.isOrderDialog() == true)
        {
            Order o3 = OrderPaneOrderDialog.gettempDialogOrder();

            if (o3.calculateDeposit() > 0)
            {

                lbl2.setText(
                        "Kunden skal betale:     "
                                + o3.calculatePriceWhenDelivered()
                                + " DKK");
            }

            else
            {

                lbl2.setText(
                        "Kunden skal betale:     " + o3.calculateRemainingPrice()
                                + " DKK");
            }
        } else
        {
            lbl2.setText("Kunden skal betale:     " + OrderPane.getPriceTotal() + " DKK");
        }

    }

    // ------------------- SETTERS AND GETTERS -------------------------------------

    public double getMoneyPayed()
    {
        return moneyPayed;
    }

    public Customer getTempCustomer()
    {
        return tempCustomer;
    }

    public void setTempCustomer(Customer tempCustomer)
    {
        this.tempCustomer = tempCustomer;
    }

    public void setMoneyPayed(double moneyPayed)
    {
        this.moneyPayed = moneyPayed;
    }

    public PaymentType getTypeOfPayment()
    {
        return typeOfPayment;
    }

    public void setTypeOfPayment(PaymentType typeOfPayment)
    {
        this.typeOfPayment = typeOfPayment;
    }

    public boolean isConfirmedSold()
    {
        return isConfirmedSold;
    }

    public boolean isWithCostumer()
    {
        return isWithCostumer;
    }

    public DatePicker getDateStart()
    {
        return dateStart;
    }

    public DatePicker getDateEnd()
    {
        return dateEnd;
    }

    public CheckBox getChkboxDelivery()
    {
        return chkboxDelivery;
    }

    public void setChkboxDelivery(CheckBox chkboxDelivery)
    {
        this.chkboxDelivery = chkboxDelivery;
    }

    public int getAmountClipsUsed()
    {
        return amountClipsUsed;
    }

    // ----------------------- Controller ------------------------------------------------

    private class Controller
    {

        public void setPriceDetail()
        {
            if (comboAmountClips.getValue() > 0)
            {

                double tempResult = OrderPane.getPriceTotal()
                        - (OrderPane.getTempOrder().pricePerTap() * comboAmountClips.getValue());

                lbl2.setText("Kunden skal betale:     " + tempResult + " DKK");

                txfPriceToPay.setText("" + tempResult);
            }

        }

        public void removeCustomer()
        {

            txfAddedCus.clear();
            isWithCostumer = false;

        }

        public void addCustomer()
        {

            if (allCustomers.getSelectionModel().getSelectedItem() != null)
            {
                tempCustomer = allCustomers.getSelectionModel().getSelectedItem();
                txfAddedCus.setText(tempCustomer.getName());
                isWithCostumer = true;

            }
        }

        public void cancelAction()
        {
            isConfirmedSold = false;
            OrderPanePaymentDialog.this.hide();

        }

        public void okAction()

        {
            double beerPayedWithClips = 0;

            try
            {
                remainingAmount = Double.parseDouble(txfPriceToPay.getText());

                if (OrderPane.getTempOrder() != null)
                {
                    remainingAmount = OrderPane.getTempOrder().calculateRemainingPrice();
                }

                if (comboAmountClips.getValue() != null && comboAmountClips.getValue() > 0)
                {
                    amountClipsUsed = comboAmountClips.getValue();
                    beerPayedWithClips = OrderPane.getTempOrder().pricePerTap() * comboAmountClips.getValue();
                    remainingAmount = remainingAmount - beerPayedWithClips;
                }
                double temp = Double.parseDouble(txfPriceToPay.getText());

                if (BookingPane.isbookingPane() == true)
                {

                    if (dateStart.getValue() == null)
                    {
                        dateStart.setStyle("-fx-background-color: #ff4444;");
                        dateStart.setPromptText("Dato Mangler");
                        throw new IllegalArgumentException();
                    }
                    if (dateEnd.getValue() == null)
                    {

                        dateEnd.setStyle("-fx-background-color: #ff4444;");
                        dateEnd.setPromptText("Dato Mangler");
                        throw new IllegalArgumentException();
                    }

                    if (txfAddedCus.getText().isEmpty() == true)
                    {
                        txfAddedCus.setPromptText("Kunde skal vælges");
                        txfAddedCus.setStyle("-fx-text-box-border: red");
                        throw new IllegalArgumentException();
                    }
                }

                if (temp <= remainingAmount && temp > 0)
                {

                    isConfirmedSold = true;
                    setTypeOfPayment(comboPaymentType.getSelectionModel().getSelectedItem());
                    setMoneyPayed(Double.parseDouble(txfPriceToPay.getText()));

                    if (BookingPane.isbookingPane() == true)
                    {
                        setMoneyPayed(Double.parseDouble(txfPriceToPay.getText()) - BookingPane.calcTotalDeposit() + 1);
                    }
                }

                if (temp == 0 && beerPayedWithClips > 0)
                {

                    isConfirmedSold = true;
                    setTypeOfPayment(PaymentType.VOUCHERCLIP);
                    setMoneyPayed(comboAmountClips.getValue());

                }

                if (isConfirmedSold == true)
                {
                    OrderPanePaymentDialog.this.hide();
                }

                else
                {

                    txfPriceToPay.clear();
                    txfPriceToPay.setStyle("-fx-text-box-border: red");
                    txfPriceToPay.setPromptText("Ugyldigt input");

                    throw new IllegalArgumentException();

                }

            }

            catch (

            IllegalArgumentException ie)
            {
                ie.printStackTrace();
//                System.out.println("IllegalArugmentException - OrderPanePaymentDialog 180-220  " + remainingAmount);
            }

        }
    }

}
