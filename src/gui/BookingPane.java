package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;

import application.model.Customer;
import application.model.DepositProduct;
import application.model.DraftBeerDispenser;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.Price;
import application.model.TourTeam;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class BookingPane extends GridPane
{

    Service service = new Service();
    private final Controller controller = new Controller();
    private static Label lblTotalPrice = new Label("Total pris på ordre:   0.0 DKK");
    private final Button btnCancel = new Button("Annuller Reservation");
    private final Button btnCreateRes = new Button("Opret Reservation");
    private static TableView<Price> depositProductTable = new TableView<Price>();
    private static TableView<Price> draftDispenserTable = new TableView<Price>();
    private static TableView<OrderLine> currentReservation = new TableView<OrderLine>();
    private final static ObservableList<OrderLine> data = FXCollections.observableArrayList();
    private HBox buttonBox = new HBox(10);

    private final ListView<Order> lvwbookedProducts = new ListView<>();
    private final ListView<Order> lvwbookedTours = new ListView<>();

    public static double currentPrice;
    public static double priceTotal;
    public static boolean tourteam;
    public static boolean isBookingPane;
    public static Order tempOrder = new Order(LocalDateTime.now());

    public BookingPane()
    {
        this.initContent();
    }

    // -----------------------initialize content----------------------
    @SuppressWarnings("unchecked")
    private void initContent()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(20));
        this.setHgap(10);
        this.setVgap(10);

        // ------------------------------ TABLEVIEW depositProductTable ------------------------------------------

        final Label lblHeader = new Label("Fustager og Kulsyre");
        lblHeader.setFont(Font.font(23));
        this.add(lblHeader, 0, 0, 1, 1);
        this.add(depositProductTable, 0, 1, 4, 5);

        // ------------------------------- Product --------------------------------------------
        TableColumn<Price, String> product = new TableColumn<Price, String>("Produkt");
        product.setMinWidth(200);
        product.setCellValueFactory(new PropertyValueFactory<Price, String>("product"));

        // ------------------------------- Price ----------------------------------------------
        TableColumn<Price, Double> price = new TableColumn<Price, Double>("Pris");
        price.setMinWidth(100);
        price.setCellValueFactory(new PropertyValueFactory<>("price"));

        // ------------------------------- productCount ---------------------------------------
        TableColumn<Price, String> productCount = new TableColumn<Price, String>("Lagerbeholdning");
        productCount.setMinWidth(150);
        productCount.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        Integer.toString(priceObject.getValue().getProduct().getProductCount())));

        // ------------------------------- Unit Size ------------------------------------------
        TableColumn<Price, Double> productSize = new TableColumn<Price, Double>("Størrelse");
        productSize.setMinWidth(100);
        productSize.setCellValueFactory(
                priceObject -> new SimpleDoubleProperty(
                        ((DepositProduct) priceObject.getValue().getProduct()).getUnitSize()).asObject());
        // ------------------------------- Unit Type ------------------
        TableColumn<Price, String> unit = new TableColumn<Price, String>("Type");
        unit.setMinWidth(100);
        unit.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        ((DepositProduct) priceObject.getValue().getProduct()).getUnit()));
        // ------------------------------- Deposit ---------------------
        TableColumn<Price, Double> deposit = new TableColumn<Price, Double>("Pant");
        deposit.setMinWidth(100);
        deposit.setCellValueFactory(
                priceObject -> new SimpleDoubleProperty(
                        ((DepositProduct) priceObject.getValue().getProduct()).getDeposit()).asObject());

        depositProductTable.setItems(
                FXCollections.observableArrayList(service.getReservationDepositProduct()));
        depositProductTable.getColumns().addAll(product, productSize, unit, productCount, deposit, price);
        depositProductTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // ------------------------------- TABLE draftBeerTable ----------------------------------------

        final Label lblHeader2 = new Label("Anlæg & Rundvisning");
        lblHeader2.setFont(Font.font(23));
        this.add(lblHeader2, 0, 6, 1, 1);

        this.add(draftDispenserTable, 0, 7, 4, 5);

        // ------------------------------- Product ----------------------------------------
        TableColumn<Price, String> productName = new TableColumn<Price, String>("Produkt");
        productName.setMinWidth(200);
        productName.setCellValueFactory(new PropertyValueFactory<Price, String>("product"));

        // ------------------------------- Price ----------------------------------------

        TableColumn<Price, Double> productPrice = new TableColumn<Price, Double>("Pris");
        productPrice.setMinWidth(150);
        productPrice.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<Price, String> productCount2 = new TableColumn<Price, String>("Tilgængelige produkter");
        productCount2.setMinWidth(100);
        productCount2.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        Integer.toString(priceObject.getValue().getProduct().getProductCount())));

        draftDispenserTable.setItems(
                FXCollections.observableArrayList(service.getReservationDraftBeerDispenser()));

        draftDispenserTable.getColumns().addAll(productName, productPrice, productCount2);
        draftDispenserTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //------------------------------ Vertical Seperator --------------------------------------------

        Separator v1Separator = new Separator();
        Separator v2Separator = new Separator();
        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        v2Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v1Separator, 4, 0, 1, 15);
        this.add(v2Separator, 9, 0, 1, 15);

        // --------------------------- TABLE reservationTable ------------------------------------

        final Label lblReservation = new Label("Reservation");
        lblReservation.setFont(Font.font(23));

        this.add(lblReservation, 5, 0, 1, 1);
        this.add(currentReservation, 5, 1, 4, 5);

        TableColumn<OrderLine, String> proNavn = new TableColumn<OrderLine, String>("Produktnavn");
        proNavn.setMinWidth(200);
        proNavn.setCellValueFactory(
                navnObject -> new ReadOnlyStringWrapper(
                        navnObject.getValue().getProduct().getName()));

        TableColumn<OrderLine, String> proAntal = new TableColumn<OrderLine, String>("Antal");
        proAntal.setMinWidth(100);
        proAntal.setCellValueFactory(new PropertyValueFactory<>("numberOfProducts"));

        TableColumn<OrderLine, String> proFullAmount = new TableColumn<OrderLine, String>("Samlet Pris");
        proFullAmount.setMinWidth(150);
        proFullAmount.setCellValueFactory(
                amountObject -> new ReadOnlyStringWrapper(
                        Double.toString(amountObject.getValue().getFullAmount())));

        currentReservation.getColumns().addAll(proNavn, proAntal, proFullAmount);
        currentReservation.setPlaceholder(new Label("Ingen reservation oprettet."));

        this.add(lblTotalPrice, 6, 6);
        GridPane.setHalignment(lblTotalPrice, HPos.RIGHT);

        //------------------------------Reservation buttons ----------------------------------------------
        this.add(buttonBox, 5, 6);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.getChildren().add(btnCancel);
        buttonBox.getChildren().add(btnCreateRes);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelReservationAction());
        btnCreateRes.setOnAction(event -> controller.createReservationAction());

        // ---------------------- ListView Booked Products ------------------------------------------------

        final Label lblHeader3 = new Label("Udelejet Produkter");
        lblHeader3.setFont(Font.font(23));
        this.add(lblHeader3, 10, 0, 1, 1);
        this.add(lvwbookedProducts, 10, 1, 4, 5);
        lvwbookedProducts.setPrefSize(600, 500);

        // ---------------------- ListView Booked Tours ---------------------------------------------------

        final Label lblBookedtours = new Label("Kommende Rundvisninger");
        lblBookedtours.setFont(Font.font(23));
        this.add(lblBookedtours, 10, 6, 1, 1);
        this.add(lvwbookedTours, 10, 7, 4, 5);
        lvwbookedProducts.setPrefSize(600, 500);

        // ---------------------------- KEY PRESS DELETE orderLine ---------------------------------------
        currentReservation.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.DELETE
                    && (!currentReservation.getSelectionModel().getSelectedItem().equals(null)))
            {
                data.remove(currentReservation.getSelectionModel().getSelectedItem());
                currentReservation.setItems(FXCollections.observableArrayList(data));
                calcTotalPrice();
            }
        });

        // --------------------------- KEY PRESS ENTER DepositProducts Table -----------------------------

        depositProductTable.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER
                    && (!depositProductTable.getSelectionModel().getSelectedItem().equals(null)))
            {

                setProductLineDepositProduct();

            }

        });

        // ------------- KEY PRESS ENTER DraftBeerDispenser Table -----------------------------
        draftDispenserTable.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER
                    && (!draftDispenserTable.getSelectionModel().getSelectedItem().equals(null)))
            {

                setProductLineDraftBeerDispenser();

            }

        });

        //------------------- Mouse Event DepositProducts ---------------------------------

        depositProductTable.setRowFactory(allProMouse -> {
            TableRow<Price> priceRow = new TableRow<>();
            priceRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!priceRow.isEmpty()))
                {
                    setProductLineDepositProduct();
                }

            });
            return priceRow;
        });

        //------------------- Mouse Event DraftBeerDispenser ---------------------------------

        draftDispenserTable.setRowFactory(allProMouse -> {
            TableRow<Price> priceRow = new TableRow<>();
            priceRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!priceRow.isEmpty()))
                {
                    setProductLineDraftBeerDispenser();
                }

            });
            return priceRow;
        });

    }

    public void setTotalPriceTextField()
    {

        lblTotalPrice.setText("Total pris på ordre:   " + calcTotalPrice() + " DKK");

    }

    public static double getCurrentPrice()
    {
        return currentPrice;
    }

    public static boolean isbookingPane()
    {
        return isBookingPane;
    }

    public void getOrderInformation()
    {
        isBookingPane = true;
        Stage stage = new Stage();
        BookingPaneDialog dialog = new BookingPaneDialog(stage);
        dialog.showAndWait();
        isBookingPane = false;
    }

    public static Order getTempOrder()
    {
        return tempOrder;
    }

    public static void setTempOrder(Order tempOrder)
    {
        OrderPane.tempOrder = tempOrder;
    }

    public static double getPriceTotal()
    {
        return priceTotal;
    }

    public static TableView<OrderLine> getCurrentReservation()
    {
        return currentReservation;
    }

    public static boolean isTourteam()
    {
        return tourteam;
    }

    public static void setTourteam(boolean tourteam)
    {
        BookingPane.tourteam = tourteam;
    }

    /**
     * Creates a new OrderLine creation dialog when a product is pressed by "Enter" or "Double-Click"
     */
    public void setProductLineDraftBeerDispenser()
    {

        //Creates variable with price information for BookingPaneDialog
        currentPrice = draftDispenserTable.getSelectionModel().getSelectedItem().getPrice();
        //Creates Price object from the selected item in table
        Price p1 = new Price(currentPrice, draftDispenserTable.getSelectionModel().getSelectedItem().getProduct());

        tourteam = false;
        //Warning if trying to book multiple TourTeams or TourTeams and deposit products.
        if (data.size() > 0 && p1.getProduct() instanceof TourTeam
                || data.size() == 1 && data.get(0).getProduct() instanceof TourTeam == true)
        {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Advarsel");
            alert.setHeaderText("Rundvisning skal bookes alene.");
            alert.showAndWait();
            tourteam = true;
        }

        while (tourteam == false)
        {
            Stage stage = new Stage();
            BookingPaneDialog dialog = new BookingPaneDialog(stage);
            dialog.showAndWait();

            if (dialog.isConfirmed() != true)
            {
                tourteam = true;
            }

            if (dialog.isConfirmed() == true)
            {

                boolean newItem = true;
                int orderLinePlace = 0;

                //Gets number of products and possibly a new price per product
                int numberOfProducts = BookingPaneDialog.getNumberOfProducts();
                double discountedPrice = BookingPaneDialog.getDiscountedPrice();

                for (OrderLine orderLine : tempOrder.getOrderLines())
                {
                    if (orderLine.getProduct() == p1.getProduct()
                            && orderLine.getDiscountedPrice() == discountedPrice)
                    {
                        orderLine.setNumberOfProducts(numberOfProducts + orderLine.getNumberOfProducts());

                        newItem = false;
                        if (data.contains(orderLine))
                        {
                            orderLinePlace = data.indexOf(orderLine);

                        }
                    }
                }
                if (newItem == false)
                {
                    data.get(orderLinePlace).setFullAmount();
                }

                if (newItem == true)
                {
                    //Sets discounted price if entered and creates orderline connected to temp Order tempOrder
                    p1.setPrice(discountedPrice);
                    tempOrder.createOrderLine(numberOfProducts, discountedPrice, p1.getProduct());

                    //Adds all orderlines from temp Order tempOrder to ObservableList

                    if (data.size() == 0)
                    {
                        data.addAll(tempOrder.getOrderLines());
                    } else
                    {
                        data.add(tempOrder.getOrderLines().get(data.size()));
                    }
                    //Price is calculated and the Fullamount of the last orderline in the Observablelist is set to this.

                    data.get(data.size() - 1).setFullAmount();
                }

                //The Label lblTotalPrice is updated with new price
                setTotalPriceTextField();

                //currentOrder is populated with array of data
                currentReservation.setItems(FXCollections.observableArrayList(data));
                currentReservation.refresh();
                tourteam = true;
            }
        }
    }

    public void setProductLineDepositProduct()
    {
        currentPrice = depositProductTable.getSelectionModel().getSelectedItem().getPrice();
        Price p1 = new Price(currentPrice, depositProductTable.getSelectionModel().getSelectedItem().getProduct());

        tourteam = false;

        //Warning if trying to book multiple TourTeams or TourTeams and deposit products.
        if (data.size() > 0 && p1.getProduct() instanceof TourTeam
                || data.size() == 1 && data.get(0).getProduct() instanceof TourTeam == true)
        {
            Alert alert = new Alert(AlertType.WARNING);
            alert.setTitle("Advarsel");
            alert.setHeaderText("Rundvisning skal bookes alene.");
            alert.showAndWait();
            tourteam = true;
        }

        while (tourteam == false)
        {
            Stage stage = new Stage();
            BookingPaneDialog dialog = new BookingPaneDialog(stage);
            dialog.showAndWait();

            if (dialog.isConfirmed() != true)
            {
                tourteam = true;
            }

            if (dialog.isConfirmed() == true)
            {
                boolean newItem = true;
                int orderLinePlace = 0;
                int numberOfProducts = BookingPaneDialog.getNumberOfProducts();
                double discountedPrice = BookingPaneDialog.getDiscountedPrice();

                for (OrderLine orderLine : tempOrder.getOrderLines())
                {
                    if (orderLine.getProduct() == p1.getProduct()
                            && orderLine.getDiscountedPrice() == discountedPrice)
                    {
                        orderLine.setNumberOfProducts(numberOfProducts + orderLine.getNumberOfProducts());

                        newItem = false;
                        if (data.contains(orderLine))
                        {
                            orderLinePlace = data.indexOf(orderLine);
                        }
                    }
                }
                if (newItem == false)
                {
                    data.get(orderLinePlace).setFullAmount();
                }

                if (newItem == true)
                {
                    // Check to see if discount price is less or the same as the product getting discounted
                    p1.setPrice(discountedPrice);
                    tempOrder.createOrderLine(numberOfProducts, discountedPrice, p1.getProduct());

                    //Adds all orderlines from temp Order tempOrder to ObservableList
                    if (data.size() == 0)
                    {
                        data.addAll(tempOrder.getOrderLines());
                    } else
                    {
                        data.add(tempOrder.getOrderLines().get(data.size()));
                    }
                    //Price is calculated and the Fullamount of the last orderline in the Observablelist is set to this.

                    data.get(data.size() - 1).setFullAmount();
                }
                setTotalPriceTextField();

                //currentOrder is populated with array of data
                currentReservation.setItems(FXCollections.observableArrayList(data));
                currentReservation.refresh();
                tourteam = true;
            }
        }
    }

    public static double calcRemainingCost()
    {

        return calcTotalPrice() - calcPayedAmount();
    }

    private static double calcPayedAmount()
    {
        double tempTotal = 0;

        for (Payment payment : tempOrder.getPayments())
        {
            tempTotal += payment.getAmount();
        }
        return tempTotal;
    }

    /**
     * Calculates the total price for an order by looking at each orderlines .getFullAmount() method
     * @returns totalPrice
     */
    public static double calcTotalPrice()
    {
        double totalPrice = 0;
        for (OrderLine orderLine : data)
        {
            totalPrice += orderLine.getFullAmount();
        }

        return totalPrice;

    }

    /**
     * Calculates the total price for an order by looking at each orderlines .getFullAmount() method
     * @returns totalPrice
     */
    public static double calcTotalDeposit()
    {
        double totalPrice = 0;

        for (OrderLine orderLine : data)
        {
            if (orderLine.getProduct() instanceof DepositProduct)
            {
                totalPrice += ((DepositProduct) orderLine.getProduct()).getDeposit() * orderLine.getNumberOfProducts();
            }
            if (orderLine.getProduct() instanceof DraftBeerDispenser || orderLine.getProduct() instanceof TourTeam)
            {
                totalPrice += orderLine.getFullAmount();
            }
        }

        return totalPrice;

    }

    public static double calcDeposit()
    {
        double totalPrice = 0;

        for (OrderLine orderLine : data)
        {
            if (orderLine.getProduct() instanceof DepositProduct)
            {
                totalPrice += ((DepositProduct) orderLine.getProduct()).getDeposit() * orderLine.getNumberOfProducts();
            }
        }

        return totalPrice;

    }

    public void updateControls()
    {
        currentReservation.getItems().clear();
        lvwbookedProducts.getItems().setAll(service.getBookedProducts());
        lvwbookedTours.getItems().setAll(service.getBookedTours());
        data.clear();
        currentReservation.getItems().clear();
        currentReservation.refresh();
        lvwbookedProducts.refresh();
        lvwbookedTours.refresh();
        tempOrder = new Order(LocalDateTime.now());

    }

    // ----------------------- controller ----------------------------

    private class Controller
    {

        public void cancelReservationAction()
        {
            for (OrderLine orderLine : data)
            {
                tempOrder.removeOrderLine(orderLine);
            }
            data.clear();
            currentReservation.setItems(FXCollections.observableArrayList(data));
            setTotalPriceTextField();
        }

        public void createReservationAction()
        {
            {
                try
                {

                    if (data.isEmpty())
                    {
                        throw new RuntimeException();
                    }

                    isBookingPane = true;
                    priceTotal = tempOrder.calculatePrice();

                    if (data.get(0).getProduct() instanceof TourTeam == true)
                    {
                        setTourteam(false);
                    }
                    Stage stage = new Stage();
                    OrderPanePaymentDialog dialog = new OrderPanePaymentDialog(stage);
                    dialog.showAndWait();

                    if (dialog.isConfirmedSold() == true)
                    {

                        if (dialog.getMoneyPayed() <= tempOrder.calculateRemainingPrice()
                                && dialog.getMoneyPayed() != 0)
                        {
                            LocalDate startDate = dialog.getDateStart().getValue();
                            LocalDate endDate = dialog.getDateEnd().getValue();
                            boolean delivery = dialog.getChkboxDelivery().isSelected();

                            Order finalOrder = service.createOrderReservation(LocalDateTime.now(),
                                    startDate, endDate, delivery);

                            double amountPayed = 0;

                            if (tempOrder.calculateDeposit() > 0)
                            {
                                amountPayed = dialog.getMoneyPayed() + tempOrder.calculateDeposit() - 1;

                            } else
                            {
                                amountPayed = dialog.getMoneyPayed() - 1;
                            }

                            Payment payment = service.createPayment(dialog.getTypeOfPayment(), amountPayed);

                            for (OrderLine orderLine : data)
                            {
                                finalOrder.createOrderLine(orderLine.getNumberOfProducts(),
                                        orderLine.getDiscountedPrice(),
                                        orderLine.getProduct());
                            }

                            finalOrder.addPayment(payment);

                            if (dialog.isWithCostumer() == true)
                            {
                                Customer customer = dialog.getTempCustomer();
                                customer.addOrder(finalOrder);
                            }

                            lvwbookedProducts.getItems().setAll(service.getBookedProducts());
                            lvwbookedTours.getItems().setAll(service.getBookedTours());

                            service.sellOrder(finalOrder.getOrderLines(), finalOrder.getPayments(), finalOrder);

                            for (OrderLine orderLine : data)
                            {
                                tempOrder.removeOrderLine(orderLine);
                            }
                            data.clear();
                            currentReservation.setItems(FXCollections.observableArrayList(data));
                            setTotalPriceTextField();

                            Alert alert = new Alert(AlertType.INFORMATION);
                            alert.setTitle("Reservation");
                            alert.setHeaderText("Reservation er oprettet");
                            alert.showAndWait();
                            tourteam = true;
                        }
                    }
                }

                catch (RuntimeException rte)
                {
                    System.out.println("RuntimeException - #789 BookingPane");
                }
            }
            isBookingPane = false;
        }

    }

}
