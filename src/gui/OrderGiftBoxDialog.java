package gui;

import java.time.LocalDateTime;
import java.util.ArrayList;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import application.model.Order;
import application.model.OrderLine;
import application.model.Product;
import application.model.ProductType;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OrderGiftBoxDialog extends Stage
{

    Service service = new Service();
    private double discountedPrice;
    private final Controller controller = new Controller();
    private boolean isConfirmed = false;

    private int numberOfBottles = 0;

    private static TableView<Product> allProducts = new TableView<Product>();
    private static TableView<OrderLine> currentOrder = new TableView<OrderLine>();

    private final Label lblError = new Label("Ugyldigt Input");

    private final Order tempOrder = new Order(LocalDateTime.now());

    private final ObservableList<Product> allProductsData = FXCollections
            .observableArrayList();

    private final ObservableList<OrderLine> allCurrentOrderdata = FXCollections
            .observableArrayList();

    public static ArrayList<OrderLine> bottles = new ArrayList<>();

    // -----------------------initialize new stage and scene--------------

    public OrderGiftBoxDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Antal");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private final TextField txfDiscountedPrice = new TextField();
    private final TextField txfAmount = new TextField();

    Button btnCancel = new Button("Cancel");
    Button btnOK = new Button("OK");

    HBox buttonBox = new HBox(10);

    private void initContent(GridPane pane)
    {
        isConfirmed = false;
        allCurrentOrderdata.clear();
        bottles.clear();
        currentOrder.getItems().clear();
        currentOrder.refresh();
        allProducts.refresh();

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------
        Label lbl1 = new Label("Antal: ");
        pane.add(lbl1, 4, 5);

        Label lbl2 = new Label("Nypris På Gaveæske: ");
        pane.add(lbl2, 4, 0);

        pane.add(lblError, 1, 2);
        lblError.setVisible(false);
        lblError.setTextFill(Color.RED);

        txfDiscountedPrice.setText(Double.toString(OrderPane.getPriceRowDataSingle()));

        // -------------------------- Text Fields ----------------------------

        pane.add(txfDiscountedPrice, 5, 0);
        pane.add(txfAmount, 5, 5);
        txfAmount.setText("1");

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 4, 15);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);

        Button btnAdd = new Button("Tilføj");
        pane.add(btnAdd, 5, 6);
        btnAdd.setOnAction(event -> controller.addAct());

        Button btnCancel = new Button("Cancel");
        buttonBox.getChildren().add(btnCancel);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelAction());

        Button btnOK = new Button("OK");
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.okAction());

        //--------------------- TableView Product ----------------------------------------

        allProducts.setEditable(false);

        TableColumn<Product, String> productName = new TableColumn<Product, String>("Navn");
        productName.setMinWidth(200);
        productName.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));

        allProducts.getColumns().addAll(productName);
        pane.add(allProducts, 0, 0, 3, 25);

        for (Product product : service.getAllProducts())
        {
            if (product.getProductType() == ProductType.BOTTLE)
            {
                allProductsData.add(product);
            }
        }
        allProducts.setItems(allProductsData);
        allProducts.setMaxWidth(200);
        allProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        //--------------------- TableView CurrentOrder ----------------------------------------

        currentOrder.setEditable(false);
        currentOrder.setPlaceholder(new Label("Ingen øl tilføjet"));

//        TableColumn<OrderLine, String> productName1 = new TableColumn<Product, String>("Navn");
//        productName1.setMinWidth(200);
//        productName1.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));

        TableColumn<OrderLine, String> proNavn = new TableColumn<OrderLine, String>("Produktnavn");
        proNavn.setMinWidth(200);
        proNavn.setCellValueFactory(
                navnObject -> new ReadOnlyStringWrapper(
                        navnObject.getValue().getProduct().getName()));

        TableColumn<OrderLine, String> proAntal = new TableColumn<OrderLine, String>("Antal");
        proAntal.setMinWidth(100);
        proAntal.setCellValueFactory(new PropertyValueFactory<>("numberOfProducts"));

        currentOrder.getColumns().addAll(proNavn, proAntal);
        pane.add(currentOrder, 6, 0, 3, 25);

        currentOrder.setMaxWidth(450);
        currentOrder.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // -------------------------- Key Event --------------------------------

        /**
         * Keypress UP: Increases numberOfProducts with 1 each press
         * Keypress DOWN: Decreases numberOfProducts with 1 each press
         * KeyPress ENTER: call Controller.okAction()
         */

        txfDiscountedPrice.setOnKeyPressed(e -> {

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

    }

    // ------------- GETTERS AND SETTERS -------------------------------------

    public double getDiscountedPrice()
    {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice)
    {
        this.discountedPrice = discountedPrice;
    }

    public boolean isConfirmed()
    {
        return isConfirmed;
    }

    public ArrayList<OrderLine> getBottles()
    {
        return bottles;
    }

    // -------------------------------------------------------------------------

    private class Controller
    {

        public void addAct()

        {
            boolean isNewItem = true;

            if (allProducts.getSelectionModel().getSelectedItem() != null)
            {

                OrderLine o1 = tempOrder.createOrderLine(
                        Integer.parseInt(txfAmount.getText()), 0,
                        allProducts.getSelectionModel().getSelectedItem());

                if (o1.getNumberOfProducts() + numberOfBottles <= OrderPane.getGiftBoxSize())
                {

                    for (OrderLine orderLine : allCurrentOrderdata)
                    {
                        if (orderLine.getProduct() == allProducts.getSelectionModel().getSelectedItem())
                        {

                            orderLine.setNumberOfProducts(
                                    orderLine.getNumberOfProducts() + Integer.parseInt(txfAmount.getText()));
                            isNewItem = false;

                        }
                    }

                    if (isNewItem == true)
                    {

                        allCurrentOrderdata.add(o1);
                    }

                    numberOfBottles += o1.getNumberOfProducts();

                    currentOrder.setItems(allCurrentOrderdata);
                    txfAmount.setText("1");

                }

                else
                {
                    tempOrder.removeOrderLine(o1);
                }

            }

            currentOrder.refresh();

        }

        /**
         * Closes OrderPaneDialog. Sets isConfirmed to false
         */
        public void cancelAction()
        {
            allCurrentOrderdata.clear();
            bottles.clear();

            isConfirmed = false;
            OrderGiftBoxDialog.this.hide();
        }

        /**
         * Sets static fields with information written in the text fields so they are reachable from OrderPane
         * Sets isConfirmed to true
         * Throws ParseException if text field test cant be parsed and IllegalArugmetnException if textField is letters
         * not numbers
         */
        public void okAction()
        {

            try
            {

                if (numberOfBottles != OrderPane.getGiftBoxSize())
                {

                    lblError.setVisible(true);
                    throw new IllegalArgumentException();
                }

                for (OrderLine orderLine : allCurrentOrderdata)
                {
                    bottles.add(orderLine);

                }

                discountedPrice = Double.parseDouble(txfDiscountedPrice.getText());

                if (discountedPrice < 0)
                {
                    lblError.setVisible(true);
                    txfDiscountedPrice.requestFocus();
                    throw new IllegalArgumentException();
                }

                isConfirmed = true;
                OrderGiftBoxDialog.this.hide();
            }

            catch (ParseException pE)
            {

            }

            catch (IllegalArgumentException iAE)
            {

            }

        }
    }

}
