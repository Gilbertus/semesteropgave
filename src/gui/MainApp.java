package gui;

import application.service.Service;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application
{
    private final Controller controller = new Controller();
    Service service = new Service();

    public static void main(String[] args)
    {
        Application.launch(args);
    }

    @Override
    public void init()
    {
        Service.loadStorage();
    }

    @Override
    public void start(Stage stage)
    {
        Image image = new Image("aahusbryg.png");
        stage.getIcons().add(image);

        stage.setMaximized(true);
        stage.setTitle("Aarhus Bryghus");
        BorderPane pane = new BorderPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop()
    {
        Service.saveStorage();
    }

    // -------------------------------------------------------------------------

    private void initContent(BorderPane pane)
    {
        MenuBar menubar = new MenuBar();
        this.initMenus(menubar);
        pane.setTop(menubar);
        TabPane tabPane = new TabPane();
        this.initTabPane(tabPane);
        pane.setCenter(tabPane);

    }

    private void initMenus(MenuBar menubar)
    {

        Menu menuPrint = new Menu("System");
        menubar.getMenus().add(menuPrint);

        Menu menuVersion = new Menu("Version");

        menuPrint.getItems().add(menuVersion);

        menuVersion.setOnAction(event -> controller.versionAction());

        menuVersion.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN));

        Menu menuStatistic = new Menu("Statastik");

        menuPrint.getItems().add(menuStatistic);

        menuStatistic.setOnAction(event -> controller.statisticAction());

        MenuItem exit = new MenuItem("Exit");
        menuPrint.getItems().add(exit);
        exit.setOnAction(event -> controller.exitAction());
        exit.setAccelerator(new KeyCodeCombination(KeyCode.M, KeyCombination.SHORTCUT_DOWN));

    }

    private void initTabPane(TabPane tabPane)
    {
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        // ----- Order ---------------------------

        Tab tabOrder = new Tab("Ordre");
        tabPane.getTabs().add(tabOrder);

        OrderPane orderPane = new OrderPane();
        tabOrder.setContent(orderPane);
        tabOrder.setOnSelectionChanged(event -> orderPane.updateControls());

        // ----------- PRODUCT ------------------------

        Tab tabProduct = new Tab("Produkt");
        tabPane.getTabs().add(tabProduct);

        ProductPane productPane = new ProductPane();
        tabProduct.setContent(productPane);
        tabProduct.setOnSelectionChanged(event -> productPane.updateControls());

        // ----------- PRICE LIST ---------------------------

        Tab tabPriceList = new Tab("Prislister");
        tabPane.getTabs().add(tabPriceList);

        PriceListPane priceListPane = new PriceListPane();
        tabPriceList.setContent(priceListPane);
        tabPriceList.setOnSelectionChanged(event -> priceListPane.updateControls());

        // ---------- BOOKING --------------------------------

        Tab tabBooking = new Tab("Reservation");
        tabPane.getTabs().add(tabBooking);

        BookingPane bookingPane = new BookingPane();
        tabBooking.setContent(bookingPane);
        tabBooking.setOnSelectionChanged(event -> bookingPane.updateControls());

        Tab tabCustomer = new Tab("Kunder");
        tabPane.getTabs().add(tabCustomer);

        CustomerPane customerPane = new CustomerPane();
        tabCustomer.setContent(customerPane);
        tabCustomer.setOnSelectionChanged(event -> customerPane.updateControls());

    }

    // -------------------------------------------------------------------------

    private class Controller
    {
        public void exitAction()
        {
            Service.saveStorage();
            Platform.exit();
        }

        public void versionAction()
        {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Version Vindue");
            alert.setHeaderText("Version 1.2");
            alert.setContentText(
                    "Du bruger V.12 af 'Aarhus Bryghus Salgs og Styring' program");
            alert.showAndWait();
        }

        public void statisticAction()
        {
            Stage stage = new Stage();
            StatisticDialog dialog = new StatisticDialog(stage);
            dialog.showAndWait();
        }
    }
}
