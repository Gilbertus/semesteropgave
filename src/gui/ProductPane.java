package gui;

import java.util.function.Predicate;

import application.model.Beverage;
import application.model.GiftBox;
import application.model.Price;
import application.model.PriceList;
import application.model.Product;
import application.model.VoucherClip;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ProductPane extends GridPane
{
    Service service = new Service();
    private final Controller controller = new Controller();

    private final TextField searchField = new TextField();
    private final TextField txfPrice = new TextField();
    private final TextField txfAlcoPer = new TextField();
    private final TextField txfGlassAmount = new TextField();
    private final TextField txfBeerAmount = new TextField();
    private final TextField txfBoxType = new TextField();
    private final TextField txfVoucherClips = new TextField();
    private final Label lbl2 = new Label("Produkt Detaljer");
    private final Label lbl10 = new Label("Produktoversigt");
    private final Label lbl3 = new Label("Pris: ");
    private final Label lbl4 = new Label("Alkohol Procent: ");
    private final Label lbl5 = new Label("Antal Glas: ");
    private final Label lbl6 = new Label("Antal Øl: ");
    private final Label lbl7 = new Label("Type af box: ");
    private final Label lbl8 = new Label("Antal Klip: ");
    private final Label lbl9 = new Label("Prisliste: ");
    private final Button addbtn = new Button("Nyt Produkt");
    private final Button rembtn = new Button("Fjern Produkt");
    private final Separator h1Separator = new Separator();
    private final Separator v1Separator = new Separator();
    private final Separator v2Separator = new Separator();
    private final ComboBox<PriceList> comboPriceList = new ComboBox<>();
    private TableView<Product> allProducts = new TableView<Product>();
    private final ObservableList<Product> data = FXCollections
            .observableArrayList(service.getAllProducts());

    public ProductPane()
    {
        this.initContent();
    }

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent()
    {
        this.setGridLinesVisible(false);
        this.setPadding(new Insets(20));
        this.setHgap(10);
        this.setVgap(10);

        // --------------------- Labels ---------------------------------------

        this.add(lbl2, 4, 0, 2, 1);
        lbl2.setFont(Font.font(23));
        this.add(lbl10, 0, 0, 2, 1);
        lbl10.setFont(Font.font(23));
        this.add(lbl3, 4, 4);
        this.add(lbl4, 4, 5);
        this.add(lbl5, 4, 6);
        this.add(lbl6, 4, 7);
        this.add(lbl7, 4, 8);
        this.add(lbl8, 4, 9);
        this.add(lbl9, 4, 3);

        // -------------------- Buttons on HBox --------------------------------------

        this.add(addbtn, 4, 10);
        GridPane.setHalignment(addbtn, HPos.LEFT);
        addbtn.setOnAction(event -> controller.createAction());
        this.add(rembtn, 5, 10);
        GridPane.setHalignment(rembtn, HPos.RIGHT);
        rembtn.setOnAction(event -> controller.remAction());

        // --------------------Line Separator----------------------------------

        h1Separator.orientationProperty().set(Orientation.HORIZONTAL);
        this.add(h1Separator, 0, 1, 9, 1);
        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v1Separator, 3, 0, 1, 78);
        v2Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v2Separator, 6, 0, 1, 78);

        // ------------------- Text Fields ----------------------------------

        this.add(searchField, 0, 2);
        searchField.setFont(Font.font("CALIBRI"));
        searchField.setPromptText("Søgefelt");

        this.add(txfPrice, 5, 4);
        txfPrice.setEditable(false);
        this.add(txfAlcoPer, 5, 5);
        txfAlcoPer.setEditable(false);
        this.add(txfGlassAmount, 5, 6);
        txfGlassAmount.setEditable(false);
        this.add(txfBeerAmount, 5, 7);
        txfBeerAmount.setEditable(false);
        this.add(txfBoxType, 5, 8);
        txfBoxType.setEditable(false);
        this.add(txfVoucherClips, 5, 9);
        txfVoucherClips.setEditable(false);

        // ----- TABLE COLUMN PRODUCTS --------------

        TableColumn<Product, String> productName = new TableColumn<Product, String>("Navn");
        productName.setMinWidth(200);
        productName.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));

        TableColumn<Product, String> productCount = new TableColumn<Product, String>("Lagerbeholdning");
        productCount.setMinWidth(200);
        productCount.setCellValueFactory(new PropertyValueFactory<Product, String>("productCount"));

        TableColumn<Product, String> productID = new TableColumn<Product, String>("Produkt ID");
        productID.setMinWidth(200);
        productID.setCellValueFactory(new PropertyValueFactory<Product, String>("idNo"));

        allProducts.setItems(data);
        allProducts.getColumns().addAll(productName, productCount, productID);
        allProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        this.add(allProducts, 0, 3, 3, 45);

        // -------------------- Change Listener -------------------------------

        ChangeListener<Product> tableListener = (ov, o, n) -> controller.setProductDetails();
        allProducts.getSelectionModel().selectedItemProperty().addListener(tableListener);

        ChangeListener<PriceList> comboBoxListener = (ov, o, n) -> controller.setPriceDetail();
        comboPriceList.getSelectionModel().selectedItemProperty().addListener(comboBoxListener);

        // -------------------- Combo Box--------------------------------------

        this.add(comboPriceList, 5, 3);
        comboPriceList.getItems().setAll(service.getAllPriceLists());
        comboPriceList.setValue(service.getAllPriceLists().get(0));

        // --------------------- Search Function --------------------------------

        // ---- Search method in productPane
        FilteredList<Product> filtProduct = new FilteredList<>(data, e -> true);
        // ---- Activates when key is released
        searchField.setOnKeyReleased(e -> {
            searchField.textProperty().addListener((observableValue, oldVal, newVal) -> {
                filtProduct.setPredicate((Predicate<? super Product>) product -> {
                    if (newVal == null || newVal.isEmpty())
                    {
                        return true;
                    }
                    String lcf = newVal.toLowerCase();
                    // --- Checks if product contains the input string
                    if (product.getName().toLowerCase().contains(lcf))
                    {
                        return true;
                    }
                    return false;
                });
            });
            // ---- Sorts the list when searched
            SortedList<Product> sortedProduct = new SortedList<>(filtProduct);
            sortedProduct.comparatorProperty().bind(allProducts.comparatorProperty());
            allProducts.setItems(sortedProduct);

        });

        // -------------------------- Context Menu and Items ----------------------------------------
        ContextMenu menu = new ContextMenu();
        // Menu options
        MenuItem option2 = new MenuItem("Slet produkt");
        // adding options to menu
        menu.getItems().addAll(option2);
        // adding Context Menu to Tableview
        allProducts.setContextMenu(menu);
        // create menu item actions
        option2.setOnAction(event -> controller.remAction());
    }

    /**
     * Updates when ProductTab is choosen
     */
    public void updateControls()
    {
        allProducts.refresh();
        comboPriceList.setValue(service.getAllPriceLists().get(0));
    }

    // ----------------------- controller ----------------------------

    private class Controller
    {
        /**
         * Sets the txfPrice with the selected Products price on the selected pricelist
         */
        public void setPriceDetail()
        {
            Product p = allProducts.getSelectionModel().getSelectedItem();
            PriceList pL = comboPriceList.getSelectionModel().getSelectedItem();
            if (comboPriceList.getSelectionModel().getSelectedItem() != null)
            {
                for (Price price : pL.getPrices())
                {
                    if (price.getProduct() == p && p != null)
                    {
                        txfPrice.setText(Double.toString(price.getPrice()));
                        txfPrice.setDisable(false);
                        break;
                    }
                    else if (p == null)
                    {

                    }
                    else
                    {
                        txfPrice.setText("N/A");
                        txfPrice.setDisable(true);
                    }
                }
            }
        }

        /**
         * Fills the textfields with information for the giving product.
         * Disables all textfields which are not used for the specific subclass of Product
         */
        public void setProductDetails()
        {

            txfAlcoPer.clear();
            txfBeerAmount.clear();
            txfBoxType.clear();
            txfGlassAmount.clear();
            txfVoucherClips.clear();

            txfAlcoPer.setDisable(false);
            txfBeerAmount.setDisable(false);
            txfBoxType.setDisable(false);
            txfGlassAmount.setDisable(false);
            txfVoucherClips.setDisable(false);

            setPriceDetail();

            if (allProducts.getSelectionModel().getSelectedItem() instanceof GiftBox
                    && allProducts.getSelectionModel().getSelectedItem() != null)
            {

                GiftBox gf = (GiftBox) allProducts.getSelectionModel().getSelectedItem();
                txfBeerAmount.setText(Integer.toString(gf.getBeerAmount()));
                txfBoxType.setText(gf.getBoxType());
                txfGlassAmount.setText(Integer.toString(gf.getGlassAmount()));

                txfAlcoPer.setText("N/A");
                txfVoucherClips.setText("N/A");
                txfAlcoPer.setDisable(true);
                txfVoucherClips.setDisable(true);

            }
            else if (allProducts.getSelectionModel().getSelectedItem() instanceof Beverage
                    && allProducts.getSelectionModel().getSelectedItem() != null)
            {
                Beverage b = (Beverage) allProducts.getSelectionModel().getSelectedItem();
                txfAlcoPer.setText(Double.toString(b.getAlcoPer()));

                txfBeerAmount.setText("N/A");
                txfBoxType.setText("N/A");
                txfGlassAmount.setText("N/A");
                txfVoucherClips.setText("N/A");

                txfBeerAmount.setDisable(true);
                txfBoxType.setDisable(true);
                txfGlassAmount.setDisable(true);
                txfVoucherClips.setDisable(true);

            }
            else if (allProducts.getSelectionModel().getSelectedItem() instanceof VoucherClip
                    && allProducts.getSelectionModel().getSelectedItem() != null)
            {

                VoucherClip vC = (VoucherClip) allProducts.getSelectionModel().getSelectedItem();
                txfVoucherClips.setText(Integer.toString(vC.getClipCount()));

                txfAlcoPer.setText("N/A");
                txfBeerAmount.setText("N/A");
                txfBoxType.setText("N/A");
                txfGlassAmount.setText("N/A");

                txfAlcoPer.setDisable(true);
                txfBeerAmount.setDisable(true);
                txfBoxType.setDisable(true);
                txfGlassAmount.setDisable(true);

            }
            else
            {

                txfAlcoPer.setText("N/A");
                txfBeerAmount.setText("N/A");
                txfBoxType.setText("N/A");
                txfGlassAmount.setText("N/A");
                txfVoucherClips.setText("N/A");

                txfAlcoPer.setDisable(true);
                txfBeerAmount.setDisable(true);
                txfBoxType.setDisable(true);
                txfGlassAmount.setDisable(true);
                txfVoucherClips.setDisable(true);

            }
        }

        /**
         * Open ProductDialog for create a new product
         */
        public void createAction()
        {
            Stage stage = new Stage();
            ProductDialog dialog = new ProductDialog(stage);
            dialog.showAndWait();
            // ... wait for the dialog to close

//            updateControls();
            fillTableView();

        }

        /**
         * Remove selected Product with Alert confirmations window.
         * Updates the list after deletion
         */
        public void remAction()
        {

            if (allProducts.getSelectionModel().getSelectedItem() != null)
            {
                Product tempProduct = allProducts.getSelectionModel().getSelectedItem();
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Bekræftigelse vindue");
                alert.setHeaderText("Kunde: " + tempProduct.getName());
                alert.setContentText(
                        "Er du sikker på du vil slette det valgte produkt?: \n"
                                + tempProduct.getName());

                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK)
                {

                    for (Price price : tempProduct.getPrices())
                    {
                        price.getPriceList().removePrice(price);
                        price.setProduct(null);
                    }
                    service.deleteProduct(tempProduct);

                }
                fillTableView();
            }

        }

        /**
         * Fills the table view with products
         */
        public void fillTableView()
        {

            allProducts.getItems().setAll(FXCollections
                    .observableArrayList(service.getAllProducts()));
        }
    }

}
