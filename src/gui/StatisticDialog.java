package gui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.function.Predicate;

import application.model.Order;
import application.model.Price;
import application.model.PriceList;
import application.model.Product;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class StatisticDialog extends Stage
{

    Service service = new Service();
    private final Controller controller = new Controller();
    private final TextField searchField = new TextField();
    private final DatePicker dateStartPicker = new DatePicker();
    private final DatePicker dateEndPicker = new DatePicker();
    private final ComboBox<PriceList> comboPriceList = new ComboBox<>();
    private static TableView<Order> allOrders = new TableView<Order>();
    private static TableView<Price> allProducts = new TableView<Price>();
    private final TextField txfProductName = new TextField();
    private final TextField txfProductSold = new TextField();
    private final TextField txfAmountEarned = new TextField();
    private final TextField txfClipsUsed = new TextField();
    private final Label lblHeading1 = new Label("Produktoversigt");
    private final Label lblHeading2 = new Label("Statistik");
    private final Label lblHeading4 = new Label("Klippekort Data");
    private final Label lblHeading3 = new Label("Ordre Tilknyttet Produktet");
    private final Label lblDateStart = new Label("Fra Dato:");
    private final Label lblDateEnd = new Label("Til Dato:");
    private final Label lblProductName = new Label("Produkt Navn: ");
    private final Label lblProductSold = new Label("Antal Solgt: ");
    private final Label lblAmountEarned = new Label("Penge Tjent: ");
    private final Label lblClipUsed = new Label("Klip brugt");
    private final Button btnOneDay = new Button("1 Dag");
    private final Button btnOneWeek = new Button("1 Uge");
    private final Button btnOneMonth = new Button("1 Måned");
    private final Button btnSixMonths = new Button("6 Måned");
    private final Button btnOneYear = new Button("1 År");
    private final Button btnAlways = new Button("Altid");
    private final Separator v1Separator = new Separator();
    private final Separator v2Separator = new Separator();
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    public StatisticDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);
        this.setTitle("Statistik");
        GridPane pane = new GridPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent(GridPane pane)
    {
        pane.setGridLinesVisible(false);
        pane.setPadding(new Insets(20));
        pane.setHgap(10);
        pane.setVgap(10);

        // --------------------- Labels ---------------------------------------

        lblHeading1.setFont(new Font("CALIBRI", 20));
        pane.add(lblHeading1, 0, 0);
        lblHeading2.setFont(new Font("CALIBRI", 20));
        pane.add(lblHeading2, 5, 0);
        lblHeading4.setFont(new Font("CALIBRI", 20));
        pane.add(lblHeading4, 5, 9);
        lblHeading3.setFont(new Font("CALIBRI", 20));
        pane.add(lblHeading3, 9, 0);
        pane.add(lblDateStart, 9, 44);
        pane.add(lblDateEnd, 10, 44);
        pane.add(lblProductName, 5, 4);
        pane.add(lblProductSold, 5, 5);
        pane.add(lblAmountEarned, 5, 6);
        pane.add(lblClipUsed, 5, 10);
        pane.add(txfProductName, 6, 4);
        pane.add(txfProductSold, 6, 5);
        pane.add(txfAmountEarned, 6, 6);
        pane.add(txfClipsUsed, 6, 10);

        // ------------------- Seperators --------------------------------------

        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        pane.add(v1Separator, 4, 0, 1, 70);
        v2Separator.orientationProperty().set(Orientation.VERTICAL);
        pane.add(v2Separator, 8, 0, 1, 70);

        // ------------------ ChangeListener ------------------------------

        ChangeListener<PriceList> comboBoxListener = (ov, o, n) -> controller.setPriceDetail();
        comboPriceList.getSelectionModel().selectedItemProperty().addListener(comboBoxListener);

        ChangeListener<Price> allProductsListener = (ov, o, n) -> controller.setStatisticDetails();
        allProducts.getSelectionModel().selectedItemProperty().addListener(allProductsListener);

        dateEndPicker.setOnAction(a -> controller.setStatisticDetails());
        dateStartPicker.setOnAction(a -> controller.setStatisticDetails());

        // ------------------- Combobox --------------------------------------

        pane.add(comboPriceList, 2, 2);
        GridPane.setHalignment(comboPriceList, HPos.RIGHT);
        comboPriceList.getItems().setAll(service.getAllPriceLists());
        comboPriceList.setValue(service.getAllPriceLists().get(0));

        // -------------------- Date Picker --------------------------------

        pane.add(dateStartPicker, 9, 45, 1, 1);
        pane.add(dateEndPicker, 10, 45, 1, 1);
        dateStartPicker.setValue(LocalDate.now().minusMonths(1));
        dateEndPicker.setValue(LocalDate.now());
        txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));

        // -------------------- Buttons --------------------------------------

        pane.add(btnOneDay, 9, 46);
        btnOneDay.setOnAction(event -> controller.oneDayAct());
        pane.add(btnOneWeek, 10, 46);
        btnOneWeek.setOnAction(event -> controller.oneWeekAct());
        pane.add(btnOneMonth, 9, 47);
        btnOneMonth.setOnAction(event -> controller.oneMonthAct());
        pane.add(btnSixMonths, 10, 47);
        btnSixMonths.setOnAction(event -> controller.sixMonthsAct());
        pane.add(btnOneYear, 9, 48);
        btnOneYear.setOnAction(event -> controller.oneYearAct());
        pane.add(btnAlways, 10, 48);
        btnAlways.setOnAction(event -> controller.alwaysAct());

        //--------------------- TableView Price ----------------------------------------

        allProducts.setEditable(false);

        TableColumn<Price, String> product = new TableColumn<Price, String>("Produktnavn");
        product.setMinWidth(200);
        product.setCellValueFactory(new PropertyValueFactory<Price, String>("product"));

        TableColumn<Price, String> price = new TableColumn<Price, String>("Pris");
        price.setMinWidth(100);
        price.setCellValueFactory(new PropertyValueFactory<Price, String>("price"));

        // priceObject used in Lambda function to get value IdNo from Product in Price constructor - Parsed to String and
        // afterwards packed in a ReadOnlyStringWrapper to convert String to an ObservableValue<String>.

        TableColumn<Price, String> productID = new TableColumn<Price, String>("ID");
        productID.setMinWidth(60);
        productID.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        Integer.toString(priceObject.getValue().getProduct().getIdNo())));

        allProducts.getColumns().addAll(product, price, productID);
        pane.add(allProducts, 0, 3, 4, 55);

        allProducts.setMaxWidth(450);

        // -------------------- TableView Orders -----------------------------------------

        TableColumn<Order, String> saleDateTime = new TableColumn<Order, String>("Salgs Tid");
        saleDateTime.setMinWidth(135);
        saleDateTime.setCellValueFactory(
                saleDateTimeObject -> new ReadOnlyStringWrapper(
                        "" + saleDateTimeObject.getValue().getDate().format(formatter)));

        TableColumn<Order, String> totalOrderPrice = new TableColumn<Order, String>("Total Ordre Pris");
        totalOrderPrice.setMinWidth(135);
        totalOrderPrice.setCellValueFactory(totalOrderPriceObject -> new ReadOnlyStringWrapper(
                Double.toString(totalOrderPriceObject.getValue().calculatePrice())));

        TableColumn<Order, String> isOrderPayed = new TableColumn<Order, String>("Ordre Betalt");
        isOrderPayed.setMinWidth(135);
        //Changes shown text of boolean isPayed.
        isOrderPayed.setCellValueFactory(cellData -> {
            boolean payed = cellData.getValue().isPayed();
            String payment;
            if (payed == true)
            {
                payment = "Betalt";
            }
            else
            {
                payment = "Ikke Betalt";
            }

            return new ReadOnlyStringWrapper(payment);
        });

        TableColumn<Order, String> customerNameField = new TableColumn<Order, String>("Kunde");
        customerNameField.setMinWidth(176);
        customerNameField.setCellValueFactory(customerObject -> {
            String tempField;
            if (customerObject.getValue().getCustomer() == null)
            {
                tempField = "N/A";
            }
            else
            {
                tempField = customerObject.getValue().getCustomer().getName();
            }
            return new ReadOnlyStringWrapper(tempField);

        });

        TableColumn<Order, String> customerPhoneField = new TableColumn<Order, String>("Tlf");
        customerPhoneField.setMinWidth(60);

        customerPhoneField.setCellValueFactory(customerObject -> {
            String tempField;
            if (customerObject.getValue().getCustomer() == null)
            {
                tempField = "N/A";
            }
            else
            {
                tempField = customerObject.getValue().getCustomer().getPhoneNumber();
            }
            return new ReadOnlyStringWrapper(tempField);
        });

        allOrders.getColumns().addAll(saleDateTime, totalOrderPrice, isOrderPayed, customerNameField,
                customerPhoneField);
        pane.add(allOrders, 9, 3, 3, 40);

        allOrders.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        allProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // -------------------------- Key Events ---------------------------------

        /**
         * KEYPRESS | LOCATION       | ACTION
         * DOWN     | SearchField    | Sets focus on allProducts index (0)
         * Enter    | SearchField    | Sets focus on allProducts index (0)
         */

        searchField.setOnKeyPressed(e -> {

            if (e.getCode() == KeyCode.DOWN)
            {
                focusTableView();

            }

            if (e.getCode() == KeyCode.ENTER)
            {
                focusTableView();
            }

        });

        /**
         * Creates search field where you find products which includes the string you write
         * in their name
         */

        // --------------------- Search Function Products --------------------------------

        final ObservableList<Price> data = FXCollections
                .observableArrayList(comboPriceList.getSelectionModel().getSelectedItem().getPrices());

        pane.add(searchField, 0, 2);
        searchField.setFont(Font.font("CALIBRI"));
        searchField.setPromptText("Søgefelt");

        FilteredList<Price> filtPrice = new FilteredList<>(data, e -> true);
        searchField.setOnKeyReleased(e -> {
            searchField.textProperty().addListener((observableValue, oldVal, newVal) -> {
                filtPrice.setPredicate((Predicate<? super Price>) p -> {
                    if (newVal == null || newVal.isEmpty())
                    {
                        return true;
                    }
                    String lcf = newVal.toLowerCase();
                    if (p.getProduct().getName().toLowerCase().contains(lcf))
                    {
                        return true;
                    }

                    return false;
                });
            });
            SortedList<Price> sortedProduct = new SortedList<>(filtPrice);
            sortedProduct.comparatorProperty().bind(allProducts.comparatorProperty());
            allProducts.setItems(sortedProduct);
        });

    }
    // ------------------------- Methods -------------------------------------------

    /**
     * Sets focus on the products table view, selecting the top row
     */
    public void focusTableView()
    {
        allProducts.requestFocus();
        allProducts.getSelectionModel().select(0);
        allProducts.getFocusModel().focus(0);
    }

    // ----------------------- controller ----------------------------

    private class Controller
    {

        public void setStatisticDetails()
        {
            int soldNumber = 0;
            double amountEarned = 0;
            ArrayList<Order> orders = new ArrayList<>();

            LocalDate StartPickerValue = dateStartPicker.getValue();
            LocalDate EndPickerValue = dateEndPicker.getValue();

            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));

            if (allProducts.getSelectionModel().getSelectedItem() != null)
            {
                Product p1 = allProducts.getSelectionModel().getSelectedItem().getProduct();

                for (Order order : service.getAllOrders())
                {
                    LocalDate OrderValue = order.getDate().toLocalDate();
                    if (OrderValue.isAfter(StartPickerValue) && OrderValue.isBefore(EndPickerValue)
                            || OrderValue.isEqual(EndPickerValue))
                    {
                        soldNumber += order.amountSold(p1);
                        amountEarned += order.amountEarned(p1);

                        if (order.includeProduct(p1) == true)
                        {
                            orders.add(order);
                        }
                    }
                }

                txfProductName.setText(p1.getName());
                txfProductSold.setText("" + soldNumber);
                txfAmountEarned.setText("" + amountEarned + " KR");

                allOrders.setItems(FXCollections
                        .observableArrayList(orders));
            }

        }

        /**
         * Fills allProducts with prices and their connected prodcuts
         */
        public void setPriceDetail()
        {
            if (comboPriceList.getSelectionModel().getSelectedItem() != null)
            {
                allProducts.setItems(FXCollections
                        .observableArrayList(comboPriceList.getSelectionModel().getSelectedItem().getPrices()));
            }
        }

        public void oneDayAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusDays(1));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

        public void oneWeekAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusWeeks(1));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1 month back
         */
        public void oneMonthAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(1));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 6 months back
         */
        public void sixMonthsAct()
        {

            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(6));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1 year back
         */
        public void oneYearAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusYears(1));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1000 months back
         */
        public void alwaysAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(1000));
            txfClipsUsed.setText("" + service.clipsUsedInPeriod(dateStartPicker.getValue(), dateEndPicker.getValue()));
        }

    }

}
