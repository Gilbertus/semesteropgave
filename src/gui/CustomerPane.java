package gui;

import java.time.format.DateTimeFormatter;
import java.util.function.Predicate;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import application.model.Customer;
import application.model.Order;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class CustomerPane extends GridPane
{
    Service service = new Service();
    private final Controller controller = new Controller();

    public static Order tempOrder;

    private final TextField txfName = new TextField();
    private final TextField txfAddress = new TextField();
    private final TextField txfPhone = new TextField();
    private final TextField txfEmail = new TextField();

    private final Button addbtn = new Button("Ny Kunde");
    private final Button delbtn = new Button("Slet Kunde");
    private final Button updbtn = new Button("Opdatere Kunde");

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    private TableView<Customer> allCustomers = new TableView<Customer>();
    private TableView<Order> allOrders = new TableView<Order>();

    private final ObservableList<Customer> customers = FXCollections
            .observableArrayList(service.getAllCustomers());

    public CustomerPane()
    {
        this.initContent();
    }

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent()
    {

        this.setGridLinesVisible(false);
        this.setPadding(new Insets(20));
        this.setHgap(10);
        this.setVgap(10);

        // --------------------- Labels ---------------------------------------

        Label lbl2 = new Label("Kunde Detaljer");
        Label lbl3 = new Label("Navn: ");
        Label lbl4 = new Label("Addresse: ");
        Label lbl5 = new Label("Tlf Nummer: ");
        Label lbl6 = new Label("Email: ");

        lbl2.setFont(Font.font(23));

        this.add(lbl2, 4, 0, 2, 1);
        this.add(lbl3, 4, 3);
        this.add(lbl4, 4, 4);
        this.add(lbl5, 4, 5);
        this.add(lbl6, 4, 6);

        // ----------------------------- Buttons on HBox --------------------------------------
        this.add(delbtn, 4, 7, 2, 1);
        this.add(updbtn, 5, 7, 2, 1);
        this.add(addbtn, 2, 0);
        GridPane.setHalignment(addbtn, HPos.RIGHT);

        delbtn.setOnAction(event -> controller.deleteAction());
        addbtn.setOnAction(event -> controller.addAction());
        updbtn.setOnAction(event -> controller.updateAction());

        // -----------------------------Line Separator----------------------------------

        Separator h1Separator = new Separator();
        Separator v1Separator = new Separator();
        Separator v2Separator = new Separator();

        h1Separator.orientationProperty().set(Orientation.HORIZONTAL);
        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        v2Separator.orientationProperty().set(Orientation.VERTICAL);

        this.add(v1Separator, 3, 0, 1, 78);
        this.add(v2Separator, 7, 0, 1, 78);
        this.add(h1Separator, 0, 1, 15, 1);

        // ---------------------------- Text Fields -------------------------------------------

        this.add(txfName, 5, 3);
        txfName.setPrefWidth(300);
        this.add(txfAddress, 5, 4);
        this.add(txfPhone, 5, 5);
        this.add(txfEmail, 5, 6);

        // ------------------------------ Customers TableView ---------------------------

        TableColumn<Customer, String> customerName = new TableColumn<Customer, String>("Navn");
        customerName.setMinWidth(200);
        customerName.setCellValueFactory(new PropertyValueFactory<Customer, String>("name"));

        TableColumn<Customer, String> customerAddress = new TableColumn<Customer, String>("Addresse");
        customerAddress.setMinWidth(200);
        customerAddress.setCellValueFactory(new PropertyValueFactory<Customer, String>("address"));

        TableColumn<Customer, String> customerPhone = new TableColumn<Customer, String>("Tlf Nummer");
        customerPhone.setMinWidth(200);
        customerPhone.setCellValueFactory(new PropertyValueFactory<Customer, String>("phoneNumber"));

        TableColumn<Customer, String> customerEmail = new TableColumn<Customer, String>("Email");
        customerEmail.setMinWidth(200);
        customerEmail.setCellValueFactory(new PropertyValueFactory<Customer, String>("email"));

        allCustomers.setItems(customers);
        allCustomers.getColumns().addAll(customerName, customerAddress, customerPhone, customerEmail);
        allCustomers.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        this.add(allCustomers, 0, 2, 3, 45);

        // ------------------------------ Customers Orders TableView -------------------------------

        TableColumn<Order, String> saleDateTime = new TableColumn<Order, String>("Salgs Tid");
        saleDateTime.setMinWidth(135);
        saleDateTime.setCellValueFactory(
                saleDateTimeObject -> new ReadOnlyStringWrapper(
                        "" + saleDateTimeObject.getValue().getDate().format(formatter)));

        TableColumn<Order, String> totalOrderPrice = new TableColumn<Order, String>("Total Ordre Pris");
        totalOrderPrice.setMinWidth(135);
        totalOrderPrice.setCellValueFactory(totalOrderPriceObject -> new ReadOnlyStringWrapper(
                Double.toString(totalOrderPriceObject.getValue().calculatePrice())));

        TableColumn<Order, String> isOrderPayed = new TableColumn<Order, String>("Ordre Betalt");
        isOrderPayed.setMinWidth(135);
        //Changes shown text of boolean isPayed.
        isOrderPayed.setCellValueFactory(cellData -> {
            boolean payed = cellData.getValue().isPayed();
            String payment;
            if (payed == true)
            {
                payment = "Betalt";
            }
            else
            {
                payment = "Ikke Betalt";
            }

            return new ReadOnlyStringWrapper(payment);
        });
        allOrders.getColumns().addAll(saleDateTime, totalOrderPrice, isOrderPayed);
        this.add(allOrders, 9, 2, 3, 45);

        // -------------------- Change Listener -------------------------------

        ChangeListener<Customer> tableListener = (ov, o, n) -> controller.setOrderDetails();
        allCustomers.getSelectionModel().selectedItemProperty().addListener(tableListener);

        // --------------------- Key/Mouse actions ----------------------------

        /**
         * MOUSEPRESS    | LOCATION       | ACTION
         * Double-Click  | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         * KEYPRESS      | LOCATION       | ACTION
         * ENTER         | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         */

        allOrders.setRowFactory(allOrdMouse -> {
            TableRow<Order> orderRow = new TableRow<>();
            orderRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!orderRow.isEmpty()))
                {
                    setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                    controller.getOrderInformation();

                }

            });
            return orderRow;
        });

        allOrders.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && (!allOrders.getSelectionModel().getSelectedItem().equals(null)))
            {
                setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                controller.getOrderInformation();

            }

        });

        // --------------------- Search Function --------------------------------

        /**
         * Creates search field where you find customers which includes the string you write
         * in their name or phone number
         */

        TextField searchField = new TextField();
        this.add(searchField, 0, 0);
        searchField.setFont(Font.font("CALIBRI"));
        searchField.setPromptText("Søgefelt");

        FilteredList<Customer> filtProduct = new FilteredList<>(customers, e -> true);
        searchField.setOnKeyReleased(e -> {
            searchField.textProperty().addListener((observableValue, oldVal, newVal) -> {
                filtProduct.setPredicate((Predicate<? super Customer>) customer -> {

                    if (newVal == null || newVal.isEmpty())
                    {
                        return true;
                    }

                    String lcf = newVal.toLowerCase();
                    if (customer.getName().toLowerCase().contains(lcf) ||
                            customer.getPhoneNumber().contains(lcf))
                    {
                        return true;
                    }
                    int value = Integer.parseInt(newVal);
                    if (Integer.parseInt(customer.getPhoneNumber()) == value)
                    {
                        return true;
                    }

                    return false;
                });
            });
            SortedList<Customer> sortedCustomer = new SortedList<>(filtProduct);
            sortedCustomer.comparatorProperty().bind(allCustomers.comparatorProperty());
            allCustomers.setItems(sortedCustomer);

        });
    }

    // -------------------------- GETTERS AND SETTERS ----------------------------------------
    public static Order getTempOrder()
    {
        return tempOrder;
    }

    public static void setTempOrder(Order tempOrder)
    {
        CustomerPane.tempOrder = tempOrder;
    }

    /**
     * Updates CustomerPane when CustomerTab is selected
     */
    public void updateControls()
    {
        allCustomers.refresh();

        if (allCustomers.getSelectionModel().getSelectedItem() != null)
        {
            ObservableList<Order> orders = FXCollections
                    .observableArrayList(allCustomers.getSelectionModel().getSelectedItem().getOrders());
            allOrders.setItems(orders);
        }

        allOrders.refresh();

    }

    public void mouseEvent()
    {
        // ------------------------------ Key/Mouse actions ----------------------------

        /**
         * MOUSEPRESS    | LOCATION       | ACTION
         * Double-Click  | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         * KEYPRESS      | LOCATION       | ACTION
         * ENTER         | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         */

        allOrders.setRowFactory(allOrdMouse -> {
            TableRow<Order> orderRow = new TableRow<>();
            orderRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!orderRow.isEmpty()))
                {
                    setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                    controller.getOrderInformation();

                }

            });
            return orderRow;
        });

        allOrders.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && (!allOrders.getSelectionModel().getSelectedItem().equals(null)))
            {
                setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                controller.getOrderInformation();

            }

        });
    }

    // -------------------------------- controller -------------------------------------

    private class Controller
    {

        /**
         * Fills the Order TableView with the selected customer's orders
         */
        public void setOrderDetails()
        {
            try
            {
                Customer c1 = allCustomers.getSelectionModel().getSelectedItem();
                txfAddress.setText(c1.getAddress());
                txfName.setText(c1.getName());
                txfPhone.setText(c1.getPhoneNumber());
                txfEmail.setText(c1.getEmail());

                ObservableList<Order> orders = FXCollections
                        .observableArrayList(allCustomers.getSelectionModel().getSelectedItem().getOrders());

                allOrders.setItems(orders);
            }

            catch (NullPointerException np)
            {
                System.out.println("NullPointerException - CustomerPane 270 - 310");
            }

        }

        /**
         * Opens OrderPaneOrderDialog + refresh allOrders
         */
        public void getOrderInformation()
        {
            Stage stage = new Stage();
            OrderPaneOrderDialog dialog = new OrderPaneOrderDialog(stage);
            dialog.showAndWait();

            allOrders.refresh();
        }

        /**
         * Opens CustomerDialog - Creates new Customer with information fomr CustomerDialog
         */
        public void addAction()
        {

            Stage stage = new Stage();
            CustomerDialog dialog = new CustomerDialog(stage);
            dialog.showAndWait();
            // ... wait for the dialog to close

            allCustomers.setItems(FXCollections
                    .observableArrayList(service.getAllCustomers()));
            allCustomers.requestFocus();
            allCustomers.refresh();

        }

        /**
         * Delete selected customer
         */
        public void deleteAction()
        {
            try
            {
                Customer tempCustomer = allCustomers.getSelectionModel().getSelectedItem();
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setTitle("Bekræftigelse vindue");
                alert.setHeaderText("Kunde: " + tempCustomer.getName());
                alert.setContentText(
                        "Er du sikker på du vil slette den valgte kunde: \n"
                                + tempCustomer.getName());

                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK)
                {
                    for (Order order : tempCustomer.getOrders())
                    {
                        tempCustomer.removeOrder(order);
                    }
                    service.deleteCustomer(tempCustomer);
                }

                allCustomers.setItems(FXCollections
                        .observableArrayList(service.getAllCustomers()));
                allCustomers.refresh();
            }
            catch (NullPointerException nP)
            {
                System.out.println("NullPointerException CustomerPane #352");
            }
        }

        /**
         * Update selected customer by changing information in the textFields
         */
        public void updateAction()
        {
            Customer tempCustomer = allCustomers.getSelectionModel().getSelectedItem();
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("Bekræftigelse vindue");
            alert.setHeaderText("Kunde: " + tempCustomer.getName());
            alert.setContentText(
                    "Er du sikker på du vil opdatere den valgte kunde: \n"
                            + tempCustomer.getName());

            alert.showAndWait();

            if (alert.getResult() == ButtonType.OK)
            {

                try
                {
                    int test = Integer.parseInt(tempCustomer.getPhoneNumber());
                    if (txfPhone.getText().length() == 8)
                    {

                        tempCustomer.setAddress(txfAddress.getText());
                        tempCustomer.setName(txfName.getText());
                        tempCustomer.setPhoneNumber(txfPhone.getText());
                        tempCustomer.setEmail(txfEmail.getText());

                        allCustomers.setItems(FXCollections
                                .observableArrayList(service.getAllCustomers()));
                        allCustomers.refresh();
                    }

                    else
                    {

                    }

                }
                catch (ParseException e)
                {
                    System.out.println("Exception CustomerPane #388");
                }

                catch (IllegalArgumentException e)
                {
                    System.out.println("Exception CustomerPane #393");
                }

                catch (NullPointerException e)
                {
                    System.out.println("Exception CustomerPane #398");
                }
            }
        }
    }
}
