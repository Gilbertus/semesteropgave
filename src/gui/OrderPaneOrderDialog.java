package gui;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import application.model.DepositProduct;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OrderPaneOrderDialog extends Stage
{

    private final Controller controller = new Controller();
    private TableView<OrderLine> orderLines = new TableView<OrderLine>();
    private TableView<OrderLine> orderLinesToRemove = new TableView<OrderLine>();
    private TableView<Payment> payments = new TableView<Payment>();

    private ArrayList<OrderLine> returnProducts = new ArrayList<>();

    private static Order tempDialogOrder;

    private Order tempOrderToCalcPrice = new Order(LocalDateTime.now());

    private static boolean OrderDialog;
    private boolean isDepositOrder;
    private boolean firstCalc;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    // -----------------------initialize new stage and scene--------------

    public OrderPaneOrderDialog(Stage owner)
    {
        isDepositOrder = false;
        firstCalc = true;

        if (OrderPane.isOrderPane == true)
        {
            tempDialogOrder = OrderPane.getTempOrder();
        }

        else
        {
            tempDialogOrder = CustomerPane.getTempOrder();
        }

        for (OrderLine orderline : tempDialogOrder.getOrderLines())
        {
            if (orderline.getProduct() instanceof DepositProduct)
            {

                isDepositOrder = true;

            }

        }

        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Ordre Informaiton");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private final TextField txfDate = new TextField();
    private final TextField txfTotalCost = new TextField();
    private final TextField txfAmountPayed = new TextField();
    private final TextField txfRemainingAmount = new TextField();

    private final TextField txfCusName = new TextField();
    private final TextField txfCusAdd = new TextField();
    private final TextField txfCusPho = new TextField();

    Button btnCancel = new Button("Cancel");
    Button btnOK = new Button("OK");
    Button btnAdd = new Button("Tilføj");
    Button btnRem = new Button("Fjern");

    HBox buttonBox = new HBox(10);

    @SuppressWarnings("unchecked")
    private void initContent(GridPane pane)
    {

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------
        Label lbl1 = new Label("Dato af salg: ");
        pane.add(lbl1, 4, 0);

        Label lbl2 = new Label("Total Beløb: ");
        pane.add(lbl2, 4, 1);

        Label lbl3 = new Label("Betalt: ");
        pane.add(lbl3, 4, 2);

        Label lbl4 = new Label("Resterende beløb: ");
        pane.add(lbl4, 4, 3);

        Label lbl8 = new Label("Kunde Detaljer: ");
        lbl8.setFont(new Font("CALIBRI", 20));
        pane.add(lbl8, 4, 5);

        Label lbl5 = new Label("Navn: ");
        pane.add(lbl5, 4, 6);

        Label lbl6 = new Label("Adresse: ");
        pane.add(lbl6, 4, 7);

        Label lbl7 = new Label("Tlf nummer: ");
        pane.add(lbl7, 4, 8);

        // -------------------------- Text Fields ----------------------------

        pane.add(txfDate, 5, 0);
        pane.add(txfRemainingAmount, 5, 3);
        pane.add(txfAmountPayed, 5, 2);
        pane.add(txfTotalCost, 5, 1);

        txfDate.setEditable(false);
        txfTotalCost.setEditable(false);
        txfAmountPayed.setEditable(false);
        txfRemainingAmount.setEditable(false);

        updateTxf();

        pane.add(txfCusName, 5, 6, 2, 1);
        txfCusName.setEditable(false);
        pane.add(txfCusAdd, 5, 7, 2, 1);
        txfCusAdd.setEditable(false);
        pane.add(txfCusPho, 5, 8, 2, 1);
        txfCusPho.setEditable(false);

        // -------------------------- Buttons -------------------------------

        if (isDepositOrder == true)
        {
            pane.add(btnAdd, 0, 11);
            pane.add(btnRem, 1, 11);
        }

        btnAdd.setOnAction(event -> controller.addAction());
        btnRem.setOnAction(event -> controller.remAction());

        pane.add(buttonBox, 4, 4);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);

        Button btnAddPayment = new Button("Tilføj Betaling");
        buttonBox.getChildren().add(btnAddPayment);
//        btnAddPayment.setPrefWidth(140);
        btnAddPayment.setCancelButton(true);
        btnAddPayment.setOnAction(event -> controller.addPaymentAct());

        Button btnOK = new Button("OK");
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.okAction());

        // -------------------------- TableView OrderLines -----------------------------

        TableColumn<OrderLine, String> proNavn = new TableColumn<OrderLine, String>("Produktnavn");
        proNavn.setMinWidth(130);
        proNavn.setCellValueFactory(
                navnObject -> new ReadOnlyStringWrapper(
                        navnObject.getValue().getProduct().getName()));

        TableColumn<OrderLine, String> proAntal = new TableColumn<OrderLine, String>("Antal");
        proAntal.setMinWidth(85);
        proAntal.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("numberOfProducts"));

        TableColumn<OrderLine, String> proFullAmount = new TableColumn<OrderLine, String>("Samlet Pris");
        proFullAmount.setMinWidth(100);
        proFullAmount.setCellValueFactory(
                amountObject -> new ReadOnlyStringWrapper(
                        Double.toString(amountObject.getValue().getFullAmount())));

        orderLines.getColumns().addAll(proNavn, proAntal, proFullAmount);
        orderLines.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        pane.add(orderLines, 0, 0, 3, 9);
        setOrderLines();

        // -------------------------- TableView OrderLines To Remove -----------------------------

        TableColumn<OrderLine, String> proNavn1 = new TableColumn<OrderLine, String>("Produktnavn");
        proNavn1.setMinWidth(130);
        proNavn1.setCellValueFactory(
                navnObject -> new ReadOnlyStringWrapper(
                        navnObject.getValue().getProduct().getName()));

        TableColumn<OrderLine, String> proAntal1 = new TableColumn<OrderLine, String>("Antal");
        proAntal1.setMinWidth(85);
        proAntal1.setCellValueFactory(new PropertyValueFactory<OrderLine, String>("numberOfProducts"));

        TableColumn<OrderLine, String> proFullAmount1 = new TableColumn<OrderLine, String>("Samlet Pris");
        proFullAmount1.setMinWidth(100);
        proFullAmount1.setCellValueFactory(
                amountObject -> new ReadOnlyStringWrapper(
                        Double.toString(amountObject.getValue().getFullAmount())));

        orderLinesToRemove.getColumns().addAll(proNavn1, proAntal1, proFullAmount1);
        orderLinesToRemove.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        orderLinesToRemove.setMinHeight(200);
        if (isDepositOrder == true)
        {
            pane.add(orderLinesToRemove, 0, 12, 3, 3);
        }

        // -------------------------- TableView Payments -----------------------------

        TableColumn<Payment, String> paymentType = new TableColumn<Payment, String>("Betalings Metode");
        paymentType.setMinWidth(130);
        paymentType.setCellValueFactory(new PropertyValueFactory<Payment, String>("paymentType"));

        TableColumn<Payment, String> paymentAmount = new TableColumn<Payment, String>("Betalt Beløb");
        paymentAmount.setMinWidth(130);
        paymentAmount.setCellValueFactory(new PropertyValueFactory<Payment, String>("amount"));

        payments.getColumns().addAll(paymentType, paymentAmount);
        pane.add(payments, 12, 0, 1, 14);
        payments.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        setPayments();

    }

    // --------------------------- GETTERS AND SETTERS --------------------------

    public static Order gettempDialogOrder()
    {
        return tempDialogOrder;
    }

    public ArrayList<OrderLine> getReturnProducts()
    {
        return returnProducts;
    }

    public static boolean isOrderDialog()
    {
        return OrderDialog;
    }

    public void setOrderDialog(boolean orderDialog)
    {
        OrderPaneOrderDialog.OrderDialog = orderDialog;
    }

    /**
     * Fills the TableView orderLines with OrderLines.
     * Has to be method as every OrderLine needs the FullAmount set.
     */
    private void setOrderLines()
    {
        ArrayList<OrderLine> tempDialogOrderLineArray = new ArrayList<>();

        for (OrderLine oL1 : tempDialogOrder.getOrderLines())
        {
            oL1.setFullAmount();
            tempDialogOrderLineArray.add(oL1);

        }

        orderLines.setItems(FXCollections
                .observableArrayList(tempDialogOrderLineArray));

    }

    private void setPayments()
    {

        payments.setItems(FXCollections
                .observableArrayList(tempDialogOrder.getPayments()));
        payments.refresh();
    }

    private void updateTxf()
    {
        // -------- Customer information ----------------
        if (tempDialogOrder.getCustomer() != null)
        {

            txfCusName.setText(tempDialogOrder.getCustomer().getName());

            txfCusAdd.setText(tempDialogOrder.getCustomer().getAddress());

            txfCusPho.setText(tempDialogOrder.getCustomer().getPhoneNumber());

        }

        else
        {

            txfCusName.setText("N/A");

            txfCusAdd.setText("N/A");

            txfCusPho.setText("N/A");
        }

        // ---------------- Price Information --------------------------

        txfDate.setText("" + tempDialogOrder.getDate().format(formatter));

        if (isDepositOrder == true)
        {
            Order o3 = tempDialogOrder;

            if (firstCalc == true)
            {
                txfTotalCost.setText(Double
                        .toString(o3.calculatePrice()));
                firstCalc = false;
            }

            txfAmountPayed.setText(Double.toString(o3.calculatePayed()));

            txfRemainingAmount
                    .setText(Double
                            .toString(o3.calculatePriceWhenDelivered()));

        } else
        {

            txfTotalCost.setText(Double.toString(tempDialogOrder.calculatePrice()));
            txfAmountPayed.setText(Double.toString(tempDialogOrder.calculatePayed()));
            txfRemainingAmount
                    .setText(Double.toString(tempDialogOrder.calculateRemainingPrice()));

        }

    }

    // -------------------------------------------------------------------------

    private class Controller
    {

        public void addAction()
        {

            boolean isNewItem = true;

            if (orderLines.getSelectionModel().getSelectedItem() != null)
            {
                OrderLine pickedOrderLine = orderLines.getSelectionModel().getSelectedItem();

                OrderLine o1 = tempOrderToCalcPrice.createOrderLine(1, pickedOrderLine.getDiscountedPrice(),
                        pickedOrderLine.getProduct());

                for (OrderLine orderLine : returnProducts)
                {
                    if (orderLine.getProduct() == pickedOrderLine.getProduct())
                    {
                        orderLine.setNumberOfProducts(orderLine.getNumberOfProducts() + 1);
                        orderLine.setFullAmount();
                        isNewItem = false;
                    }
                }
                if (isNewItem == true)
                {
                    returnProducts.add(o1);
                }

                if (pickedOrderLine.getNumberOfProducts() > 0)
                {
                    pickedOrderLine.setNumberOfProducts(pickedOrderLine.getNumberOfProducts() - 1);
                }

                if (pickedOrderLine.getNumberOfProducts() <= 0)
                {

                    tempDialogOrder.removeOrderLine(pickedOrderLine);
                }

                orderLinesToRemove.setItems(FXCollections
                        .observableArrayList(returnProducts));

                setOrderLines();
                orderLines.refresh();
                orderLinesToRemove.refresh();
                updateTxf();
            }

        }

        public void remAction()
        {

            boolean isNewItem = true;

            if (orderLinesToRemove.getSelectionModel().getSelectedItem() != null)
            {
                OrderLine pickedOrderLine = orderLinesToRemove.getSelectionModel().getSelectedItem();

                for (OrderLine orderLine : tempDialogOrder.getOrderLines())
                {
                    if (orderLine.getProduct() == pickedOrderLine.getProduct())
                    {
                        orderLine.setNumberOfProducts(orderLine.getNumberOfProducts() + 1);
                        orderLine.setFullAmount();
                        isNewItem = false;
                    }
                }
                if (isNewItem == true)
                {
                    tempDialogOrder.createOrderLine(1, pickedOrderLine.getDiscountedPrice(),
                            pickedOrderLine.getProduct());
                }

                if (pickedOrderLine.getNumberOfProducts() > 0)
                {
                    pickedOrderLine.setNumberOfProducts(pickedOrderLine.getNumberOfProducts() - 1);
                }

                if (pickedOrderLine.getNumberOfProducts() <= 0)
                {

                    returnProducts.remove(pickedOrderLine);
                }

                orderLinesToRemove.setItems(FXCollections
                        .observableArrayList(returnProducts));

                setOrderLines();
                orderLinesToRemove.refresh();
                orderLines.refresh();
                updateTxf();
            }

        }

        public void okAction()
        {

            OrderPaneOrderDialog.this.hide();
            OrderDialog = false;

        }

        public void addPaymentAct()
        {
            Service service = new Service();
            OrderDialog = true;

            if (tempDialogOrder.isPayed() != true)
            {

                {
                    try
                    {

                        Stage stage = new Stage();
                        OrderPanePaymentDialog dialog = new OrderPanePaymentDialog(stage);
                        dialog.showAndWait();

                        if (dialog.isConfirmedSold() == true)
                        {

                            if (dialog.getMoneyPayed() <= tempDialogOrder.calculateRemainingPrice()
                                    && dialog.getMoneyPayed() != 0)
                            {

                                Payment payment = service.createPayment(dialog.getTypeOfPayment(),
                                        dialog.getMoneyPayed());

                                tempDialogOrder.addPayment(payment);

//                                if (isDepositOrder == true)
//                                {
//
//
//
//
//                                } else
//                                {
                                if (tempDialogOrder.calculateRemainingPrice() == 0)
                                {
                                    tempDialogOrder.setPayed(true);
                                }
//                                }

                                setPayments();
                                updateTxf();

                            } else
                            {
                                throw new RuntimeException();
                            }

                        }
                    }

                    catch (RuntimeException rte)
                    {
                        System.out.println("RuntimeException - OrderPaneOrderDialog");
                    }
                }

            }
            OrderDialog = false;
        }
    }
}
