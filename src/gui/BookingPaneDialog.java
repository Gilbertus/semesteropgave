package gui;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class BookingPaneDialog extends Stage
{
    public static double discountedPrice;
    public static int numberOfProducts;

    private final Controller controller = new Controller();
    private boolean isConfirmed = false;
    private final TextField txfNumberOfProducts = new TextField();
    private final TextField txfDiscountedPrice = new TextField();
    private final Label lblError = new Label("Ugyldigt Input");
    private Button btnCancel = new Button("Annuller");
    private Button btnOK = new Button("Ok");

    HBox buttonBox = new HBox(10);

    // -----------------------initialize new stage and scene--------------

    public BookingPaneDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);

        this.setTitle("Antal");
        GridPane pane = new GridPane();
        this.initContent(pane);

        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private void initContent(GridPane pane)
    {
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------

        Label lblAmount = new Label("Antal: ");
        Label lblNewPrice = new Label("Ny pris per produkt: ");

        pane.add(lblAmount, 0, 0);
        pane.add(lblNewPrice, 0, 1);
        pane.add(lblError, 1, 2);

        lblError.setVisible(false);
        lblError.setTextFill(Color.RED);

        // -------------------------- Text Fields ----------------------------

        txfNumberOfProducts.setText("1");
        txfDiscountedPrice.setText(Double.toString(BookingPane.getCurrentPrice()));
        pane.add(txfNumberOfProducts, 1, 0);
        pane.add(txfDiscountedPrice, 1, 1);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 0, 2);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);

        buttonBox.getChildren().add(btnCancel);
        buttonBox.getChildren().add(btnOK);

        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);

        btnCancel.setOnAction(event -> controller.cancelAction());
        btnOK.setOnAction(event -> controller.okAction());

        // -------------------------- Key Event --------------------------------

        txfNumberOfProducts.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.UP)
            {
                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());
                numberOfProducts++;
                txfNumberOfProducts.setText("" + numberOfProducts);
            }
            if (e.getCode() == KeyCode.DOWN)
            {

                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());
                if (numberOfProducts > 1)
                {
                    numberOfProducts--;
                    txfNumberOfProducts.setText("" + numberOfProducts);
                }
            }

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });

        txfDiscountedPrice.setOnKeyPressed(e -> {

            if (e.getCode() == KeyCode.ENTER)
            {
                controller.okAction();
            }

        });
    }

    public static double getDiscountedPrice()
    {
        return discountedPrice;
    }

    public static void setDiscountedPrice(double discountedPrice)
    {
        BookingPaneDialog.discountedPrice = discountedPrice;
    }

    public static int getNumberOfProducts()
    {
        return numberOfProducts;
    }

    public static void setNumberOfProducts(int numberOfProducts)
    {
        BookingPaneDialog.numberOfProducts = numberOfProducts;
    }

    public boolean isConfirmed()
    {
        return isConfirmed;
    }

    // ------------------------------ Controller -------------------------------------------

    private class Controller
    {

        public void cancelAction()
        {
            isConfirmed = false;
            BookingPaneDialog.this.hide();
        }

        public void okAction()
        {

            try
            {
                discountedPrice = Double.parseDouble(txfDiscountedPrice.getText());
                numberOfProducts = Integer.parseInt(txfNumberOfProducts.getText());

                if (discountedPrice < 0)
                {
                    lblError.setVisible(true);
                    txfDiscountedPrice.requestFocus();
                    throw new IllegalArgumentException();
                }

                if (numberOfProducts < 0)
                {
                    lblError.setVisible(true);
                    txfNumberOfProducts.requestFocus();
                    throw new IllegalArgumentException();
                }

                isConfirmed = true;
                BookingPaneDialog.this.hide();
            }

            catch (ParseException pE)
            {

            }

            catch (IllegalArgumentException iAE)
            {

            }

        }
    }

}
