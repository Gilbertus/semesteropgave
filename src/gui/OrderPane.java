package gui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.function.Predicate;

import application.model.Customer;
import application.model.GiftBox;
import application.model.Order;
import application.model.OrderLine;
import application.model.Payment;
import application.model.PaymentType;
import application.model.Price;
import application.model.PriceList;
import application.model.Product;
import application.service.Service;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class OrderPane extends GridPane
{
    public OrderPane()
    {
        this.initContent();
    }

    Service service = new Service();
    TextField searchField = new TextField();

    private final Controller controller = new Controller();

    private final DatePicker dateStartPicker = new DatePicker();
    private final DatePicker dateEndPicker = new DatePicker();

    private final ComboBox<PriceList> comboPriceList = new ComboBox<>();

    private TableView<OrderLine> currentOrder = new TableView<OrderLine>();
    private static TableView<Order> allOrders = new TableView<Order>();
    private static TableView<Price> allProducts = new TableView<Price>();

    private static Label lblTotalPrice = new Label("Total pris på ordre:   0.0 DKK");
    private Label lblEarnedInPeriod = new Label();

    public static double priceRowDataSingle;
    public static int giftBoxSize;
    public static int giftBoxGlasses;

    /**
     * Used as a temporary Order for creation of the currentOrder TableView
     * and later for the finalOrder which goes into the storage
     */
    private Order tempOrderCreation = new Order(LocalDateTime.now());

    /**
     * Used for the dialogs so they can reach a Order
     */
    public static Order tempOrder;
    public static double priceTotal;
    public static boolean isOrderPane;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    Button btnCancel = new Button("Cancel");
    Button btnOK = new Button("OK");
    Button btnOneDay = new Button("1 Dag");
    Button btnOneWeek = new Button("1 Uge");
    Button btnOneMonth = new Button("1 Måned");
    Button btnSixMonths = new Button("6 Måned");
    Button btnOneYear = new Button("1 År");
    Button btnAlways = new Button("Altid");

    HBox buttonBox = new HBox(10);

    private final ObservableList<OrderLine> data = FXCollections.observableArrayList();

    // -----------------------initialize content----------------------

    @SuppressWarnings("unchecked")
    private void initContent()
    {

        this.setGridLinesVisible(false);
        this.setPadding(new Insets(20));
        this.setHgap(10);
        this.setVgap(10);

        // --------------------- Labels ---------------------------------------

        Label lblHeading1 = new Label("Produktoversigt");
        lblHeading1.setFont(Font.font(23));
        this.add(lblHeading1, 0, 0);

        Label lblHeading2 = new Label("Salgs Vindue");
        lblHeading2.setFont(Font.font(23));
        this.add(lblHeading2, 5, 0);

        Label lblHeading3 = new Label("Ordre Oversigt");
        lblHeading3.setFont(Font.font(23));
        this.add(lblHeading3, 9, 0);

        Label lblDateStart = new Label("Fra Dato:");
        this.add(lblDateStart, 9, 44);

        Label lblDateEnd = new Label("Til Dato:");
        this.add(lblDateEnd, 10, 44);

        this.add(lblEarnedInPeriod, 11, 45);

        this.add(lblTotalPrice, 7, 44);
        GridPane.setHalignment(lblTotalPrice, HPos.RIGHT);

        // ------------------- Seperators --------------------------------------

        Separator v1Separator = new Separator();

        v1Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v1Separator, 4, 0, 1, 70);

        Separator v2Separator = new Separator();

        v2Separator.orientationProperty().set(Orientation.VERTICAL);
        this.add(v2Separator, 8, 0, 1, 70);

        // ------------------ ChangeListener ------------------------------

        ChangeListener<PriceList> comboBoxListener = (ov, o, n) -> controller.setPriceDetail();
        comboPriceList.getSelectionModel().selectedItemProperty().addListener(comboBoxListener);

        dateEndPicker.setOnAction(a -> controller.setTransactionsDetails());
        dateStartPicker.setOnAction(a -> controller.setTransactionsDetails());

        // ------------------- Combobox --------------------------------------

        this.add(comboPriceList, 2, 2);
        GridPane.setHalignment(comboPriceList, HPos.RIGHT);

        comboPriceList.getItems().setAll(service.getAllPriceLists());
        comboPriceList.setValue(service.getAllPriceLists().get(0));

        // -------------------- Date Picker --------------------------------

        this.add(dateStartPicker, 9, 45, 1, 1);
        this.add(dateEndPicker, 10, 45, 1, 1);

        dateStartPicker.setValue(LocalDate.now().minusDays(1));
        dateEndPicker.setValue(LocalDate.now());

        // -------------------- Buttons --------------------------------------

        this.add(buttonBox, 5, 44);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));

        Button btnCancel = new Button("Annullere Ordre");
        buttonBox.getChildren().add(btnCancel);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelOrderAction());

        Button btnOK = new Button("Sælg Ordre");
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.sellOrderAction());

        this.add(btnOneDay, 9, 46);
        btnOneDay.setOnAction(event -> controller.oneDayAct());

        this.add(btnOneWeek, 10, 46);
        btnOneWeek.setOnAction(event -> controller.oneWeekAct());

        this.add(btnOneMonth, 9, 47);
        btnOneMonth.setOnAction(event -> controller.oneMonthAct());

        this.add(btnSixMonths, 10, 47);
        btnSixMonths.setOnAction(event -> controller.sixMonthsAct());

        this.add(btnOneYear, 9, 48);
        btnOneYear.setOnAction(event -> controller.oneYearAct());

        this.add(btnAlways, 10, 48);
        btnAlways.setOnAction(event -> controller.alwaysAct());

        //--------------------- TableView Price ----------------------------------------

        allProducts.setEditable(false);

        TableColumn<Price, String> product = new TableColumn<Price, String>("Produktnavn");
        product.setMinWidth(200);
        product.setCellValueFactory(new PropertyValueFactory<Price, String>("product"));

        TableColumn<Price, String> price = new TableColumn<Price, String>("Pris");
        price.setMinWidth(100);
        price.setCellValueFactory(new PropertyValueFactory<Price, String>("price"));

        // priceObject used in Lambda function to get value IdNo from Product in Price constructor - Parsed to String and
        // afterwards packed in a ReadOnlyStringWrapper to convert String to an ObservableValue<String>.

        TableColumn<Price, String> productID = new TableColumn<Price, String>("ID");
        productID.setMinWidth(60);
        productID.setCellValueFactory(
                priceObject -> new ReadOnlyStringWrapper(
                        Integer.toString(priceObject.getValue().getProduct().getIdNo())));

        allProducts.getColumns().addAll(product, price, productID);
        this.add(allProducts, 0, 3, 4, 55);

        allProducts.setMaxWidth(450);

        //--------------------- TableView OrderLine ------------------------------------

        currentOrder.setEditable(true);
        currentOrder.setPlaceholder(new Label("Ingen order oprettet."));

        TableColumn<OrderLine, String> proNavn = new TableColumn<OrderLine, String>("Produktnavn");
        proNavn.setMinWidth(200);
        proNavn.setCellValueFactory(
                navnObject -> new ReadOnlyStringWrapper(
                        navnObject.getValue().getProduct().getName()));

        TableColumn<OrderLine, String> proAntal = new TableColumn<OrderLine, String>("Antal");
        proAntal.setMinWidth(100);
        proAntal.setCellValueFactory(new PropertyValueFactory<>("numberOfProducts"));

//        proAntal.setCellFactory(TextFieldTableCell.<OrderLine, Integer>forTableColumn(new IntegerStringConverter()));
//        proAntal.setOnEditCommit((CellEditEvent<OrderLine, Integer> event) -> {
//            TablePosition<OrderLine, Integer> pos = event.getTablePosition();
//            Integer newAmount = event.getNewValue();
//            int row = pos.getRow();
//            OrderLine orderLine = event.getTableView().getItems().get(row);
//            orderLine.setNumberOfProducts(newAmount);
//        });

        TableColumn<OrderLine, String> proFullAmount = new TableColumn<OrderLine, String>("Samlet Pris");
        proFullAmount.setMinWidth(150);
        proFullAmount.setCellValueFactory(
                amountObject -> new ReadOnlyStringWrapper(
                        Double.toString(amountObject.getValue().getFullAmount())));

        currentOrder.getColumns().addAll(proNavn, proAntal, proFullAmount);

        this.add(currentOrder, 5, 3, 3, 40);

        // -------------------- TableView Orders -----------------------------------------

        TableColumn<Order, String> saleDateTime = new TableColumn<Order, String>("Salgs Tid");
        saleDateTime.setMinWidth(135);
        saleDateTime.setCellValueFactory(
                saleDateTimeObject -> new ReadOnlyStringWrapper(
                        "" + saleDateTimeObject.getValue().getDate().format(formatter)));

        TableColumn<Order, String> totalOrderPrice = new TableColumn<Order, String>("Total Ordre Pris");
        totalOrderPrice.setMinWidth(135);
        totalOrderPrice.setCellValueFactory(totalOrderPriceObject -> new ReadOnlyStringWrapper(
                Double.toString(totalOrderPriceObject.getValue().calculatePrice())));

        TableColumn<Order, String> isOrderPayed = new TableColumn<Order, String>("Ordre Betalt");
        isOrderPayed.setMinWidth(135);
        //Changes shown text of boolean isPayed.
        isOrderPayed.setCellValueFactory(cellData -> {
            boolean payed = cellData.getValue().isPayed();
            String payment;
            if (payed == true)
            {
                payment = "Betalt";
            }
            else
            {
                payment = "Ikke Betalt";
            }

            return new ReadOnlyStringWrapper(payment);
        });

        TableColumn<Order, String> customerNameField = new TableColumn<Order, String>("Kunde");
        customerNameField.setMinWidth(176);
        customerNameField.setCellValueFactory(customerObject -> {
            String tempField;
            if (customerObject.getValue().getCustomer() == null)
            {
                tempField = "N/A";
            }
            else
            {
                tempField = customerObject.getValue().getCustomer().getName();
            }
            return new ReadOnlyStringWrapper(tempField);

        });

        TableColumn<Order, String> customerPhoneField = new TableColumn<Order, String>("Tlf");
        customerPhoneField.setMinWidth(60);

        customerPhoneField.setCellValueFactory(customerObject -> {
            String tempField;
            if (customerObject.getValue().getCustomer() == null)
            {
                tempField = "N/A";
            }
            else
            {
                tempField = customerObject.getValue().getCustomer().getPhoneNumber();
            }
            return new ReadOnlyStringWrapper(tempField);
        });

        allOrders.getColumns().addAll(saleDateTime, totalOrderPrice, isOrderPayed, customerNameField,
                customerPhoneField);
        this.add(allOrders, 9, 3, 3, 40);

        controller.setTransactionsDetails();

        allOrders.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        allProducts.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        currentOrder.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        // -------------------------- Key Events ---------------------------------

        /**
         * KEYPRESS | LOCATION       | ACTION
         * DELETE   | currentOrder   | Deletes seleted OrderLine
         * CTRL + S | All            | Calls Controller.sellOrderAction
         * DOWN     | SearchField    | Sets focus on allProducts index (0)
         * Enter    | SearchField    | Sets focus on allProducts index (0)
         * Enter    | allProducts    | Calls setProductLine()
         * Enter    | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         */

        currentOrder.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.DELETE && (!currentOrder.getSelectionModel().getSelectedItem().equals(null)))
            {
                tempOrderCreation.removeOrderLine(currentOrder.getSelectionModel().getSelectedItem());
                data.remove(currentOrder.getSelectionModel().getSelectedItem());
                currentOrder.setItems(FXCollections.observableArrayList(data));
                tempOrderCreation.calculatePrice();

            }
        });

        this.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler()
        {

            final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.S,

                    KeyCombination.CONTROL_DOWN);

            @Override
            public void handle(Event event)
            {

                if (keyComb1.match((KeyEvent) event))
                {

                    controller.sellOrderAction();

                }

            }

        });

        searchField.setOnKeyPressed(e -> {

            if (e.getCode() == KeyCode.DOWN)
            {
                focusTableView();

            }

            if (e.getCode() == KeyCode.ENTER)
            {
                focusTableView();
            }

        });

        allOrders.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && (!allOrders.getSelectionModel().getSelectedItem().equals(null)))
            {
                setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                getOrderInformation();

            }

        });

        allProducts.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER && (!allProducts.getSelectionModel().getSelectedItem().equals(null)))
            {

                controller.setProductLine();

            }

        });

        //------------------- KeyBoard Event ------------------------------

        /**
         * MOUSEPRESS    | LOCATION       | ACTION
         * Double-Click  | allOrders      | Sets tempOrder to selected and Call getOrderInformation()
         * Double-Click  | allProducts    | Calls setProductLine()
         */

        //------------------- Mouse Event All Products ---------------------------------

        allProducts.setRowFactory(allProMouse -> {
            TableRow<Price> priceRow = new TableRow<>();
            priceRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!priceRow.isEmpty()))
                {

                    controller.setProductLine();

                }

            });
            return priceRow;
        });

        //------------------- Mouse Event All Orders ---------------------------------

        allOrders.setRowFactory(allOrdMouse -> {
            TableRow<Order> orderRow = new TableRow<>();
            orderRow.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!orderRow.isEmpty()))
                {
                    setTempOrder(allOrders.getSelectionModel().getSelectedItem());
                    getOrderInformation();

                }

            });
            return orderRow;
        });

        /**
         * Creates search field where you find products which includes the string you write
         * in their name
         */

        // --------------------- Search Function Products --------------------------------

        final ObservableList<Price> data = FXCollections
                .observableArrayList(comboPriceList.getSelectionModel().getSelectedItem().getPrices());

        this.add(searchField, 0, 2);
        searchField.setFont(Font.font("CALIBRI"));
        searchField.setPromptText("Søgefelt");

        FilteredList<Price> filtPrice = new FilteredList<>(data, e -> true);
        searchField.setOnKeyReleased(e -> {
            searchField.textProperty().addListener((observableValue, oldVal, newVal) -> {
                filtPrice.setPredicate((Predicate<? super Price>) p -> {
                    if (newVal == null || newVal.isEmpty())
                    {
                        return true;
                    }
                    String lcf = newVal.toLowerCase();
                    if (p.getProduct().getName().toLowerCase().contains(lcf))
                    {
                        return true;
                    }

                    return false;
                });
            });
            SortedList<Price> sortedProduct = new SortedList<>(filtPrice);
            sortedProduct.comparatorProperty().bind(allProducts.comparatorProperty());
            allProducts.setItems(sortedProduct);
        });

    }

    // ----------------- GETTERS AND SETTERS --------------------------------

    public static double getPriceRowDataSingle()
    {
        return priceRowDataSingle;
    }

    public static double getPriceTotal()
    {
        return priceTotal;
    }

    public static boolean isOrderPane()
    {
        return isOrderPane;
    }

    public static int getGiftBoxSize()
    {
        return giftBoxSize;
    }

    public static int getGiftBoxGlasses()
    {
        return giftBoxGlasses;
    }

    public static void setOrderPane(boolean isOrderPane)
    {
        OrderPane.isOrderPane = isOrderPane;
    }

    public static Order getTempOrder()
    {
        return tempOrder;
    }

    public static void setTempOrder(Order tempOrder)
    {
        OrderPane.tempOrder = tempOrder;
    }

    /**
     * Sets isOrderPane = true
     * Opens OrderPaneOrderDialog
     * Calls controller.setTransactionsDeatils()
     * Sets isOrdePane = false
     */
    public void getOrderInformation()
    {

        isOrderPane = true;
        Stage stage = new Stage();
        OrderPaneOrderDialog dialog = new OrderPaneOrderDialog(stage);
        dialog.showAndWait();

        controller.setTransactionsDetails();
        isOrderPane = false;

    }

    // ------------------------- Methods -------------------------------------------

    /**
     * Sets the Total Price Label
     */
    public void setTotalPriceTextField()
    {

        lblTotalPrice.setText("Total pris på ordre:   " + tempOrderCreation.calculatePrice() + " DKK");

    }

    /**
     * Sets focus on the products table view, selecting the top row
     */
    public void focusTableView()
    {
        allProducts.requestFocus();
        allProducts.getSelectionModel().select(0);
        allProducts.getFocusModel().focus(0);
    }

    /**
     * Updates OrderPane when OrderTab is selected
     */
    public void updateControls()
    {
        comboPriceList.getItems().setAll(service.getAllPriceLists());
        comboPriceList.getSelectionModel().select(0);
        currentOrder.getItems().clear();
        tempOrderCreation = new Order(LocalDateTime.now());
        data.clear();
        currentOrder.setItems(FXCollections.observableArrayList(data));
        allProducts.setItems(null);

        if (comboPriceList.getSelectionModel().getSelectedItem() != null)
        {
            allProducts.setItems(FXCollections
                    .observableArrayList(comboPriceList.getSelectionModel().getSelectedItem().getPrices()));

        }

        controller.setTransactionsDetails();

        allProducts.refresh();
        allOrders.refresh();
        currentOrder.refresh();

    }

    // ----------------------- controller ----------------------------

    private class Controller
    {

        /**
         * Creates a new OrderLine creation dialog when a product is pressed by "Enter" or "Double-Click"
         */
        public void setProductLine()
        {
            double oldPrice = allProducts.getSelectionModel().getSelectedItem().getPrice();
            Price priceRowData = allProducts.getSelectionModel().getSelectedItem();
            priceRowDataSingle = allProducts.getSelectionModel().getSelectedItem().getPrice();

            int numberOfProducts = 0;
            double discountedPrice = 0;

            if (allProducts.getSelectionModel().getSelectedItem().getProduct() instanceof GiftBox)
            {

                GiftBox giftBox = (GiftBox) allProducts.getSelectionModel().getSelectedItem().getProduct();

                giftBoxSize = giftBox.getBeerAmount();
                giftBoxGlasses = giftBox.getGlassAmount();

                Stage stage = new Stage();
                OrderGiftBoxDialog dialog = new OrderGiftBoxDialog(stage);
                dialog.showAndWait();

                if (dialog.isConfirmed() == true)
                {

                    discountedPrice = dialog.getDiscountedPrice();

                    ArrayList<OrderLine> bottles = new ArrayList<>();

                    OrderLine o1 = tempOrderCreation.createOrderLine(1, dialog.getDiscountedPrice(), giftBox);

                    setTotalPriceTextField();
                    //currentOrder is populated with array of data

                    for (OrderLine orderLine : dialog.getBottles())
                    {

                        OrderLine o2 = tempOrderCreation.createOrderLine(orderLine.getNumberOfProducts(), 0,
                                orderLine.getProduct());

                        bottles.add(o2);

                    }

                    if (giftBoxGlasses > 0)
                    {
                        for (Product product : service.getAllProducts())
                        {
                            if (product.getName().equals("Glas"))
                            {
                                tempOrderCreation.createOrderLine(giftBoxGlasses, 0,
                                        product);
                            }
                        }

                    }

                    data.clear();
                    if (data.size() == 0)
                    {
                        data.addAll(tempOrderCreation.getOrderLines());
                    }

                    else
                    {
                        data.add(tempOrderCreation.getOrderLines().get(data.size()));

                    }
                    int index = data.indexOf(o1);
                    data.get(index).setFullAmount();

                    currentOrder.setItems(FXCollections.observableArrayList(data));
                    currentOrder.refresh();
                    searchField.clear();
                    searchField.requestFocus();
                    bottles.clear();
                }
            }

            else
            {

                Stage stage = new Stage();
                OrderPaneDialog dialog = new OrderPaneDialog(stage);
                dialog.showAndWait();

                numberOfProducts = dialog.getNumberOfProducts();
                discountedPrice = dialog.getDiscountedPrice();

                if (dialog.isConfirmed() == true)
                {

                    boolean newItem = true;
                    int orderLinePlace = 0;

                    // ---- Goes through all orderLines on the Order - tempOrderCreation
                    for (OrderLine orderLine : tempOrderCreation.getOrderLines())
                    {
                        // ------ Checks if the product already exists on the Order and with the same discountedPrice
                        if (orderLine.getProduct() == priceRowData.getProduct()
                                && orderLine.getDiscountedPrice() == discountedPrice)
                        {
                            // ---- Increases the numberOfProducts on the already exsisting OrderLine if product is the same
                            // ---- and discountedPrice is the same. Then sets new Item to false and gets location of the orderLine
                            orderLine.setNumberOfProducts(numberOfProducts + orderLine.getNumberOfProducts());
                            newItem = false;
                            if (data.contains(orderLine))
                            {
                                orderLinePlace = data.indexOf(orderLine);

                            }
                        }
                    }

                    // ---- Sets the new fullAmount after orderLine has increased numberOfProducts
                    if (newItem == false)
                    {
                        data.get(orderLinePlace).setFullAmount();
                    }

                    // ------ if product is not the same as an exsisting orderLines product it creates a new orderLine
                    if (newItem == true)
                    {

                        // Check to see if discount price is less or the same as the product getting discounted
                        priceRowData.setPrice(discountedPrice);
                        tempOrderCreation.createOrderLine(numberOfProducts, discountedPrice, priceRowData.getProduct());
                        priceRowData.setPrice(oldPrice);

                        if (data.size() == 0)
                        {
                            data.addAll(tempOrderCreation.getOrderLines());
                        }

                        else
                        {
                            data.add(tempOrderCreation.getOrderLines().get(data.size()));

                        }

                        data.get(data.size() - 1).setFullAmount();
                    }

                }
                //Added to observableArrayList

                setTotalPriceTextField();
                //currentOrder is populated with array of data

                currentOrder.setItems(FXCollections.observableArrayList(data));
                currentOrder.refresh();
                searchField.clear();
                searchField.requestFocus();
            }

        }

        /**
         * Fills allProducts with prices and their connected prodcuts
         */
        public void setPriceDetail()
        {
            if (comboPriceList.getSelectionModel().getSelectedItem() != null)
            {
                allProducts.setItems(FXCollections
                        .observableArrayList(comboPriceList.getSelectionModel().getSelectedItem().getPrices()));
            }
        }

        /**
         * Fills allOrders with allOrders in storage where the orders date isBefore EndPickerValue and isAfter StartPickerValue
         * Then it sorts by date in a Descending order
         */
        public ArrayList<Order> setTransactionsDetails()
        {
            ArrayList<Order> tempOrderArray = new ArrayList<>();

            for (Order order : service.getAllOrders())
            {

                LocalDate OrderValue = order.getDate().toLocalDate();
                LocalDate StartPickerValue = dateStartPicker.getValue();
                LocalDate EndPickerValue = dateEndPicker.getValue();

                if (OrderValue.isAfter(StartPickerValue) && OrderValue.isBefore(EndPickerValue)
                        || OrderValue.isEqual(EndPickerValue))
                {

                    tempOrderArray.add(order);

                }

                tempOrderArray.sort((a, b) -> b.getDate().compareTo(a.getDate()));
                lblEarnedInPeriod.setText(
                        "Penge tjent i perioden: " + service.calculateEarned(tempOrderArray) + " kr");

                allOrders.setItems(FXCollections
                        .observableArrayList(tempOrderArray));

                allOrders.refresh();

            }
            return tempOrderArray;

        }

        /**
         * Clears data - Cancel the Order and thereby deletes all OrderLines in currentOrder
         */
        public void cancelOrderAction()
        {

            for (OrderLine orderLine : data)
            {
                tempOrderCreation.removeOrderLine(orderLine);
            }

            data.clear();
            currentOrder.setItems(FXCollections.observableArrayList(data));
            setTotalPriceTextField();

        }

        /**
         * Creates new Order with OrderLines from currentOrder
         * Error if data is empty | Error if trying to pay more than needed | Error if the payed amount is <= 0
         * Clears data, currentOrders and searchField.
         * Calls setTotalPriceTextField() and setTransactionsDetails()
         */

        public void sellOrderAction()
        {

            {
                try
                {
                    if (data.isEmpty())
                    {
                        throw new RuntimeException();
                    }
                    tempOrder = tempOrderCreation;
                    priceTotal = tempOrderCreation.calculatePrice();
                    Stage stage = new Stage();
                    OrderPanePaymentDialog dialog = new OrderPanePaymentDialog(stage);
                    dialog.showAndWait();

                    if (dialog.isConfirmedSold() == true)

                    {
                        if (dialog.getMoneyPayed() <= tempOrderCreation.calculateRemainingPrice()
                                && dialog.getMoneyPayed() != 0)
                        {

                            Order finalOrder = service.createOrder(LocalDateTime.now());

                            Payment payment = service.createPayment(dialog.getTypeOfPayment(),
                                    dialog.getMoneyPayed());

                            {
                                for (OrderLine orderLine : data)
                                {
                                    finalOrder.createOrderLine(orderLine.getNumberOfProducts(),
                                            orderLine.getDiscountedPrice(),
                                            orderLine.getProduct());

                                }
                            }

                            payment.payedWithVoucher(finalOrder);

                            if (dialog.getAmountClipsUsed() != 0
                                    && dialog.getAmountClipsUsed() > 0
                                    && payment.getPaymentType() != PaymentType.VOUCHERCLIP)
                            {

                                Payment payment2 = service.createPayment(PaymentType.VOUCHERCLIP,
                                        dialog.getAmountClipsUsed());

                                payment2.payedWithVoucher(finalOrder);
                                finalOrder.addPayment(payment2);

                            }

                            finalOrder.addPayment(payment);

                            if (dialog.isWithCostumer() == true)
                            {
                                Customer customer = dialog.getTempCustomer();
                                customer.addOrder(finalOrder);
                            }

                            service.sellOrder(finalOrder.getOrderLines(), finalOrder.getPayments(), finalOrder);

                            for (OrderLine orderLine : data)
                            {
                                tempOrderCreation.removeOrderLine(orderLine);
                            }

                            data.clear();
                            currentOrder.setItems(FXCollections.observableArrayList(data));
                            setTotalPriceTextField();
                            searchField.clear();
                            setTransactionsDetails();

                        }
                        data.clear();
                    }

                }

                catch (RuntimeException rte)
                {
                    System.out.println("RuntimeException - #789 OrderPane");
                }
            }

        }

        public void oneDayAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusDays(1));
        }

        public void oneWeekAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusWeeks(1));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1 month back
         */
        public void oneMonthAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(1));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 6 months back
         */
        public void sixMonthsAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(6));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1 year back
         */
        public void oneYearAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusYears(1));
        }

        /**
         * Sets dateEnd to .now() and dateStart to 1000 months back
         */
        public void alwaysAct()
        {
            dateEndPicker.setValue(LocalDate.now());
            dateStartPicker.setValue(LocalDate.now().minusMonths(1000));
        }

    }

}
