package gui;

import java.util.ArrayList;

import application.model.PriceList;
import application.model.Product;
import application.model.ProductType;
import application.service.Service;
import javafx.beans.value.ChangeListener;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ProductDialog extends Stage
{
    Service service = new Service();
    private final Controller controller = new Controller();
    private final Label lblError = new Label();
    private final TextField txfName = new TextField();
    private final TextField txfProductCount = new TextField();
    private final TextField txfPrice = new TextField();
    private final TextField txfAlcoPer = new TextField();
    private final TextField txfGlassAmount = new TextField();
    private final TextField txfBeerAmount = new TextField();
    private final TextField txfBoxType = new TextField();
    private final TextField txfVoucherClips = new TextField();
    private final TextField txfUnitType = new TextField();
    private final TextField txfUnits = new TextField();
    private final TextField txfDeposit = new TextField();
    private final Label lblType = new Label("Produkt Type");
    private final Label lbl1 = new Label("Navn: ");
    private final Label lbl2 = new Label("Lagerbeholdning: ");
    private final Label lbl3 = new Label("Standard Pris: ");
    private final Label lbl4 = new Label("Alkohol Procent: ");
    private final Label lbl5 = new Label("Antal Glas: ");
    private final Label lbl6 = new Label("Antal Øl: ");
    private final Label lbl7 = new Label("Type af box: ");
    private final Label lbl8 = new Label("Antal Klip: ");
    private final Label lbl9 = new Label("Måleenhed: ");
    private final Label lbl10 = new Label("Størrelse: ");
    private final Label lbl11 = new Label("Pant: ");
    private final Button btnCancel = new Button("Cancel");
    private final Button btnOK = new Button("OK");
    private final ComboBox<ProductType> ComboSpecificTypes = new ComboBox<>();
    private final HBox buttonBox = new HBox(10);

    // -----------------------initialize new stage and scene--------------

    public ProductDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);
        this.setTitle("Nyt Produkt");
        GridPane pane = new GridPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private void initContent(GridPane pane)
    {
        pane.setGridLinesVisible(false);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------

        pane.add(lblType, 0, 0);
        pane.add(lbl1, 0, 1);
        pane.add(lbl2, 0, 2);
        pane.add(lbl3, 0, 3);
        pane.add(lbl4, 0, 4);
        pane.add(lbl5, 0, 5);
        pane.add(lbl6, 0, 6);
        pane.add(lbl7, 0, 7);
        pane.add(lbl8, 0, 8);
        pane.add(lbl9, 0, 9);
        pane.add(lbl10, 0, 10);
        pane.add(lbl11, 0, 11);
        pane.add(lblError, 1, 12);
        GridPane.setHalignment(lblError, HPos.RIGHT);
        lblError.setText("Mangler information");
        lblError.setTextFill(Color.RED);
        lblError.setVisible(false);

        // -------------------------- ComboBox --------------------------------
        pane.add(ComboSpecificTypes, 1, 0);
        GridPane.setHalignment(ComboSpecificTypes, HPos.RIGHT);
        ArrayList<ProductType> productTypes = service.getAllProductTypes();

        productTypes.remove(ProductType.TOUR);

        ComboSpecificTypes.getItems().setAll(productTypes);
        ComboSpecificTypes.setValue(ProductType.BOTTLE);

        // ------------------------ ChangeListener --------------------------

        ChangeListener<ProductType> enumListener = (ov, o, n) -> controller.setFields();
        ComboSpecificTypes.getSelectionModel().selectedItemProperty().addListener(enumListener);

        // -------------------------- Text Fields ----------------------------

        pane.add(txfName, 1, 1);
        pane.add(txfProductCount, 1, 2);
        pane.add(txfPrice, 1, 3);
        pane.add(txfAlcoPer, 1, 4);
        pane.add(txfGlassAmount, 1, 5);
        pane.add(txfBeerAmount, 1, 6);
        pane.add(txfBoxType, 1, 7);
        pane.add(txfVoucherClips, 1, 8);
        pane.add(txfUnitType, 1, 9);
        pane.add(txfUnits, 1, 10);
        pane.add(txfDeposit, 1, 11);
        txfGlassAmount.setDisable(true);
        txfBeerAmount.setDisable(true);
        txfBoxType.setDisable(true);
        txfVoucherClips.setDisable(true);
        txfUnits.setDisable(true);
        txfUnitType.setDisable(true);
        txfDeposit.setDisable(true);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 1, 13);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);
        buttonBox.getChildren().add(btnCancel);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelAction());
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.createAction());
    }

    // -------------------------------------------------------------------------

    private class Controller
    {
        /**
         * Sets the specific fields needed for creating the specific subclass of Product
         */
        public void setFields()
        {
            ProductType productType = ComboSpecificTypes.getValue();
            {
                if (productType == ProductType.BOTTLE || productType == ProductType.SPIRIT
                        || productType == ProductType.TAPBEER || productType == ProductType.DISPENSER)
                {

                    txfAlcoPer.setDisable(false);
                    txfGlassAmount.clear();
                    txfGlassAmount.setDisable(true);
                    txfBeerAmount.clear();
                    txfBeerAmount.setDisable(true);
                    txfBoxType.clear();
                    txfBoxType.setDisable(true);
                    txfVoucherClips.clear();
                    txfVoucherClips.setDisable(true);
                    txfUnits.clear();
                    txfUnits.setDisable(true);
                    txfUnitType.clear();
                    txfUnitType.setDisable(true);
                    txfDeposit.clear();
                    txfDeposit.setDisable(true);
                }

                if (productType == ProductType.MISC)
                {
                    txfAlcoPer.clear();
                    txfAlcoPer.setDisable(true);
                    txfGlassAmount.clear();
                    txfGlassAmount.setDisable(true);
                    txfBeerAmount.clear();
                    txfBeerAmount.setDisable(true);
                    txfBoxType.clear();
                    txfBoxType.setDisable(true);
                    txfVoucherClips.clear();
                    txfVoucherClips.setDisable(true);
                    txfUnits.clear();
                    txfUnits.setDisable(true);
                    txfUnitType.clear();
                    txfUnitType.setDisable(true);
                    txfDeposit.clear();
                    txfDeposit.setDisable(true);
                }

                if (productType == ProductType.GIFTBOX)
                {
                    txfGlassAmount.setDisable(false);
                    txfBeerAmount.setDisable(false);
                    txfBoxType.setDisable(false);
                    txfAlcoPer.clear();
                    txfAlcoPer.setDisable(true);
                    txfVoucherClips.clear();
                    txfVoucherClips.setDisable(true);
                    txfUnits.clear();
                    txfUnits.setDisable(true);
                    txfUnitType.clear();
                    txfUnitType.setDisable(true);
                    txfDeposit.clear();
                    txfDeposit.setDisable(true);
                }

                if (productType == ProductType.VOUCHERCLIP)
                {
                    txfAlcoPer.setDisable(true);
                    txfGlassAmount.clear();
                    txfGlassAmount.setDisable(true);
                    txfBeerAmount.clear();
                    txfBeerAmount.setDisable(true);
                    txfBoxType.clear();
                    txfBoxType.setDisable(true);
                    txfVoucherClips.clear();
                    txfVoucherClips.setDisable(false);
                    txfUnits.clear();
                    txfUnits.setDisable(true);
                    txfUnitType.clear();
                    txfUnitType.setDisable(true);
                    txfDeposit.clear();
                    txfDeposit.setDisable(true);
                }
                if (productType == ProductType.CARBON || productType == ProductType.KEG)
                {
                    txfAlcoPer.setDisable(true);
                    txfGlassAmount.clear();
                    txfGlassAmount.setDisable(true);
                    txfBeerAmount.clear();
                    txfBeerAmount.setDisable(true);
                    txfBoxType.clear();
                    txfBoxType.setDisable(true);
                    txfVoucherClips.clear();
                    txfVoucherClips.setDisable(true);
                    txfUnits.clear();
                    txfUnits.setDisable(false);
                    txfUnitType.clear();
                    txfUnitType.setDisable(false);
                    txfDeposit.clear();
                    txfDeposit.setDisable(false);
                }
            }

        }

        /**
         * Closes ProductDialog without doing anything
         */
        public void cancelAction()
        {
            ProductDialog.this.hide();
        }

        /**
         * Creates a new product with the information given in the enabled fields.
         * Throws a RuntimeException if invalid information is entered.
         */
        public void createAction()
        {
            try
            {
                String name = txfName.getText().trim();
                ProductType productType = ComboSpecificTypes.getValue();
                int productCount = Integer.parseInt(txfProductCount.getText().trim());

                if (productType == ProductType.MISC || productType == ProductType.DISPENSER)
                {

                    Product p = service.createProduct(name, productCount, productType);

                    for (PriceList priceList : service.getAllPriceLists())
                    {
                        priceList.createPrice(Double.parseDouble(txfPrice.getText()), p);
                    }
                    ProductDialog.this.hide();

                }

                if (productType == ProductType.BOTTLE || productType == ProductType.SPIRIT
                        || productType == ProductType.TAPBEER)
                {
                    double alcoPer = Double.parseDouble(txfAlcoPer.getText().trim());

                    Product p1 = service.createBerverage(name, productCount, productType, alcoPer);

                    for (PriceList priceList : service.getAllPriceLists())
                    {
                        priceList.createPrice(Double.parseDouble(txfPrice.getText()), p1);
                    }
                    ProductDialog.this.hide();
                }

                if (productType == ProductType.KEG || productType == ProductType.CARBON)
                {

                    Double units = Double.parseDouble(txfUnits.getText().trim());
                    String unitType = txfUnitType.getText().trim();
                    Double deposit = Double.parseDouble(txfDeposit.getText().trim());

                    Product p2 = service.createDepositProduct(name, productCount, productType, unitType, units,
                            deposit);

                    for (PriceList priceList : service.getAllPriceLists())
                    {
                        priceList.createPrice(Double.parseDouble(txfPrice.getText()), p2);
                    }
                    ProductDialog.this.hide();
                }

                if (productType == ProductType.GIFTBOX)
                {
                    int glassAmount = Integer.parseInt(txfGlassAmount.getText().trim());
                    int beerAmount = Integer.parseInt(txfBeerAmount.getText().trim());
                    String boxType = txfBoxType.getText().trim();

                    Product p2 = service.createGiftBox(name, productCount, productType, beerAmount, glassAmount,
                            boxType);

                    for (PriceList priceList : service.getAllPriceLists())
                    {
                        priceList.createPrice(Double.parseDouble(txfPrice.getText()), p2);
                    }
                    ProductDialog.this.hide();
                }

                if (productType == ProductType.VOUCHERCLIP)
                {
                    int clipCount = Integer.parseInt(txfVoucherClips.getText().trim());

                    Product p3 = service.createVoucherClip(name, productCount, productType, clipCount);

                    for (PriceList priceList : service.getAllPriceLists())
                    {
                        priceList.createPrice(Double.parseDouble(txfPrice.getText()), p3);
                    }
                    ProductDialog.this.hide();
                }

            }

            catch (RuntimeException c)
            {
                lblError.setVisible(true);
            }
        }
    }
}
