package gui;

import application.model.Price;
import application.model.PriceList;
import application.service.Service;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PriceListDialog extends Stage
{
    Service service = new Service();
    private final Controller controller = new Controller();
    private final ComboBox<PriceList> comboPriceList = new ComboBox<>();
    private final TextField txfName = new TextField();
    private final Button btnCancel = new Button("Cancel");
    private final Button btnOK = new Button("OK");
    private final HBox buttonBox = new HBox(10);

    // -----------------------initialize new stage and scene--------------

    public PriceListDialog(Stage owner)
    {
        this.initOwner(owner);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setResizable(false);
        this.setTitle("Ny Prisliste");
        GridPane pane = new GridPane();
        this.initContent(pane);
        Scene scene = new Scene(pane);
        this.setScene(scene);
    }

    // -----------------------initialize content----------------------

    private void initContent(GridPane pane)
    {

        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(10));

        // ---------------------- Labels --------------------------------

        Label lbl1 = new Label("Navn: ");
        pane.add(lbl1, 0, 1);

        // -------------------------- ComboBox --------------------------------

        pane.add(comboPriceList, 0, 0);
        comboPriceList.getItems().setAll(service.getAllPriceLists());
        comboPriceList.setValue(service.getAllPriceLists().get(0));

        // -------------------------- Text Fields ----------------------------

        pane.add(txfName, 1, 1);

        // -------------------------- Buttons -------------------------------

        pane.add(buttonBox, 0, 2);
        buttonBox.setPadding(new Insets(11, 0, 0, 0));
        buttonBox.setAlignment(Pos.TOP_RIGHT);
        buttonBox.getChildren().add(btnCancel);
        btnCancel.setPrefWidth(70);
        btnCancel.setCancelButton(true);
        btnCancel.setOnAction(event -> controller.cancelAction());
        buttonBox.getChildren().add(btnOK);
        btnOK.setOnAction(event -> controller.createAction());
    }

    // ---------------------Controller----------------------------------------------------

    private class Controller
    {

        public void cancelAction()
        {
            PriceListDialog.this.hide();
        }

        /**
         * Creates new PriceList
         */
        public void createAction()
        {
            try
            {
                PriceList pc = comboPriceList.getSelectionModel().getSelectedItem();
                PriceList p = service.createPriceList(txfName.getText().trim());
                for (Price price : pc.getPrices())
                {
                    p.createPrice(price.getPrice(), price.getProduct());
                }
                PriceListDialog.this.hide();
            }
            catch (RuntimeException c)
            {
                String s = "Du skal skrive et navn";
                txfName.setStyle("-fx-text-box-border: red");
                txfName.setPromptText(s);

            }
        }
    }
}
